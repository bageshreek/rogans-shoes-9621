﻿@using Znode.Engine.WebStore.ViewModels;
@using Znode.Libraries.ECommerce.Utilities
@model  SearchRequestViewModel
@{
    //Check if category ID is present or not
    if (Model.CategoryId <= 0)
    {
        ViewBag.Title = "Search";
    }
    string searchTerm = Model?.SearchTerm;
    bool isEnableCMSPageSearch = Helper.IsEnableCMSPageSearch();
    int contentPageNumber = SessionHelper.GetDataFromSession<int>(WebStoreConstants.CMSPageNumber);
    int contentPageSize = SessionHelper.GetDataFromSession<int>(WebStoreConstants.CMSPageSizeValue);
}

@if (!isEnableCMSPageSearch)
{
    <div id="layout-category" data-category="@Model.SearchTerm" data-categoryId="@Model.CategoryId" class="main-wrapper">
        <div class="container-fluid border-top-1">
            <div class="PLP-Container">

                @if (Model?.ProductListViewModel?.Products?.Count > 0)
                {
                    <aside class="col-xs-12 col-sm-2 side-bar search-mobile">
                        <!--Mobile View Facet Calling Code-->
                        <div class="col-xs-12 nopadding side-bar hidden-lg hidden-md visible-xs">
                            <a href="#" class="toggle narrow-results active" data-toggle="modal" data-target="#FacetModal">Filters</a>
                            <!-- Modal -->

                            <div id="FacetModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Filters</h4>
                                            <a class="close pull-right" data-dismiss="modal" aria-label="Close" title="Close"> &#x2715 </a>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-xs-12 nopadding">
                                                <div class="facets-content-area">
                                                    <span id="Facetmobile"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div id="Facetdesktop">
                            <div id="Facetdesktopmain" class="facets-content-area">
                                @{Html.RenderPartial("_FacetList", Model.ProductListViewModel.SearchResultViewModel);}
                            </div>
                        </div>
                        <div class="col-xs-12 nopadding">
                            <div id="compareProductList">
                            </div>
                        </div>
                    </aside>
                }

                <div class="col-xs-12 col-sm-10 category-landing-container search-page">
                    <div data-test-selector="divSearchResult">
                        @{Html.RenderPartial("_ProductGrid", Model.ProductListViewModel);}
                    </div>
                </div>
                <div class="col-xs-12 nopadding">
                    <z-widget class="category-recent-view-list">@Html.WidgetPartial("RecentlyViewProduct", "Recently View Product", "126", ZnodeCMSTypeofMappingEnum.PortalMapping.ToString())</z-widget>
                </div>
            </div>
        </div>
    </div>
}
else
{
    <div id="layout-category" data-category="@Model.SearchTerm" data-categoryId="@Model.CategoryId" class="main-wrapper search-page">
        <div class="container-fluid">

            <div>
                <div class="col-md-12 header-results-text">
                    @Html.RenderBlock(WebStore_Resources.TextSearchResult?.Replace("{0}", searchTerm))
                </div>
            </div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#product-list-tab" aria-controls="product-list-tab" role="tab" data-toggle="tab">@Model.ProductListViewModel.TotalProductCount PRODUCTS</a></li>
                <li role="presentation"><a href="#pages-list-tab" onclick="Search.prototype.GetSearchCMSPage(this)" id="formcmspagesearch" data-searchterm="@searchTerm" data-targetid="#searchcmspages" aria-controls="pages-list-tab" role="tab" data-toggle="tab">@Model.ProductListViewModel.TotalCMSPageCount PAGES</a></li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="product-list-tab">
                    <div class="row">
                        @if (Model?.ProductListViewModel?.Products?.Count > 0)
                        {
                            <aside class="col-xs-12 col-sm-3 side-bar search-mobile">
                                @{Html.RenderPartial("_FacetList", Model.ProductListViewModel.SearchResultViewModel);}
                                <div class="col-xs-12 nopadding">
                                    <div id="compareProductList">
                                    </div>
                                </div>
                            </aside>
                        }
                        <div class="col-xs-12 col-sm-9 category-landing-container search-page">
                            <div data-test-selector="divSearchResult">
                                @{Html.RenderPartial("_ProductGrid", Model.ProductListViewModel);}
                            </div>
                        </div>
                        <div class="col-xs-12 nopadding">
                            <z-widget class="category-recent-view-list">@Html.WidgetPartial("RecentlyViewProduct", "Recently View Product", "126", ZnodeCMSTypeofMappingEnum.PortalMapping.ToString())</z-widget>
                        </div>
                        <input type="hidden" id="hdncontentPageNumber" name="hdncontentPageNumber" value="@contentPageNumber">
                        <input type="hidden" id="hdncontentPageSize" name="hdncontentPageSize" value="@contentPageSize">
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="pages-list-tab">
                    <div class="container">
                        <div id="searchcmspages"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
}


<script async>
    $(document).ready(function () { screen.width > 600 ? $("#Facetdesktop").append($("#Facetdesktopmain")) : $("#Facetmobile").append($("#Facetdesktopmain")) });
</script>

<!--Facet call in Modal in Mobile View-->
<script async>
    $(document).ready(function () {
        function togglePanel() { var w = $(window).width(); if (w <= 600) { $("#FacetModal").add() } else { $("#FacetModal").remove() } }
        $(window).resize(function () { togglePanel() }); togglePanel()
    })
</script>

