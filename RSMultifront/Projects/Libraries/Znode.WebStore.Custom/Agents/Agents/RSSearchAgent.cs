﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSSearchAgent : SearchAgent
    {
        private readonly ISearchClient _searchClient;
        public RSSearchAgent(ISearchClient searchClient):base()
        {
            _searchClient = GetClient<ISearchClient>(searchClient);
        }        

        /// <summary>
        /// In Search Suggestions same Product was coming multiple times for diff categories
        /// So we removed category name and just kept productname in suggestion.
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        public override List<AutoComplete> GetSuggestions(string searchTerm, string category)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                FilterCollection filter = GetRequiredFilters();
                filter.Add(WebStoreEnum.ProductIndex.ToString(), FilterOperators.Equals, ZnodeConstant.DefaultPublishProductIndex.ToString());
                KeywordSearchModel searchModel = _searchClient.GetKeywordSearchSuggestion(new SearchRequestModel { Keyword = searchTerm.Trim(), LocaleId = PortalAgent.LocaleId, CatalogId = GetCatalogId().GetValueOrDefault(), PortalId = PortalAgent.CurrentPortal.PortalId, PageSize = 10, IsAutocomplete = true }, new ExpandCollection { "Categories" }, filter, null, 0, 0);
                List<AutoComplete> autoCompleteList = new List<AutoComplete>();
                //Map autocomplete list
                searchModel.Products?.ForEach(x =>
                {

                    if (!string.IsNullOrEmpty(x.CategoryName))
                    {
                        AutoComplete autoCompleteModel = new AutoComplete();
                        // autoCompleteModel.Name = $"<div class='auto-list'><p><b>{x.Name}</b>{" in "}{x.CategoryName}</p></div>";
                        /*Nivi code start*/
                        autoCompleteModel.Name = $"<div class='auto-list'><p><b>{x.Name}</b></p></div>";
                        /*Nivi code end*/
                        autoCompleteModel.Id = x.ZnodeProductId;
                        autoCompleteModel.DisplayText = x.Name;
                        autoCompleteModel.Properties.Add(x.CategoryName, x.CategoryId);
                        if (!AlreadyExist(autoCompleteList, autoCompleteModel))
                            autoCompleteList.Add(autoCompleteModel);
                    }
                    else
                    {
                        AutoComplete autoCompleteModel = new AutoComplete();
                        autoCompleteModel.Name = $"{x.Name}";
                        autoCompleteModel.Id = x.ZnodeProductId;
                        autoCompleteModel.DisplayText = x.Name;
                        if (!AlreadyExist(autoCompleteList, autoCompleteModel))
                            autoCompleteList.Add(autoCompleteModel);
                    }
                });
                ZnodeLogging.LogMessage("autoCompleteList:", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Verbose, new { autoCompleteList = autoCompleteList });
                return autoCompleteList;
            }
            else
                return new List<AutoComplete>();
        }

        /// <summary>
        /// Private method hence no option other than to rewrite
        /// </summary>
        /// <param name="autoCompleteList"></param>
        /// <param name="autoCompleteModel"></param>
        /// <returns></returns>
        private bool AlreadyExist(List<AutoComplete> autoCompleteList, AutoComplete autoCompleteModel)
       => autoCompleteList.Any(x => x.Name == autoCompleteModel.Name);



    }
}
