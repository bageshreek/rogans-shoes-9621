﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Api.Custom.Service.Service
{
    public class RSPublishProductService : PublishProductService, IRSPublishProductService
    {
       
        private readonly IZnodeRepository<ZnodePublishProduct> _publishProductRepository;
        private readonly new IPublishProductHelper publishProductHelper;       
        private readonly ISEOService _seoService;


        #region Constructor
        public RSPublishProductService() : base()
        {

            _publishProductRepository = new ZnodeRepository<ZnodePublishProduct>();
            publishProductHelper = ZnodeDependencyResolver.GetService<IPublishProductHelper>();            
            _seoService = new SEOService();

        }
        #endregion

        /// <summary>
        /// Store Locations fetching functionality for Store Locator,PDP and Cart
        /// </summary>
        /// <param name="state"></param>
        /// <param name="sku"></param>
        /// <returns></returns>
        public DataTable GetStoreLocations(string state, string sku)
        {
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@State", state, ParameterDirection.Input, SqlDbType.VarChar);
            objStoredProc.GetParameter("@SKU", sku, ParameterDirection.Input, SqlDbType.VarChar);

            DataSet data = objStoredProc.GetSPResultInDataSet("RS_GetStoreAddressDetails");

            DataTable storeLocationTable = data.Tables[0];

            return storeLocationTable;
        }

        /// <summary>
        /// Store Locations fetching functionality for Store Locator,PDP and Cart
        /// </summary>
        /// <param name="state"></param>
        /// <param name="sku"></param>
        /// <returns></returns>
        public DataTable GetStoreLocationDetails(string state, string sku)
        {
            DataTable storeData = this.GetStoreLocations(state, sku);
            return storeData;
        }
                
        /// <summary>
        /// PDP Performance Implementation on Color,Size,Width Click
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="portalId"></param>
        /// <param name="localeId"></param>
        /// <param name="publishProductmodel"></param>
        /// <param name="associatedProducts"></param>
        /// <param name="ConfigurableAttributeCodes"></param>
        /// <param name="catalogVersionId"></param>
        /// <returns></returns>
        protected override PublishProductModel GetDefaultConfigurableProduct(NameValueCollection expands, int portalId, int localeId, PublishProductModel publishProductmodel, List<PublishProductModel> associatedProducts, List<string> ConfigurableAttributeCodes = null, int? catalogVersionId = 0)
        {
            //ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE - GETDEFAULTCONFIGURABLEPRODUCT BASE CALL STARTED AT" + DateTime.Now, "PDP", TraceLevel.Error);
            PublishProductModel publishProduct = base.GetDefaultConfigurableProduct(expands, portalId, localeId, publishProductmodel, associatedProducts, ConfigurableAttributeCodes, catalogVersionId);
           // ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE - GETDEFAULTCONFIGURABLEPRODUCT BASE CALL ENDED and NIVI CODE STARTED AT" + DateTime.Now, "PDP", TraceLevel.Error);
            string _swatchImagePath = ConfigurationManager.AppSettings["BaseImagePath"].ToString() + "t_40/";
            
            List<AssociatedProductsModel> _products = (from p in associatedProducts

                                                       select new AssociatedProductsModel
                                                       {
                                                           PublishProductId = p.ConfigurableProductId != 0 ? p.ConfigurableProductId : p.PublishProductId,
                                                           SKU = p.ConfigurableProductSKU != null ? p.ConfigurableProductSKU : p.SKU,
                                                           OMSColorCode = p.Attributes.Where(x => x.AttributeCode == "BaseGalleryImage1")?.FirstOrDefault()?.AttributeValues + "||" + p.Attributes.Where(x => x.AttributeCode == "BaseGalleryAltText1")?.FirstOrDefault()?.AttributeValues + "," +
                                                                          p.Attributes.Where(x => x.AttributeCode == "BaseGalleryImage2")?.FirstOrDefault()?.AttributeValues + "||" + p.Attributes.Where(x => x.AttributeCode == "BaseGalleryAltText2")?.FirstOrDefault()?.AttributeValues + "," +
                                                                          p.Attributes.Where(x => x.AttributeCode == "BaseGalleryImage3")?.FirstOrDefault()?.AttributeValues + "||" + p.Attributes.Where(x => x.AttributeCode == "BaseGalleryAltText3")?.FirstOrDefault()?.AttributeValues + "," +
                                                                          p.Attributes.Where(x => x.AttributeCode == "BaseGalleryImage4")?.FirstOrDefault()?.AttributeValues + "||" + p.Attributes.Where(x => x.AttributeCode == "BaseGalleryAltText4")?.FirstOrDefault()?.AttributeValues + ",",
                                                           OMSColorValue = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Color")?.SelectValues[0].Code,
                                                           //OMSColorPath = _swatchImagePath + (p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseGalleryImage2")?.AttributeValues=="" ? p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues : p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseGalleryImage2")?.AttributeValues),
                                                           OMSColorPath = _swatchImagePath + p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues,
                                                           DisplayOrder = Convert.ToInt32(p.DisplayOrder),
                                                           //OMSColorSwatchText = p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseGalleryImage2")?.AttributeValues == "" ? p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues : p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseGalleryImage2")?.AttributeValues,
                                                           OMSColorSwatchText =  p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues,
                                                           Custom1 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Size")?.SelectValues[0].Code,
                                                           Custom2 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "ShoeWidth")?.SelectValues[0].Code,
                                                           Custom3 = "|" + p.Attributes.FirstOrDefault(y => y.AttributeCode == "Color")?.SelectValues[0].Code + "-" +
                                                                    p.Attributes.FirstOrDefault(y => y.AttributeCode == "Size")?.SelectValues[0].Code + "-" +
                                                                    p.Attributes.FirstOrDefault(y => y.AttributeCode == "ShoeWidth")?.SelectValues[0].Code
                                                       }).ToList();
            // ZnodeLogging.LogMessage("RSPublishProductService -_swatch Image Path line PDP415" + _products, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            publishProduct.AssociatedProducts = new List<AssociatedProductsModel>();
            publishProduct.AssociatedProducts.AddRange(_products);
           // ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE - GETDEFAULTCONFIGURABLEPRODUCT NIVI CODE ENDED AT" + DateTime.Now, "PDP", TraceLevel.Error);
            return publishProduct;

        }

        /// <summary>
        /// Overridden to copy parent brand to child for schema.org
        /// </summary>
        /// <param name="publishProductId"></param>
        /// <param name="filters"></param>
        /// <param name="expands"></param>
        /// <returns></returns>      
        public override PublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
            //ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE -GETPUBLISHPRODUCT BASE CALL STARTED AT" + DateTime.Now, "PDP", TraceLevel.Error);
            PublishProductModel publishProduct = base.GetPublishProduct(publishProductId, filters, expands);
          //  ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE -GETPUBLISHPRODUCT BASE CALL ENDED AND NIVICODE STARTED AT" + DateTime.Now, "PDP", TraceLevel.Error);

            /*NIVI CODE TO COPY CONFIGURABLE PRODUCT ATTRIBUTE VALUES TO SIMPLE PRODUCT*/
            int portalId, localeId;
            int? catalogVersionId;
            List<PublishProductModel> products = GetPublishProductFromPublishedData(publishProductId, filters, out portalId, out localeId, out catalogVersionId, publishProduct, out products);
            if (products?.Count > 0)
            {
                try
                {
                    List<string> codesToBeCopied = new List<string>() { "IsMappedPrice", "PDPDesignTemplate", "AvaTaxCode", "IsAvailableOnlineOnly", "CallForPricing", "ProductSpecification", "FeatureDescription", "Highlights", "FitGuideLink", "FreeShipping", "ShippingCostRules", "Weight", "Height", "ShipSeparately", "PackageSizeFromRequest", "PackagingType", "MinimumQuantity", "MaximumQuantity", "OutOfStockOptions", "LongDescription", "Brand", "PDPBrandSEOUrl", "Gender","videourl360", "NewProduct","RSGender", "VideoURL" };
                    publishProduct.Attributes.RemoveAll(x => codesToBeCopied.Contains(x.AttributeCode));
                    List<PublishAttributeModel> parentAttributeModel = products[0].Attributes.Where(x => codesToBeCopied.Contains(x.AttributeCode)).ToList<PublishAttributeModel>();
                    publishProduct.Attributes.AddRange(parentAttributeModel);
                   
                }
                catch(Exception ex)
                {
                    ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE -Error" + ex.Message + ",StackTrace=" + ex.StackTrace, "PDP", TraceLevel.Error);
                }
            }
            //ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE -GETPUBLISHPRODUCT NIVICODE ENDED AT" + DateTime.Now, "PDP", TraceLevel.Error);
            /*NIVI CODE TO COPY CONFIGURABLE PRODUCT ATTRIBUTE VALUES TO SIMPLE PRODUCT*/

            return publishProduct;
        }

        


    }
}
