﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Api.Custom.LibrariesAdmin
{
    public class RSZnodeCheckout : ZnodeCheckout
    {
        private readonly IZnodeOrderHelper orderHelper;
        private readonly IPublishProductHelper publishProductHelper;
        private readonly Dictionary<int, string> publishCategory = new Dictionary<int, string>();

        public RSZnodeCheckout()
        {
            orderHelper = GetService<IZnodeOrderHelper>();
            publishProductHelper = GetService<IPublishProductHelper>();
        }

        public RSZnodeCheckout(UserAddressModel userAccount, ZnodePortalCart shoppingCart)
        {
            this.UserAccount = userAccount;
            this.ShoppingCart = shoppingCart;
            orderHelper = GetService<IZnodeOrderHelper>();
            publishProductHelper = GetService<IPublishProductHelper>();
        }

        
        /// <summary>
        /// Overridden to save ProductImage in Database line item to be used in Email Template
        /// </summary>
        /// <param name="order"></param>
        /// <param name="addressCart"></param>
        public override  void SetOrderLineItems(ZnodeOrderFulfillment order, ZnodeMultipleAddressCart addressCart)
        {
            GetDistinctCategoryIdsforCartItem(addressCart);
            List<Libraries.Data.DataModel.ZnodeOmsOrderLineItem> lineItemShippingDateList = orderHelper.GetLineItemShippingDate(order.Order.OmsOrderDetailsId);
            DateTime? ShipDate = DateTime.Now;
            // loop through cart and add line items
            foreach (ZnodeShoppingCartItem shoppingCartItem in addressCart.ShoppingCartItems)
            {
                OrderLineItemModel orderLineItem;

                if (string.IsNullOrEmpty(shoppingCartItem.GroupId))
                {
                    orderLineItem = order.OrderLineItems.FirstOrDefault(oli => oli.GroupId == shoppingCartItem.GroupId && oli.Sku == shoppingCartItem.SKU && shoppingCartItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Simple);
                }
                else
                {
                    orderLineItem = order.OrderLineItems.FirstOrDefault(oli => oli.GroupId == shoppingCartItem.GroupId && oli.Sku == shoppingCartItem.Product.SKU && shoppingCartItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Simple);
                }

                bool addNewOrderItem = false;
                if (IsNull(orderLineItem))
                {
                    addNewOrderItem = true;
                    orderLineItem = new OrderLineItemModel();
                    orderLineItem.OmsOrderShipmentId = addressCart.OrderShipmentID;
                    if (string.IsNullOrEmpty(shoppingCartItem.Product.ShoppingCartDescription) || string.IsNullOrWhiteSpace(shoppingCartItem.Product.ShoppingCartDescription))
                        if (string.IsNullOrEmpty(shoppingCartItem.Product.Description) || string.IsNullOrWhiteSpace(shoppingCartItem.Product.Description))
                            orderLineItem.Description = shoppingCartItem.Description;
                        else
                            orderLineItem.Description = shoppingCartItem.Product.Description;
                    else
                        orderLineItem.Description = shoppingCartItem.Product.ShoppingCartDescription;
                    orderLineItem.ProductName = shoppingCartItem.Product.Name;
                    orderLineItem.Sku = shoppingCartItem.Product.SKU;
                    orderLineItem.Quantity = ((shoppingCartItem?.Product?.ZNodeConfigurableProductCollection.Count > 0) || (shoppingCartItem?.Product?.ZNodeGroupProductCollection.Count > 0)) ? 0 : shoppingCartItem.Quantity;
                    orderLineItem.Price = (shoppingCartItem?.Product?.ZNodeConfigurableProductCollection.Count > 0) ? 0 : GetParentProductPrice(shoppingCartItem);
                    orderLineItem.DiscountAmount = GetLineItemDiscountAmount(shoppingCartItem.Product.DiscountAmount, shoppingCartItem.Quantity);
                    orderLineItem.ShipSeparately = shoppingCartItem.Product.ShipSeparately;
                    orderLineItem.ParentOmsOrderLineItemsId = null;
                    /*NIVI Code*/
                    //orderLineItem.DownloadLink = shoppingCartItem.Product.DownloadLink;
                    orderLineItem.DownloadLink = shoppingCartItem.Image;
                    /*NIVI Code*/
                    orderLineItem.GroupId = shoppingCartItem.GroupId;
                    orderLineItem.IsActive = true;
                    orderLineItem.Vendor = shoppingCartItem.Product.VendorCode;
                    orderLineItem.OrderLineItemStateId = shoppingCartItem.OrderStatusId;
                    orderLineItem.IsItemStateChanged = shoppingCartItem.IsItemStateChanged;
                    if (string.Equals(shoppingCartItem.OrderStatus, ZnodeOrderStatusEnum.SHIPPED.ToString(), StringComparison.OrdinalIgnoreCase) && shoppingCartItem.IsItemStateChanged)
                        orderLineItem.ShipDate = ShipDate;
                    else if (string.Equals(shoppingCartItem.OrderStatus, ZnodeOrderStatusEnum.SHIPPED.ToString(), StringComparison.OrdinalIgnoreCase))
                        orderLineItem.ShipDate = lineItemShippingDateList.Count > 0 ? lineItemShippingDateList.Find(x => x.OmsOrderLineItemsId == shoppingCartItem.OmsOrderLineItemId).ShipDate : null;
                    if (!string.IsNullOrEmpty(shoppingCartItem.TrackingNumber))
                        orderLineItem.TrackingNumber = shoppingCartItem.TrackingNumber;
                    //to apply custom tax/shipping cost
                    orderLineItem.IsLineItemShippingCostEdited = order.IsShippingCostEdited;
                    orderLineItem.IsLineItemTaxCostEdited = order.IsTaxCostEdited;
                    orderLineItem.PartialRefundAmount = shoppingCartItem.PartialRefundAmount;
                    //Assign Auto-add-on SKUs.
                    orderLineItem.AutoAddonSku = string.IsNullOrEmpty(shoppingCartItem.AutoAddonSKUs) ? null : shoppingCartItem.AutoAddonSKUs;

                    //to set order line item attributes
                    orderLineItem.Attributes = SetLineItemAttributes(shoppingCartItem?.Product?.Attributes, shoppingCartItem?.Product.ProductCategoryIds);

                    // then make a shipping cost entry in orderlineItem table.             
                    orderLineItem.ShippingCost = orderLineItem.IsLineItemShippingCostEdited ? 0 : shoppingCartItem.ShippingCost;

                    //Set order tax to order line item.
                    orderLineItem.HST = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.HST;
                    orderLineItem.PST = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.PST;
                    orderLineItem.GST = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.GST;
                    orderLineItem.VAT = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.VAT;
                    orderLineItem.SalesTax = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.SalesTax;
                    orderLineItem.TaxTransactionNumber = shoppingCartItem.TaxTransactionNumber;
                    orderLineItem.TaxRuleId = shoppingCartItem.TaxRuleId;
                    orderLineItem.IsConfigurableProduct = shoppingCartItem.Product.ZNodeConfigurableProductCollection.Count > 0 ? true : false;
                    orderLineItem.Custom1 = shoppingCartItem.Custom1;
                    orderLineItem.Custom2 = shoppingCartItem.Custom2;
                    orderLineItem.Custom3 = shoppingCartItem.Custom3;
                    orderLineItem.Custom4 = shoppingCartItem.Custom4;
                    orderLineItem.Custom5 = shoppingCartItem.Custom5;

                    if (shoppingCartItem.Product.RecurringBillingInd)
                    {
                        orderLineItem.RecurringBillingAmount = shoppingCartItem.Product.RecurringBillingInitialAmount;
                        orderLineItem.RecurringBillingCycles = shoppingCartItem.Product.RecurringBillingTotalCycles;
                        orderLineItem.RecurringBillingFrequency = shoppingCartItem.Product.RecurringBillingFrequency;
                        orderLineItem.RecurringBillingPeriod = shoppingCartItem.Product.RecurringBillingPeriod;
                        orderLineItem.IsRecurringBilling = true;
                    }

                    orderLineItem.OrdersDiscount = shoppingCartItem.Product.OrdersDiscount;
                    //To add personalize attribute list
                    orderLineItem.PersonaliseValueList = shoppingCartItem.PersonaliseValuesList;
                    orderLineItem.PersonaliseValuesDetail = shoppingCartItem.PersonaliseValuesDetail;
                    orderLineItem.AdditionalCost = shoppingCartItem.AdditionalCost;
                    orderLineItem.OmsOrderLineItemsId = shoppingCartItem.OmsOrderLineItemId;
                }

                AddSimpleItemInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add add-on items in order line item
                AddAddOnsItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add bundle items in order line item
                AddBundleItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add Configurable item in order line item                
                AddConfigurableItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add Group item in order line item
                AddGroupItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //To add personalise attribute list
                orderLineItem.PersonaliseValueList = shoppingCartItem.PersonaliseValuesList;

                if (addNewOrderItem)
                    order.OrderLineItems.Add(orderLineItem);
            }
        }

        /// <summary>
        /// Overridden to send Selected WarehouseID as Parameter for Storewise Inventory deduction
        /// </summary>
        /// <param name="order"></param>
        /// <param name="shoppingCart"></param>
        /// <returns></returns>
        public override bool ManageOrderInventory(ZnodeOrderFulfillment order, ZnodePortalCart shoppingCart)
        {
            bool isSuccess = false;
            if (IsNotNull(order) && order.OrderLineItems.Count() > 0)
            {
                OrderWarehouseModel orderWarehouse = new OrderWarehouseModel();
                orderWarehouse.OrderId = order.Order.OmsOrderDetailsId;
                orderWarehouse.UserId = order.UserID;
                orderWarehouse.PortalId = order.PortalId;

                List<OrderWarehouseLineItemsModel> skusInventory = SetSKUInventorySetting(shoppingCart);
                // List<OrderLineItemModel> orderLineItems = order.OrderLineItems.Where(x => x.Custom1 != "").ToList();
                foreach (OrderLineItemModel item in order.OrderLineItems)
                {
                    bool allowBackOrder;
                    string inventoryTracking = GetInventoryTrackingBySKU(item.Sku, skusInventory, out allowBackOrder);
                    orderWarehouse.LineItems.Add(new OrderWarehouseLineItemsModel { OrderLineItemId = item.OmsOrderLineItemsId, SKU = item.Sku, Quantity = item.Quantity, InventoryTracking = inventoryTracking, WarehouseId = item.Custom1 == "" ? 0 : Convert.ToInt32(item.Custom1) });

                    foreach (OrderLineItemModel childitem in item.OrderLineItemCollection)
                    {
                        if (childitem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Bundles)
                        {
                            inventoryTracking = GetInventoryTrackingBySKU(childitem.Sku, skusInventory,out allowBackOrder);
                            orderWarehouse.LineItems.Add(new OrderWarehouseLineItemsModel { OrderLineItemId = childitem.OmsOrderLineItemsId, SKU = childitem.Sku, Quantity = childitem.Quantity, InventoryTracking = inventoryTracking, WarehouseId = childitem.Custom1 == "" ? 0 : Convert.ToInt32(childitem.Custom1) });

                        }
                    }
                }
                List<OrderWarehouseLineItemsModel> productInventoryList = new List<OrderWarehouseLineItemsModel>();
                 string savedCartLineItemXML = HelperUtility.ToXML(orderWarehouse.LineItems);
                isSuccess = orderHelper.ManageOrderInventory(orderWarehouse,out productInventoryList);
                GetLowInventoryProducts(shoppingCart, productInventoryList);
            }

            // }
            return isSuccess;

        }

        /// <summary>
        /// Overridden to copy DOwnload link image to all the line items 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="shoppingCartModel"></param>
        /// <param name="isTaxCostUpdated"></param>
        /// <returns></returns>
        public override ZnodeOrderFulfillment SubmitOrder(SubmitOrderModel model, ShoppingCartModel shoppingCartModel, bool isTaxCostUpdated = true)
        {
            int portalID = ShoppingCart.PortalID;
            int orderId = model?.OrderId.GetValueOrDefault() ?? 0;
            int orderDetailId = 0;
            ZnodeOrderFulfillment order = this.GetOrderFullfillment(this.UserAccount, this.ShoppingCart, portalID);

            order.Order.OmsOrderDetailsId = model?.OmsOrderDetailsId == null ? 0 : Convert.ToInt32(model?.OmsOrderDetailsId);
            order.AccountNumber = shoppingCartModel?.Shipping?.AccountNumber;
            order.ShippingMethod = shoppingCartModel?.Shipping?.ShippingMethod;
            if (IsNotNull(shoppingCartModel?.Payment?.PaymentStatusId))
                order.PaymentStatusID = shoppingCartModel.Payment.PaymentStatusId;

            if (orderId > 0)
                SetOrderStateTrackingNumber(order, model);

            //start transaction
            using (SqlConnection connection = new SqlConnection(Libraries.Data.Helpers.HelperMethods.ConnectionString))
            {
                connection.Open();     // create order object
                SqlTransaction transaction = connection.BeginTransaction();// Start a local transaction.

                try
                {
                    if (orderId > 0 && !CancelExistingOrder(order, orderId))
                        return order;

                    SetOrderAdditionalDetails(order, model);

                    bool paymentIsSuccess = SetPaymentDetails(order);
                    order.Order.OrderNumber = model?.OrderNumber;
                    /*NIVI CODE*/
                    order.OrderLineItems.ForEach(x => x.OrderLineItemCollection[0].DownloadLink = x.DownloadLink);
                    // Add the order and line items to database
                    order.AddOrderToDatabase(order, shoppingCartModel);
                    if (orderId > 0)
                    {
                        //to save return items in data base for selected order
                        if (IsNotNull(model?.ReturnOrderLineItems))
                        {
                            this.IsSuccess = SaveReturnItems(order.Order.OmsOrderDetailsId, model.ReturnOrderLineItems);
                        }
                        //ZnodeLogging.LogMessage($"Updated existing order for Order Id:{orderId }", ZnodeLogging.Components.OMS.ToString());
                    }

                    //to get current orderdetailid for verifing  order process
                    orderDetailId = order?.Order?.OmsOrderDetailsId ?? 0;

                    //Set Order Shipment Details to order Line Item.
                    SetOrderShipmentDetails(order);

                    if (paymentIsSuccess)
                    {
                        SetOrderDetailsToShoppingCart(order);
                        //to apply promotion, taxes and shipping calculation 
                        this.ShoppingCart.PostSubmitOrderProcess(orderId, shoppingCartModel.ShippingAddress.IsGuest, isTaxCostUpdated);

                        int? userId = this.ShoppingCart.GetUserId();

                        //to save referral commission and gift card history
                        this.SaveReferralCommissionAndGiftCardHistory(order, userId);

                        //reduce product inventory this code block move out of transaction because all db call of entity type and this is sp call
                        if (this.ManageOrderInventory(order, this.ShoppingCart))
                        {
                            transaction.Commit();
                            this.IsSuccess = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            this.IsSuccess = false;
                        }
                    }
                    else
                    {
                        //// payment submission failed so rollback transaction
                        transaction.Rollback();
                        this.IsSuccess = false;
                    }
                }
                catch (Exception ex)
                {
                   // ZnodeLogging.LogMessage(ex.Message, string.Empty, TraceLevel.Error, ex);

                    if (IsNotNull(ex.InnerException))
                       // ZnodeLogging.LogMessage(ex.InnerException.ToString(), string.Empty, TraceLevel.Error); // log exception

                    transaction.Rollback();
                    this.IsSuccess = false;

                    VerifySubmitOrderProcess(orderDetailId);

                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            VerifySubmitOrderProcess(orderDetailId);

            return order;
        }
    }
}
