﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Helpers;
using Znode.Engine.WebStore.Maps;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;


namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSWidgetDataAgent : WidgetDataAgent//, IRSWidgetDataAgent
    {
        private readonly IShoppingCartClient _shoppingCartsClient;
        private readonly ICartAgent _cartAgent;
        private readonly IWebstoreHelper _webstoreHelper;
        public RSWidgetDataAgent(IWebStoreWidgetClient widgetClient, IPublishProductClient productClient, IPublishCategoryClient publishCategoryClient, IBlogNewsClient blogNewsClient, IContentPageClient contentPageClient, ISearchClient searchClient, ICMSPageSearchClient cmsPageSearchClient) :
            base( widgetClient, productClient,publishCategoryClient,blogNewsClient, contentPageClient,searchClient,cmsPageSearchClient)
        {
            _cartAgent = new CartAgent(GetClient<ShoppingCartClient>(), GetClient<PublishProductClient>(), GetClient<AccountQuoteClient>(), GetClient<UserClient>());
            _shoppingCartsClient = GetClient<ShoppingCartClient>();
            _webstoreHelper = ZnodeDependencyResolver.GetService<IWebstoreHelper>();
        }

        public CartViewModel GetCart()
        {
            //Get Shopping cart from session or cookie.
            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                        _cartAgent.GetCartFromCookie();

            if (shoppingCartModel!=null)
            {
                return new CartViewModel()
                {
                    HasError = true,
                    ErrorMessage = WebStore_Resources.OutofStockMessage
                };
            }

            if (shoppingCartModel?.ShoppingCartItems?.Count == 0)
                return new CartViewModel();

            //Remove Shipping and Tax calculation From Cart.

            //Calculate cart
            shoppingCartModel = CalculateCart(shoppingCartModel);

            //Set currency details.
            return SetCartCurrency(shoppingCartModel);
        }

        //Set currecy details for shopping cart and cart items.
        private CartViewModel SetCartCurrency(ShoppingCartModel cartModel)
        {
            if (cartModel != null)
            {
                CartViewModel cartViewModel = cartModel.ToViewModel<CartViewModel>();
                cartViewModel.ShoppingCartItems?.ForEach(item => item.GroupProducts.ForEach(y => y.Quantity = (y.Quantity)));
                string currencyCode = PortalAgent.CurrentPortal.CurrencyCode;
                cartViewModel.CurrencyCode = currencyCode;
                cartViewModel.ShoppingCartItems.Select(x => { x.CurrencyCode = currencyCode; return x; })?.ToList();
                return cartViewModel;
            }
            return new CartViewModel();
        }
        //Calculate cart.
        private ShoppingCartModel CalculateCart(ShoppingCartModel shoppingCartModel)
        {
            if (shoppingCartModel != null)
            {
                shoppingCartModel.LocaleId = PortalAgent.LocaleId;
                string billingEmail = shoppingCartModel.BillingEmail;
                int shippingId = shoppingCartModel.ShippingId;
                int billingAddressId = shoppingCartModel.BillingAddressId;
                int shippingAddressId = shoppingCartModel.ShippingAddressId;
                int selectedAccountUserId = shoppingCartModel.SelectedAccountUserId;
                shoppingCartModel = _shoppingCartsClient.Calculate(shoppingCartModel);
                //Bind required data to ShoppingCartModel.
                BindShoppingCartData(shoppingCartModel, billingEmail, shippingId, shippingAddressId, billingAddressId, selectedAccountUserId);
            }

            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            return shoppingCartModel;
        }
        //Bind required data to ShoppingCartModel.
        private void BindShoppingCartData(ShoppingCartModel shoppingCartModel, string billingEmail, int shippingId, int shippingAddressId, int billingAddressId, int selectedAccountUserId)
        {
            shoppingCartModel.BillingEmail = billingEmail;
            shoppingCartModel.ShippingId = shippingId;
            shoppingCartModel.ShippingAddressId = shippingAddressId;
            shoppingCartModel.BillingAddressId = billingAddressId;
            shoppingCartModel.SelectedAccountUserId = selectedAccountUserId;
        }
        //Get sub categories of selected category
        public virtual new List<RSCategoryViewModel> GetSubCategories(WidgetParameter widgetparameter)
        {
            return new List<RSCategoryViewModel>();
        }
        //    // ZnodeLogging.LogMessage("RSWIDGETDATAAGENT GetDefaultStoreDetailsFromCookie() GetSubCategories called", "WIDGET", TraceLevel.Error);
        //    IPublishCategoryClient _categoryClient = new PublishCategoryClient();
        //    //Sorting For Category List.
        //    SortCollection sorts = new SortCollection();
        //    sorts.Add(SortKeys.DisplayOrder, SortDirections.Ascending);
        //    List<RSCategoryViewModel> rsCategoryList = new List<RSCategoryViewModel>();
        //    ZnodeLogging.LogMessage("RSWIDGETDATAAGENT _categoryClient.GetPublishCategoryList begins at" + DateTime.Now, "WIDGET", TraceLevel.Error);
        //    PublishCategoryListModel model = _categoryClient.GetPublishCategoryList(new ExpandCollection { ZnodeConstant.SEO }, GetFilterForSubCategory(widgetparameter), null);
        //    ZnodeLogging.LogMessage("RSWIDGETDATAAGENT model?.PublishCategories?.Count()=" + model?.PublishCategories?.Count(), "WIDGET", TraceLevel.Error);
        //    if (model?.PublishCategories?.Count > 0)
        //    {
        //        ZnodeLogging.LogMessage("RSWIDGETDATAAGENT foreach start time=" + DateTime.Now, "WIDGET", TraceLevel.Error);
        //        foreach (PublishCategoryModel pcm in model.PublishCategories)
        //        {
        //            RSCategoryViewModel rcvm = new RSCategoryViewModel();
        //            rcvm.CategoryId = pcm.PublishCategoryId;
        //            rcvm.CategoryName = pcm.Name;
        //            rcvm.SEOUrl = pcm.SEODetails?.SEOUrl;
        //            rcvm.ProductIdCount = pcm.ProductIds.Count();
        //            rsCategoryList.Add(rcvm);
        //        }
        //        model.PublishCategories.ForEach(d =>
        //        {
        //            RSCategoryViewModel rcvm = new RSCategoryViewModel();
        //            rcvm.CategoryId = d.PublishCategoryId;
        //            rcvm.CategoryName = d.Name;
        //            rcvm.SEOUrl = d.SEODetails?.SEOUrl;
        //            rcvm.ProductIdCount = d.ProductIds.Count();
        //            rsCategoryList.Add(rcvm);
        //        });

        //        ZnodeLogging.LogMessage("RSWIDGETDATAAGENT foreach end time=" + DateTime.Now, "WIDGET", TraceLevel.Error);
        //    }
        //    if (rsCategoryList.Count > 0 && widgetparameter.WidgetKey == "123")
        //    {
        //        int ProductCount = rsCategoryList.Sum(x => x.ProductIds.Count());
        //        rsCategoryList.RemoveRange(1, rsCategoryList.Count() - 1);
        //        rsCategoryList[0].CategoryName = widgetparameter.properties["CategoryName"].ToString();
        //        rsCategoryList[0].CategoryId = Convert.ToInt32(widgetparameter.properties["CategoryId"]);
        //        rsCategoryList[0].SEOUrl = widgetparameter.properties["SEO"].ToString();
        //        rsCategoryList[0].ProductIdCount = ProductCount;

        //        rsCategoryList[0].ParentCategoryName = widgetparameter.properties["PCategoryName"].ToString();
        //        rsCategoryList[0].ParentCategoryId = Convert.ToInt32(widgetparameter.properties["CategoryId"]);
        //        rsCategoryList[0].ParentSEODetails = widgetparameter.properties["SEO"].ToString();
        //        widgetparameter.CMSMappingId = Convert.ToInt32(widgetparameter.properties["PCategoryId"]);
        //        widgetparameter.DisplayName = widgetparameter.properties["PCategoryName"].ToString();
        //        PublishCategoryListModel Parentmodel = _categoryClient.GetPublishCategoryList(new ExpandCollection { ZnodeConstant.SEO }, GetFilterForSubCategory(widgetparameter), null);
        //        List<RSCategoryViewModel> ParentCategoryList;
        //        ParentCategoryList = Parentmodel?.PublishCategories?.Count > 0 ? Parentmodel.PublishCategories.ToViewModel<RSCategoryViewModel>().ToList() : new List<RSCategoryViewModel>();
        //        rsCategoryList[0].ParentProductIdCount = ParentCategoryList.Sum(x => x.ProductIds.Count());

        //    }
        //    //ZnodeLogging.LogMessage("RSWIDGETDATAAGENT rsCategoryList.Count="++""+, "WIDGET", TraceLevel.Error);
        //    if (rsCategoryList.Count > 0 && widgetparameter.WidgetKey == "122")
        //    {
        //        rsCategoryList[0].ParentCategoryName = widgetparameter.properties["CategoryName"].ToString();
        //        rsCategoryList[0].ParentCategoryId = Convert.ToInt32(widgetparameter.properties["CategoryId"]);
        //        rsCategoryList[0].ParentSEODetails = Convert.ToString(widgetparameter.properties["SEO"]);
        //        rsCategoryList[0].ParentProductIdCount = rsCategoryList.Sum(x => x.ProductIdCount);
        //    }
        //    return rsCategoryList;
        //}
        ////Get Filter For SubCategory.
        //private FilterCollection GetFilterForSubCategory(WidgetParameter widgetparameter)
        //{
        //    FilterCollection filters = GetRequiredFilters();
        //    filters.Add(WebStoreEnum.ZnodeParentCategoryIds.ToString(), FilterOperators.Contains, widgetparameter.CMSMappingId.ToString());
        //    return filters;
        //}

        //    public string GetDefaultStoreDetailsFromCookie()
        //{
        //    string defaultStore = GetFromCookie("DefaultSelectedStore");
        //    //ZnodeLogging.LogMessage("RSWIDGETDATAAGENT GetDefaultStoreDetailsFromCookie() GetFromCookie-" + defaultStore, "WIDGET", TraceLevel.Error);
        //    string[] storeValues = defaultStore.Split('*');
        //    string storeName = "";
        //    if (storeValues.Length > 1)
        //    {
        //        storeName = storeValues[1];
        //    }
        //    if (storeName == "")
        //        storeName = "Locate A Store";
        //    return storeName;
        //}

        //public override ProductListViewModel GetCategoryProducts(WidgetParameter widgetparameter, int pageNum = 1, int pageSize = 40, int sortValue = 0)
        //{
        //    /*NIVI CODE*/
        //    if (pageSize == 16)
        //        pageSize = 40;

        //    if ((Convert.ToInt32(widgetparameter.properties[WebStoreConstants.PageSize])) == 16)
        //        widgetparameter.properties[WebStoreConstants.PageSize] = 40;

        //    //  ZnodeLogging.LogMessage("RSWIDGETDATAAGENT base.GetCategoryProducts Begins at" + DateTime.Now, "PLP", TraceLevel.Error);
        //    ProductListViewModel productlist = base.GetCategoryProducts(widgetparameter, pageNum, pageSize, sortValue);
        //    // ZnodeLogging.LogMessage("RSWIDGETDATAAGENT base.GetCategoryProducts Ends at" + DateTime.Now, "PLP", TraceLevel.Error);
        //    return productlist;
        //}
        ////    public override ProductListViewModel GetCategoryProducts(WidgetParameter widgetparameter, int pageNum = 1, int pageSize = 16, int sortValue = 0)
        ////    {

        ////        ProductListViewModel productlist = new ProductListViewModel();
        ////        SearchRequestViewModel searchRequestModel = GetSearchRequestModel(widgetparameter);
        ////        searchRequestModel.IsFacetList = true;
        ////        //Set category parameter.
        ////        RemoveInSession(string.Format(WebStoreConstants.LastSelectedCategoryForPortal, PortalAgent.CurrentPortal.PortalId));
        ////        SaveInSession(string.Format(WebStoreConstants.LastSelectedCategoryForPortal, PortalAgent.CurrentPortal.PortalId), widgetparameter.CMSMappingId);

        ////        //Set paging parameters.
        ////        SetPagingParameters(widgetparameter, pageNum, pageSize, sortValue, searchRequestModel);

        ////        FilterCollection filters = GetFilterForProducts(widgetparameter);

        ////        KeywordSearchModel searchResult = GetSearchResult(searchRequestModel, filters);

        ////        GetSearchProducts(searchResult, productlist);

        ////        //Bind facet details.
        ////        productlist.SearchResultViewModel = BindFacet(searchResult, widgetparameter, searchRequestModel);

        ////        bool isEnableCompare = PortalAgent.CurrentPortal.EnableCompare;

        ////        productlist?.Products?.ForEach(x => { x.CategoryId = widgetparameter.CMSMappingId; x.CategoryName = widgetparameter.TypeOfMapping; x.EnableProductCompare = isEnableCompare; x.HighlightLists = _webstoreHelper.GetHighlightListFromAttributes(x.Attributes, x.SKU, x.PublishProductId); });

        ////        productlist.TotalProductCount = searchResult.TotalProductCount;
        ////        productlist.TotalCMSPageCount = searchResult.TotalCMSPageCount;
        ////        productlist.SuggestTerm = searchResult.SuggestTerm;

        ////        //Map search profile id for search report.
        ////        productlist.SearchProfileId = searchResult.SearchProfileId;
        ////        //Binding Page Number,use to redirect on PDP page only if page number eqs to 1. 
        ////        if (Convert.ToBoolean(widgetparameter.properties?.ContainsKey(ZnodeConstant.PageNumber)))
        ////            productlist.PageNumber = Convert.ToInt32(widgetparameter?.properties[ZnodeConstant.PageNumber]);

        ////        productlist.IsSearchFromSuggestions = searchResult.IsSearchFromSuggestion;
        ////        productlist.SearchKeyword = searchRequestModel.SearchTerm;

        ////        productlist.TotalPages = (searchRequestModel.PageSize.Equals(-1)) ? 1 : ((int)(searchResult.TotalProductCount + (searchRequestModel.PageSize - 1)) / searchRequestModel.PageSize).Value;
        ////        int productsCount = productlist.Products?.Count ?? 0;

        ////        productlist.SearchResultCountText = GetProductMessage(widgetparameter, searchRequestModel, searchResult, productsCount);

        ////        //Set Found Products count.
        ////        if (Equals(widgetparameter.TypeOfMapping, ZnodeConstant.Brand))
        ////            productlist.SearchTextName = widgetparameter.properties[ZnodeConstant.Brand].ToString();
        ////        else
        ////            productlist.SearchTextName = widgetparameter.TypeOfMapping;

        ////        ZnodeLogging.LogMessage("TotalProductCount:", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new { TotalProductCount = productlist?.TotalProductCount });
        ////        return productlist;
        ////    }
        ////    private void SetPagingParameters(WidgetParameter widgetparameter, int pageNum, int pageSize, int sortValue, SearchRequestViewModel searchRequestModel)
        ////    {
        ////        if (widgetparameter?.properties?.Count > 1)
        ////        {
        ////            SetPageSize(widgetparameter, pageSize, searchRequestModel);
        ////            SetSorting(widgetparameter, sortValue, searchRequestModel);
        ////            searchRequestModel.PageNumber = !Equals(Convert.ToInt32(widgetparameter.properties[WebStoreConstants.PageNumber]), 0) ? (Convert.ToInt32(widgetparameter.properties[WebStoreConstants.PageNumber])) : pageNum;
        ////        }
        ////        else
        ////        {
        ////            searchRequestModel.PageSize = pageSize;
        ////            searchRequestModel.PageNumber = pageNum;
        ////        }
        ////    }
        ////    private void SetPageSize(WidgetParameter widgetparameter, int pageSize, SearchRequestViewModel searchRequestModel)
        ////    {
        ////        //If widget parameter doesnot contain properties for paging, then get page size value from session else the default page size.
        ////        if ((HelperUtility.IsNull((widgetparameter.properties[WebStoreConstants.PageSize]))) || (Convert.ToInt32(widgetparameter.properties[WebStoreConstants.PageSize])) == 0)
        ////        {
        ////            if (HelperUtility.IsNotNull(GetFromSession<int?>(WebStoreConstants.PageSizeValue)))
        ////            {
        ////                searchRequestModel.PageSize = Convert.ToInt32(GetFromSession<int?>(WebStoreConstants.PageSizeValue));
        ////                widgetparameter.properties[WebStoreConstants.PageSize] = Convert.ToInt32(GetFromSession<int?>(WebStoreConstants.PageSizeValue));
        ////            }
        ////            else
        ////            {
        ////                if (widgetparameter.WidgetCode != "Facet")
        ////                    SaveInSession<int>(WebStoreConstants.PageSizeValue, pageSize);
        ////                searchRequestModel.PageSize = pageSize;
        ////            }
        ////        }
        ////        else
        ////        {
        ////            SaveInSession<int>(WebStoreConstants.PageSizeValue, Convert.ToInt32(widgetparameter.properties[WebStoreConstants.PageSize]));
        ////            searchRequestModel.PageSize = Convert.ToInt32(widgetparameter.properties[WebStoreConstants.PageSize]);
        ////        }
        ////    }

        ////    //Set PageSize for product grid for both category and search page.
        ////    private void SetSorting(WidgetParameter widgetparameter, int sortValue, SearchRequestViewModel searchRequestModel)
        ////    {
        ////        //If widget parameter doesnot contain properties for paging, then get page size value from session else the default page size.
        ////        if (HelperUtility.IsNull((widgetparameter.properties[WebStoreConstants.Sort])))
        ////        {
        ////            Dictionary<string, List<string>> searchValueList = GetFilterData();
        ////            if (HelperUtility.IsNotNull(GetFromSession<int?>(WebStoreConstants.SortValue)) && (searchValueList?.Count > 0))
        ////            {
        ////                searchRequestModel.Sort = Convert.ToInt32(GetFromSession<int>(WebStoreConstants.SortValue));
        ////                widgetparameter.properties[WebStoreConstants.Sort] = Convert.ToInt32(GetFromSession<int>(WebStoreConstants.SortValue));
        ////            }
        ////            else
        ////            {
        ////                if (widgetparameter.WidgetCode != "Facet")
        ////                    SaveInSession<int>(WebStoreConstants.SortValue, sortValue);
        ////            }
        ////        }
        ////        else
        ////        {
        ////            SaveInSession<int>(WebStoreConstants.SortValue, Convert.ToInt32(widgetparameter.properties[WebStoreConstants.Sort]));
        ////            searchRequestModel.Sort = Convert.ToInt32(widgetparameter.properties[WebStoreConstants.Sort]);
        ////        }
        ////    }
        ////    private SearchResultViewModel BindFacet(KeywordSearchModel searchResult, WidgetParameter parameter, SearchRequestViewModel searchRequestModel)
        ////    {
        ////        SearchResultViewModel viewModel = new SearchResultViewModel();

        ////        if (HelperUtility.IsNotNull(searchResult))
        ////        {
        ////            viewModel.Location = searchResult.Location;
        ////            viewModel.Facets = SearchMap.ToFacetViewModel(searchResult.Facets, parameter.CMSMappingId, parameter.TypeOfMapping, searchRequestModel.SearchTerm, searchRequestModel.BrandName);
        ////            viewModel.FacetFilters = SetFilterParameter(parameter.CMSMappingId, parameter.TypeOfMapping, parameter.properties, viewModel.Facets);
        ////            if (Equals(parameter.TypeOfMapping, ZnodeConstant.Brand))
        ////            {
        ////                viewModel.BrandId = parameter.CMSMappingId;
        ////                viewModel.BrandName = searchRequestModel.BrandName;

        ////            }
        ////            else
        ////            {
        ////                viewModel.CategoryId = parameter.CMSMappingId;
        ////                viewModel.SearchTerm = searchRequestModel.SearchTerm;
        ////            }
        ////            if (searchResult.Categories?.Count > 0)
        ////            {
        ////                viewModel.Categories = searchResult.Categories.ToViewModel<SearchCategoryViewModel>().ToList();
        ////                viewModel.Categories.ForEach(x => x.Categoryurl = "?SearchTerm=" + HttpUtility.UrlEncode(searchRequestModel.SearchTerm) + "&categoryId=" + x.CategoryId);
        ////            }
        ////        }
        ////        return viewModel;
        ////    }

        ////    private void GetSearchProducts(KeywordSearchModel searchResult, ProductListViewModel productlist)
        ////    {
        ////        //Get Product list data.
        ////        if (searchResult?.Products?.Count > 0)
        ////        {
        ////            productlist.Products = searchResult.Products?.Count > 0 ? searchResult.Products?.ToViewModel<ProductViewModel>()?.ToList() : new List<ProductViewModel>();
        ////        }
        ////    }
        ////    public override SortCollection GetSortForSearch(int sort)
        ////    {
        ////        switch (sort)
        ////        {
        ////            case (int)SortEnum.NameAToZ:
        ////                return new SortCollection() { { "ProductName", DynamicGridConstants.ASCKey } };
        ////            case (int)SortEnum.NameZToA:
        ////                return new SortCollection() { { "ProductName", DynamicGridConstants.DESCKey } };
        ////            case (int)SortEnum.PriceHighToLow:
        ////                return new SortCollection() { { "Price", DynamicGridConstants.DESCKey } };
        ////            case (int)SortEnum.PriceLowToHigh:
        ////                return new SortCollection() { { "Price", DynamicGridConstants.ASCKey } };
        ////            case (int)SortEnum.HighestRating:
        ////                return new SortCollection() { { "HighestRated", DynamicGridConstants.DESCKey } };
        ////            case (int)SortEnum.MostReviewed:
        ////                return new SortCollection() { { "MostReviewed", DynamicGridConstants.DESCKey } };
        ////            case (int)SortEnum.OutOfStock:
        ////                return new SortCollection() { { "OutOfStock", DynamicGridConstants.DESCKey } };
        ////            case (int)SortEnum.InStock:
        ////                return new SortCollection() { { "InStock", DynamicGridConstants.DESCKey } };
        ////            default:
        ////                return new SortCollection();
        ////        }
        ////    }
    }
}
