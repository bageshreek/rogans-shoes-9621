﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Maps;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSUserAgent : UserAgent, IUserAgent
    {     

        public RSUserAgent(ICountryClient countryClient, IWebStoreUserClient webStoreAccountClient, IWishListClient wishListClient, IUserClient userClient, IPublishProductClient productClient, ICustomerReviewClient customerReviewClient, IOrderClient orderClient,
          IGiftCardClient giftCardClient, IAccountClient accountClient, IAccountQuoteClient accountQuoteClient, IOrderStateClient orderStateClient, IPortalCountryClient portalCountryClient, IShippingClient shippingClient, IPaymentClient paymentClient, ICustomerClient customerClient, IStateClient stateClient, IPortalProfileClient portalProfileClient) :
            base(countryClient, webStoreAccountClient, wishListClient, userClient, productClient, customerReviewClient, orderClient, giftCardClient, accountClient, accountQuoteClient,
                orderStateClient, portalCountryClient, shippingClient, paymentClient, customerClient, stateClient, portalProfileClient)
        {
            
        }

        /// <summary>
        /// Overridden to save default store for user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override LoginViewModel Login(LoginViewModel model)
        {
            LoginViewModel loginViewModel;
            double cookiesexpiretime = Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]);
            try
            {
                model.PortalId = PortalAgent.CurrentPortal?.PortalId;
                //Authenticate the user credentials.
                UserModel userModel = _userClient.Login(UserViewModelMap.ToLoginModel(model), GetExpands());
                if (HelperUtility.IsNotNull(userModel))
                {
                    //Check of Reset password Condition.
                    if (HelperUtility.IsNotNull(userModel.User) && !string.IsNullOrEmpty(userModel.User.PasswordToken))
                    {
                        loginViewModel = UserViewModelMap.ToLoginViewModel(userModel);
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword, true, loginViewModel);
                    }
                    // Check user associated profiles.                
                    if (!userModel.Profiles.Any() && !userModel.IsAdminUser)
                    {
                        return ReturnErrorModel(WebStore_Resources.ProfileLoginFailedErrorMessage);
                    }
                    SetLoginUserProfile(userModel);
                    //Save the User Details in Session.
                    SaveInSession(WebStoreConstants.UserAccountKey, userModel.ToViewModel<UserViewModel>());
                    /*NIVI CODE START: TO SAVE DEFAULT STORE FOR USER*/

                    string defaultStore = GetFromCookie("DefaultSelectedStore");
                    string[] storeValues = defaultStore.Split('*');
                    if (storeValues.Length > 1)
                    {
                        userModel.Custom1 = storeValues[0];
                        userModel.Custom2 = storeValues[1];
                        userModel.Custom3 = storeValues[2];
                        this.UpdateProfile(userModel.ToViewModel<UserViewModel>(), true);
                        SaveInSession(WebStoreConstants.UserAccountKey, userModel.ToViewModel<UserViewModel>());
                    }
                    else
                    {
                        string defaultstorestring = userModel.Custom1 + "*" + userModel.Custom2 + "*" + userModel.Custom3;
                        RemoveCookie("DefaultSelectedStore");
                        SaveInCookie("DefaultSelectedStore", defaultstorestring, cookiesexpiretime);

                    }
                    HttpContext.Current.Session["FirstName"] = userModel.FirstName;
                    ///*Cart Count in header does not match number of items in cart as header as Cart Count in header is coming from session*/
                    //decimal? cartCount = GetFromSession<decimal?>(WebStoreConstants.CartCount);
                    //RemoveInSession(WebStoreConstants.CartCount);
                    /*NIVI CODE END: TO SAVE DEFAULT STORE FOR USER*/
                    return UserViewModelMap.ToLoginViewModel(userModel);
                }
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case 2://Error Code to Reset the Password for the first time login.
                        return ReturnErrorModel(ex.ErrorMessage, true);
                    case ErrorCodes.AccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorAccountLocked);
                    case ErrorCodes.LoginFailed:
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
                    case ErrorCodes.TwoAttemptsToAccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorTwoAttemptsRemain);
                    case ErrorCodes.OneAttemptToAccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorOneAttemptRemain);
                    default:
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
            }
            return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
        }

        /// <summary>
        /// Written as method is Private No option
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <param name="hasResetPassword"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        private LoginViewModel ReturnErrorModel(string errorMessage, bool hasResetPassword = false, LoginViewModel model = null)
        {
            if (HelperUtility.IsNull(model))
            {
                model = new LoginViewModel();
            }

            //Set Model Properties.
            model.HasError = true;
            model.IsResetPassword = hasResetPassword;
            model.ErrorMessage = string.IsNullOrEmpty(errorMessage) ? WebStore_Resources.InvalidUserNamePassword : errorMessage;
            return model;
        }
               

        /// <summary>
        /// Overridden for else block
        /// </summary>
        /// <param name="model"></param>
        /// <param name="webStoreUser"></param>
        /// <returns></returns>
        public override UserViewModel UpdateProfile(UserViewModel model, bool webStoreUser)
        {
            UserViewModel userViewModel = GetUserViewModelFromSession();

            if (HelperUtility.IsNotNull(userViewModel))
            {
                MapUserProfileData(model, userViewModel);
            }
            else/*Nivi code*/
            {
                userViewModel = model;
            }

            try
            {
                _userClient.UpdateUserAccountData(userViewModel.ToModel<UserModel>(), webStoreUser);
                SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);
                userViewModel.SuccessMessage = WebStore_Resources.SuccessProfileUpdated;
                Znode.Engine.WebStore.Helper.ClearCache($"UserAccountAddressList{userViewModel.UserId}");
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                return (UserViewModel)GetViewModelWithErrorMessage(userViewModel, WebStore_Resources.ErrorUpdateProfile);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return (UserViewModel)GetViewModelWithErrorMessage(userViewModel, WebStore_Resources.ErrorUpdateProfile);
            }
            return userViewModel;
        }       
                
        /// <summary>
        /// Created different overload for SetTrackingURL method to set Speedy Tracking URL
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="portalId"></param>
        /// <returns></returns>
        public override OrdersViewModel GetOrderDetails(int orderId, int portalId = 0)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, PortalAgent.CurrentPortal.PortalId.ToString());

            UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

            if (HelperUtility.IsNotNull(userViewModel) && orderId > 0)
            {
                OrderModel orderModel = portalId > 0 ? GetOrderBasedOnPortalId(orderId, portalId) : _orderClient.GetOrderById(orderId, SetExpandsForReceipt(), filters);

               if (orderModel.OmsOrderId > 0 || userViewModel.UserId == orderModel.UserId || ((orderModel.IsQuoteOrder || userViewModel?.AccountId.GetValueOrDefault() > 0)))
                {
                    List<OrderLineItemModel> orderLineItemListModel = new List<OrderLineItemModel>();

                    //Create new order line item model.
                    CreateSingleOrderLineItem(orderModel, orderLineItemListModel);

                    orderModel.OrderLineItems = orderLineItemListModel;
                    /*NIVI Code - Created different overload for SetTrackingURL*/
                    orderModel.TrackingNumber = SetTrackingURL(orderModel);
                    if (orderModel?.OrderLineItems?.Count > 0)
                    {
                        OrdersViewModel orderDetails = orderModel?.ToViewModel<OrdersViewModel>();
                        orderDetails.CurrencyCode = PortalAgent.CurrentPortal?.CurrencyCode;
                        orderDetails.CouponCode = orderDetails.CouponCode?.Replace("<br/>", ", ");
                        orderDetails?.OrderLineItems?.ForEach(x => x.UOM = orderModel?.ShoppingCartModel?.ShoppingCartItems?.Where(y => y.SKU == x.Sku).Select(y => y.UOM).FirstOrDefault());
                        /*Here*/
                        orderDetails?.OrderLineItems?.ForEach(x => x.TrackingNumber = SetTrackingUrl(x.TrackingNumber, orderModel.ShippingId));
                        return orderDetails;
                    }
                }
                return null;
            }
            return new OrdersViewModel();
        }
               
        /// <summary>
        /// Created New OverLoad for RS Specific SPEEDYTrackingUrl
        /// </summary>
        /// <param name="ordermodel"></param>
        /// <returns></returns>
        private string SetTrackingURL(OrderModel ordermodel)
        {
            string TrackingNumbers = "";
            try
            {
                //Get shipping type name based on provided shipping id
                string SPEEDY = Convert.ToString(ConfigurationManager.AppSettings["SPEEDYTrackingUrl"]);

                string[] shipping = ordermodel.Custom1?.Split(',');
                string[] TrackNumbers = ordermodel.TrackingNumber?.Split(',');
                //string shippingType = GetShippingType(ordermodel.ShippingId);
                int icount = 0;
                if (TrackNumbers?.Count() > 0)
                {
                    foreach (string shippingname in shipping)
                    {
                        if (ZnodeConstant.UPS == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + $"<a target=_blank href={ ZnodeApiSettings.UPSTrackingURL }>{TrackNumbers[icount]} </a >" + ",";
                        else if (ZnodeConstant.FedEx == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + $"<a target=_blank href={ ZnodeApiSettings.FedExTrackingURL }>{TrackNumbers[icount]} </a >" + ",";
                        else if (ZnodeConstant.USPS == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + $"<a target=_blank href={ ZnodeApiSettings.USPSTrackingURL }>{TrackNumbers[icount]} </a >" + ",";
                        else if ("SPEEDEE" == shippingname)
                            TrackingNumbers = TrackingNumbers + $"<a target=_blank href={ SPEEDY + TrackNumbers[icount]}>{TrackNumbers[icount]} </a >" + ",";
                        icount = icount + 1;
                    }
                    if (TrackingNumbers != "")
                    {
                        TrackingNumbers = TrackingNumbers.Remove(TrackingNumbers.Length - 1, 1);
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Shipping=" + Convert.ToString(ordermodel.Custom1) + " TrackingNo=" + ordermodel.TrackingNumber + "error =" + ex.Message + " stacktrace =" + ex.StackTrace, string.Empty, TraceLevel.Error);
            }
            return TrackingNumbers;
        }
        
    }
}
