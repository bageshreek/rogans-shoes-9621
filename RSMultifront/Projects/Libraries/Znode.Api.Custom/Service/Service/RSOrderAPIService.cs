﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Engine.Services.Constants;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.Api.Custom.Service.Service
{
    public class RSOrderAPIService : IRSOrderAPIService
    {
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _omsOrderRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _orderLineItemRepository;
        private IOrderService _orderService;
        public RSOrderAPIService(IOrderService orderService)
        {
            _omsOrderRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            _orderLineItemRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
            _orderService = orderService;
        }

        /// <summary>
        /// Call to SP
        /// </summary>
        /// <returns></returns>
        public virtual string ExecutePackage()
        {
            try
            {
                IZnodeViewRepository<string> objStoredProc = new ZnodeViewRepository<string>();
                IList<string> output=objStoredProc.ExecuteStoredProcedureList("Nivi_Execute_SSISJOB");
                return output[0];
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Updates Orders Status in bulk
        /// </summary>
        /// <param name="orderDetailListModel"></param>
        /// <returns></returns>
        public virtual bool UpdateOrderStatusMany(OrderLineItemDataListModel orderDetailListModel)
        {
            try
            {
                foreach (OrderLineItemDataModel orderDetailsModel in orderDetailListModel.OrderLineItemDetails)
                {
                    ZnodeOmsOrderLineItem model = _orderLineItemRepository.Table.FirstOrDefault(x => x.OmsOrderLineItemsId == orderDetailsModel.OmsOrderLineItemsId);
                    if (IsNotNull(model))
                    {
                        bool flag = UpdateOrderLineItem(model, orderDetailsModel);
                        if (flag)
                        {
                            int OrderId = _omsOrderRepository.Table.FirstOrDefault(x => x.OmsOrderDetailsId == model.OmsOrderDetailsId).OmsOrderId;
                            OrderModel orderModel = _orderService.GetOrderById(OrderId, null, GetOrderExpands());
                            SendOrderStatusEmail(orderModel);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Provides data to SendOrderReceipt method
        /// </summary>
        /// <param name="orderModel"></param>
        public void SendOrderStatusEmail(OrderModel orderModel)
        {
            foreach (OrderLineItemModel item in orderModel.OrderLineItems)
            {
                if (item.Custom4 == "Pick")
                {
                    item.OrderLineItemCollection.AddRange(orderModel.OrderLineItems.Where(x => x.ParentOmsOrderLineItemsId == item.OmsOrderLineItemsId && x.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.AddOns)?.ToList());
                }
            }

            orderModel.OrderLineItems.RemoveAll(x => x.ParentOmsOrderLineItemsId == null);

            string subject = string.Empty;
            SendOrderReceipt(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, subject, ZnodeConfigManager.SiteConfig.AdminEmail, string.Empty, orderModel.ReceiptHtml, false);
        }

        /// <summary>
        /// Sends Order Receipt mail
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="userEmailId"></param>
        /// <param name="subject"></param>
        /// <param name="senderEmail"></param>
        /// <param name="bccEmailId"></param>
        /// <param name="receiptHtml"></param>
        /// <param name="isEnableBcc"></param>
        /// <returns></returns>
        public bool SendOrderReceipt(int portalId, string userEmailId, string subject, string senderEmail, string bccEmailId, string receiptHtml, bool isEnableBcc = false)
        {
            bool isSuccess = false;
            try
            {
                //This method is used to send an email.
                ZnodeEmail.SendEmail(portalId, userEmailId, senderEmail, ZnodeEmail.GetBccEmail(isEnableBcc, portalId, bccEmailId), subject, receiptHtml, true);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Can not send the Order Receipt. Please verify the SMTP setting.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error, ex);
            }
            return isSuccess;
        }

        /// <summary>
        /// Gets Order Expands
        /// </summary>
        /// <returns></returns>
        public NameValueCollection GetOrderExpands()
        {
            NameValueCollection expands = new NameValueCollection();
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsPaymentState.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsPaymentState.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString().ToLower());
            expands.Add(ExpandKeys.ZnodeShipping, ExpandKeys.ZnodeShipping);
            expands.Add(ExpandKeys.ZnodeUser, ExpandKeys.ZnodeUser);
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsHistories.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsHistories.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString().ToLower());
            return expands;
        }

        /// <summary>
        /// updates TrackingNumber and OrderLineItemStateId
        /// </summary>
        /// <param name="model"></param>
        /// <param name="orderLineItemDetailModel"></param>
        /// <returns></returns>
        public bool UpdateOrderLineItem(ZnodeOmsOrderLineItem model, OrderLineItemDataModel orderLineItemDetailModel)
        {
            model.TrackingNumber = (!string.IsNullOrEmpty(Convert.ToString(orderLineItemDetailModel.TrackingNumber))) ? orderLineItemDetailModel.TrackingNumber : model.TrackingNumber;
            model.OrderLineItemStateId = (orderLineItemDetailModel.OrderLineItemStateId > 0) ? orderLineItemDetailModel.OrderLineItemStateId : model.OrderLineItemStateId;
            return _orderLineItemRepository.Update(model);
        }
    }
}
