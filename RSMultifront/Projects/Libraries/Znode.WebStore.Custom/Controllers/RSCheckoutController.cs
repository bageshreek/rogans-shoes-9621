﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.Models;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.WebStore.Core.Extensions;
//using Znode.WebStore.Custom.Extensions;
using Znode.WebStore.Custom.Helper;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.WebStore.Custom.Controllers
{
    [NoCacheAttribute]
    public class RSCheckoutController : CheckoutController
    {
        private readonly ICheckoutAgent _checkoutAgent;
        private readonly bool IsEnableSinglePageCheckout = PortalAgent.CurrentPortal.IsEnableSinglePageCheckout;
        private readonly IUserAgent _userAgent;       
        private readonly ICartAgent _cartAgent;
        private readonly string CheckoutReceipt = "CheckoutReciept";

        public RSCheckoutController(IUserAgent userAgent, ICheckoutAgent checkoutAgent, ICartAgent cartAgent, IPaymentAgent paymentAgent) : base(userAgent, checkoutAgent, cartAgent, paymentAgent)
        {
            _checkoutAgent = checkoutAgent;
            _userAgent = userAgent;           
            _cartAgent = cartAgent;
        }


        //Account address

        //public override ActionResult AccountAddress(int userid = 0, int quoteId = 0, int addressId = 0, string addressType = "")
        //{
        //    return PartialView("_BillingShippingAddress", _checkoutAgent.GetBillingShippingAddress(userid, quoteId > 0, addressType, addressId, 0, false));
        //}
        public virtual ActionResult AccountAddress(int userid = 0, int quoteId = 0, int addressId = 0, string addressType = "", bool isQuoteRequest = false)
        {
            return PartialView("_BillingShippingAddress", _checkoutAgent.GetBillingShippingAddress(userid, quoteId > 0, addressType, addressId, 0, false, isQuoteRequest));
        }



        /*Nivi Code To check CheckoutReceipt View*/
        [HttpGet]
        public override ActionResult SubmitOrder(OrdersViewModel order)
        {
            if (!Equals(order, null) && order.OmsOrderId > 0)
            {
                return ActionView("CheckoutReciept", order);
            }

            return RedirectToAction<HomeController>(x => x.Index());

        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public override ActionResult SubmitOrder(SubmitOrderViewModel submitOrderViewModel)
        {
            if (!TempData.ContainsKey(ZnodeConstant.BrowserRefresh))
            {
                TempData.Add(ZnodeConstant.BrowserRefresh, true);

                OrdersViewModel order = _checkoutAgent.SubmitOrder(submitOrderViewModel);

                if (HelperUtility.IsNotNull(order) && !order.HasError)
                {
                    order.Total = submitOrderViewModel.Total;
                    order.SubTotal = submitOrderViewModel.SubTotal;

                    // Below code is used for "PayPal Express" to redirect payment website.
                    if (!string.IsNullOrEmpty(submitOrderViewModel.PayPalReturnUrl) && !string.IsNullOrEmpty(submitOrderViewModel.PayPalCancelUrl) && HelperUtility.Equals(submitOrderViewModel.PaymentType?.ToLower(), ZnodeConstant.PayPalExpress.ToLower()))
                    {
                        TempData["Error"] = order.HasError;
                        TempData["ErrorMessage"] = order.ErrorMessage;
                        TempData.Remove(ZnodeConstant.BrowserRefresh);
                        return Json(new { responseText = order.PayPalExpressResponseText });
                    }

                    // Below code is used for "Amazon Pay" to redirect payment website.
                    if (!string.IsNullOrEmpty(submitOrderViewModel.AmazonPayReturnUrl) && !string.IsNullOrEmpty(submitOrderViewModel.AmazonPayCancelUrl) && HelperUtility.Equals(submitOrderViewModel.PaymentType?.ToLower(), ZnodeConstant.AmazonPay.ToLower()))
                    {
                        TempData.Remove(ZnodeConstant.BrowserRefresh);
                        return Json(new { responseText = Convert.ToString(order.PaymentStatus), responseToken = order.TrackingNumber });
                    }

                    // Below code is used, after payment success from "PayPal Express" return to "return url" of "PayPal Express" i.e. "SubmitPaypalOrder".
                    if (submitOrderViewModel.IsFromPayPalExpress)
                    {
                        TempData["Order"] = order;
                        TempData.Remove(ZnodeConstant.BrowserRefresh);
                        return Json(new { sucess = true });
                    }
                    // Below code is used, after payment success from "Amazon Pay" return to "return url" of "AmazonPay" i.e. "SubmitAmazonPayOrder".
                    if (submitOrderViewModel.IsFromAmazonPay)
                    {
                        TempData["Order"] = order;
                        TempData.Remove(ZnodeConstant.BrowserRefresh);
                        return Json(new { sucess = true });
                    }

                    if (!User.Identity.IsAuthenticated)
                    {
                        _userAgent.RemoveGuestUserSession();
                    }

                    _cartAgent.RemoveAllCartItems(order.OmsOrderId);

                    // Below code is used, for after successfully payment from "Credit Card" return receipt.
                    if (Equals(submitOrderViewModel?.PaymentType?.ToLower(), ZnodeConstant.CreditCard.ToLower()))
                    {
                        TempData.Remove(ZnodeConstant.BrowserRefresh);
                        return Json(new { receiptHTML = RenderRazorViewToString("CheckoutReciept", order), omsOrderId = order.OmsOrderId });
                    }

                    TempData.Remove(ZnodeConstant.BrowserRefresh);
                    TempData["OrderId"] = order.OmsOrderId;
                    return RedirectToAction<RSCheckoutController>(x => x.OrderCheckoutReceipt());
                }

                // Return error message, if payment through "Credit Card" raises any error.
                if (Equals(submitOrderViewModel?.PaymentType?.ToLower(), ZnodeConstant.CreditCard.ToLower()))
                {
                    TempData.Remove(ZnodeConstant.BrowserRefresh);
                    return Json(new { error = order.ErrorMessage });
                }

                // Return error message, if payment through "PayPal Express" raises any error.
                if (!string.IsNullOrEmpty(submitOrderViewModel.PayPalReturnUrl) && !string.IsNullOrEmpty(submitOrderViewModel.PayPalCancelUrl))
                {
                    TempData.Remove(ZnodeConstant.BrowserRefresh);
                    return Json(new { error = order.ErrorMessage, responseText = order.PaymentStatus });
                }

                // Return error message, if payment through "AmazonPay" raises any error.
                if (!string.IsNullOrEmpty(submitOrderViewModel.AmazonPayReturnUrl) && !string.IsNullOrEmpty(submitOrderViewModel.AmazonPayCancelUrl))
                {
                    TempData.Remove(ZnodeConstant.BrowserRefresh);
                    return Json(new { error = order.ErrorMessage, responseText = order.PaymentStatus });
                }
                SetNotificationMessage(GetErrorNotificationMessage(order.ErrorMessage));
            }

            TempData.Remove(ZnodeConstant.BrowserRefresh);
            return RedirectToAction<CheckoutController>(x => x.Index(true));
        }

        public override ActionResult OrderCheckoutReceipt()
        {
            int omsOrderId = Convert.ToInt32(_checkoutAgent.GetOrderIdFromCookie());
            if (omsOrderId > 0)
            {
                GetService<IWebstoreHelper>().SaveDataInCookie(WebStoreConstants.UserOrderReceiptOrderId, null, 1);

                OrdersViewModel order = _checkoutAgent.GetOrderViewModel(omsOrderId);
                return HelperUtility.IsNotNull(order) ? View(CheckoutReceipt,order) : RedirectToAction<HomeController>(x => x.Index());
            }
            return RedirectToAction<HomeController>(x => x.Index());
        }
    }
}
