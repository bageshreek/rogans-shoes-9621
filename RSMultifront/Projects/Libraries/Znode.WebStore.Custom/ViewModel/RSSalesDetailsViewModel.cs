﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
    public class RSSalesDetailsViewModel: BaseViewModel
    {
        public string SEOUrl { get; set; }
        public string SKU { get; set; }
        public string Brand { get; set; }
        public int ProductId { get; set; }
    }
}
