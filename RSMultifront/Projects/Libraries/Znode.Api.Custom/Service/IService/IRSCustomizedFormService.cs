﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Sample.Api.Model.RSCustomizedFormModel;

namespace Znode.Api.Custom.Service.Service
{
   public interface IRSCustomizedFormService
    {
        bool ScheduleATruck(ScheduleATruckModel model);

        bool ShoeFinder(ShoesFinderModel model);
    }
}
