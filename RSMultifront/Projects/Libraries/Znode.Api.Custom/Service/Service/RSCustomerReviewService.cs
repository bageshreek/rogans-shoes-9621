﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Framework.Business;
using System.Configuration;

using System.Collections;
using System.Net;
using System.IO;
using Znode.Libraries.ECommerce.Utilities;
using System.Data;
using System.Linq;
using System.Collections.Specialized;
using System.Web.Script.Serialization;

namespace Znode.Api.Custom.Service.Service

{
    public class RSCustomerReviewService: CustomerReviewService
    {
       
        string api_key = ConfigurationManager.AppSettings["GetResponseAPIKey"].ToString();
        string api_url = ConfigurationManager.AppSettings["GetResponseAPIURL"].ToString();
        string CampainName = ConfigurationManager.AppSettings["CampaignName"].ToString();
        private Hashtable ht = new Hashtable();
       

        private readonly IZnodeRepository<ZnodeCMSCustomerReview> _customerReviewRepository;
        #region Constructor
        public RSCustomerReviewService():base()
        {
            _customerReviewRepository = new ZnodeRepository<ZnodeCMSCustomerReview>();
        }
        #endregion

        /// <summary>
        /// GetResponse call
        /// </summary>
        /// <param name="modelReview"></param>
        /// <returns></returns>
        public override CustomerReviewModel Create(CustomerReviewModel modelReview)
        {
            try
            {
                if (api_key != null && api_url != null)
                {

                    //string newsLetterCampaign = CampainName();
                    string newsLetterCampaign = CampainName;
                    if (!string.IsNullOrEmpty(newsLetterCampaign))
                    {
                        string campaign_id = GetCampaignId(CampainName);
                        string contatcId = GetContactID(modelReview.UserName.Trim(), campaign_id);
                        string campaignID = ProductReviewGetResponse(api_key, api_url,
                            newsLetterCampaign, modelReview.Duration.Trim(), modelReview.UserName.Trim(), contatcId

                            , modelReview.ProductName, modelReview.Headline, modelReview.SEODescription, modelReview.SEOKeywords, modelReview.SEOTitle
                            , modelReview.Comments, modelReview.Status, modelReview.UserLocation,modelReview.SKU );
                    }
                }
                //modelReview.UserName = modelReview.Duration;
                string strUserEmail = modelReview.Duration + " / " + modelReview.UserName;
                modelReview.UserName = strUserEmail;
            }
           catch(Exception ex)
            {
                ZnodeLogging.LogMessage("RSCUSTOMERREVIEWSERVICE api_key=" + api_key + "api_url=" + api_url, "CustomerReview", TraceLevel.Error);
                ZnodeLogging.LogMessage("RSCUSTOMERREVIEWSERVICE Create() Error" + ex.Message + "StackTrace:"+ex.StackTrace, "CustomerReview", TraceLevel.Error);
            }
            return base.Create(modelReview);
        }

        /// <summary>
        /// Gets GetResponse CapaignId from GetResponse campaignName
        /// </summary>
        /// <param name="campaignName"></param>
        /// <returns></returns>
        public string GetCampaignId(string campaignName)
        {
            if (ht != null && ht.Count > 0)
            {
                if (ht[campaignName] != null)
                {
                    return ht[campaignName].ToString();
                }
            }
            string campaign_id = string.Empty;
            try
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                // get CAMPAIGN_ID of 'sample_marketing' campaign
                // new request object
                Hashtable _request = new Hashtable();
                _request["jsonrpc"] = "2.0";
                _request["id"] = 1;
                // set method name
                _request["method"] = "get_campaigns";
                // set conditions
                Hashtable operator_obj = new Hashtable();
                operator_obj["EQUALS"] = campaignName;
                Hashtable name_obj = new Hashtable();
                name_obj["name"] = operator_obj;
                // set params request object
                object[] params_array = { api_key, name_obj };
                _request["params"] = params_array;

                // send headers and content in one request
                // (disable 100 Continue behavior)
                System.Net.ServicePointManager.Expect100Continue = false;

                // initialize client
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
                request.Method = "POST";

                byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

                String response_string = null;
                // decode response to Json object
                Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;
                                
                    // get result
                Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;
               
                // get campaign id
                foreach (object key in result.Keys)
                {
                    campaign_id = key.ToString();
                }
                ht.Add(campaignName, campaign_id);
            }
            catch (Exception ex)
            {
                //DBHelper db = new DBHelper();
               // db.LogError(ex.ToString(), "GetCampaignId() Method", userId);
                //Log Exceptions
               // LogWriter.Write(appFilePath, "Exception Occur For GetCampaignId() Method UserID=" + userId + ":" + ex.ToString());

            }
            return campaign_id;
        }

        /// <summary>
        /// Gets GetResponse GetContactID from email and campaign_id
        /// </summary>
        /// <param name="email"></param>
        /// <param name="campaign_id"></param>
        /// <returns></returns>
        public string GetContactID(string email, string campaign_id )
        {            
            string contactID = "";
            try
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                // get CAMPAIGN_ID of 'sample_marketing' campaign
                // new request object
                Hashtable _request = new Hashtable();
                _request["jsonrpc"] = "2.0";
                _request["id"] = 3;

                // set method name
                _request["method"] = "get_contacts";

                Hashtable contact_params = new Hashtable();
                contact_params["campaigns"] = campaign_id;

                Hashtable contact_params1 = new Hashtable();
                contact_params1["EQUALS"] = email;

                Hashtable name_obj1 = new Hashtable();
                name_obj1["email"] = contact_params1;

                // set params request object
                object[] add_contact_params_array = { api_key, name_obj1, contact_params };

                _request["params"] = add_contact_params_array;

                // send headers and content in one request
                // (disable 100 Continue behavior)
                System.Net.ServicePointManager.Expect100Continue = false;

                // initialize client
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
                request.Method = "POST";

                byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

                string response_string = null;

                // decode response to Json object
                Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;

                // get result
                Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;


                // get campaign id
                foreach (object key in result.Keys)
                {
                    contactID = key.ToString();
                }
            }
            catch (Exception ex)
            {
                //DBHelper db = new DBHelper();
                //db.LogError(ex.ToString(), "GetContactID() Method", userId);
                //Log Exceptions
               // LogWriter.Write(appFilePath, "Exception Occur For GetCampaignId() Method UserID=" + userId + ":" + ex.ToString());
            }
            return contactID;
        }     

        /// <summary>
        /// Posts product Reviews in getResponse
        /// </summary>
        /// <param name="api_key"></param>
        /// <param name="api_url"></param>
        /// <param name="campaignName"></param>
        /// <param name="contactName"></param>
        /// <param name="subscriberEmail"></param>
        /// <param name="contatcId"></param>
        /// <param name="ProductName"></param>
        /// <param name="Review"></param>
        /// <param name="pros"></param>
        /// <param name="cons"></param>
        /// <param name="bestuses"></param>
        /// <param name="comment"></param>
        /// <param name="gender"></param>
        /// <param name="country"></param>
        /// <param name="SKU"></param>
        /// <returns></returns>
        public string ProductReviewGetResponse(string api_key, string api_url, string campaignName, string contactName, string subscriberEmail, string contatcId
            ,string ProductName,string Review ,string pros ,string cons ,string bestuses ,string comment ,string gender ,string country,string SKU
            )
        {
            ZnodeLogging.LogMessage("ProductReviewGetResponse begin.." + campaignName + "," + contactName + "-" + subscriberEmail + "-" + Review + "-" + pros + "-" + cons + "-" + bestuses + "-" + comment + "-" + gender + "-" + country + "-" + SKU, "RSCustomerReviewService", TraceLevel.Info);
            JavaScriptSerializer jss = new JavaScriptSerializer();
            // get CAMPAIGN_ID of 'sample_marketing' campaign
            // new request object
            Hashtable _request = new Hashtable();
            _request["jsonrpc"] = "2.0";
            _request["id"] = 1;
            // set method name
            _request["method"] = "get_campaigns";
            // set conditions
            Hashtable operator_obj = new Hashtable();
            operator_obj["EQUALS"] = campaignName;
            Hashtable name_obj = new Hashtable();
            name_obj["name"] = operator_obj;
            // set params request object
            object[] params_array = { api_key, name_obj };
            _request["params"] = params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
           // ZnodeLogging.LogMessage("ProductReviewGetResponse Before..HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);", "RSCustomerReviewService", TraceLevel.Info);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";
          //  ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 1", "RSCustomerReviewService", TraceLevel.Info);
            byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));
           // ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 2", "RSCustomerReviewService", TraceLevel.Info);
            String response_string = null;

            // decode response to Json object
            Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;
            //ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 4.1 " + jsonContent["error"], "RSCustomerReviewService", TraceLevel.Info);
            //ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 4.2 " + jsonContent["jsonrpc"], "RSCustomerReviewService", TraceLevel.Info);
            //ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 4.3 " + jsonContent["id"], "RSCustomerReviewService", TraceLevel.Info);
            //foreach (object key in jsonContent.Keys)
            //{
            //    ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 4.1 Key=" + key+",Vlaue="+ jsonContent[key], "RSCustomerReviewService", TraceLevel.Info);
            //}

            // get result
            Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;
           // ZnodeLogging.LogMessage("ProductReviewGetResponse STEP 5"+ jsonContent, "RSCustomerReviewService", TraceLevel.Info);
            string campaign_id = null;

            // get campaign id
            foreach (object key in result.Keys)
            {
                campaign_id = key.ToString();
            }

            // add contact to 'sample_marketing' campaign
            // new request object
            _request = new Hashtable();

            _request["jsonrpc"] = "2.0";
            _request["id"] = 2;

            // set method name
            //update contacts
            Hashtable contact_params = new Hashtable();
            if (string.IsNullOrEmpty(contatcId))
            {
                
                _request["method"] = "add_contact";
                contact_params["campaign"] = campaign_id;
                contact_params["name"] = contactName;
                contact_params["email"] = subscriberEmail;
            }
            else
            {
                _request["method"] = "set_contact_customs";
                contact_params["contact"] = contatcId;
            }

            string[] GenderReview = gender.Split('#');

            string splitGender = ""; string splitRecommend = ""; string splitgeturl = "";
            //try
            //{
            //    splitGender = GenderReview[0].ToString();
            //}
            //catch 
            //{ splitGender = ""; }
            //try
            //{
            //    splitRecommend = GenderReview[1].ToString();
            //}
            //catch
            //{ splitRecommend = ""; }
            //try
            //{
            //    splitgeturl = GenderReview[2].ToString();
            //}
            //catch
            //{ }           
            splitGender = GenderReview.Length > 0 ? Convert.ToString(GenderReview[0]) : "";
            splitRecommend = GenderReview.Length > 1 ? Convert.ToString(GenderReview[1]) : "";
            splitgeturl = GenderReview.Length > 2 ? Convert.ToString(GenderReview[2]) : "";
           
            Hashtable customProductName = new Hashtable();
            customProductName["name"] = "ProductName";
            customProductName["content"] = ProductName;

            Hashtable customReview = new Hashtable();
            customReview["name"] = "Review";
            customReview["content"] = Review;


            Hashtable customComment = new Hashtable();
            customComment["name"] = "comment";
            customComment["content"] = comment;

            Hashtable customCountry = new Hashtable();
            customCountry["name"] = "country";
            customCountry["content"] = country;

            Hashtable customgeturl = new Hashtable();
            customgeturl["name"] = "ProductURL";
            customgeturl["content"] = Convert.ToString(splitgeturl);

            /*multiple selections*/
            // object[] customs_array = { customGender, customRecommend, customProductName, customReview, customComment, customCountry };
            if(string.IsNullOrEmpty(pros))
            {
                pros="";
            }
            if (string.IsNullOrEmpty(cons))
            {
                cons = "";
            }
            if (string.IsNullOrEmpty(bestuses))
            {
                bestuses = "";
            }
            string[] Splitpros = pros.Split(',');
            string[] SplitCons = cons.Split(',');
            string[] SplitBestUses = bestuses.Split(',');

            int SplitprosLenghth = 0;
            int SplitConsLength = 0;
            int SplitBestUsesLength = 0;

            if (pros !=""){SplitprosLenghth = Splitpros.Length; }
            if (cons !="") { SplitConsLength = SplitCons.Length; }
            if (bestuses !="") { SplitBestUsesLength = SplitBestUses.Length; }


            Hashtable customGender = new Hashtable();

            int splitGenderlength = 0;
            if (!string.IsNullOrWhiteSpace(splitGender)) { splitGenderlength = 1;

               
                customGender["name"] = "gender";
                customGender["content"] = Convert.ToString(splitGender);                
            }
            Hashtable customRecommend = new Hashtable();
            int splitRecommendlength = 0;
            if (!string.IsNullOrWhiteSpace(splitRecommend)) {
                splitRecommendlength = 1;
                
                customRecommend["name"] = "ref";
                customRecommend["content"] = Convert.ToString(splitRecommend);                
            }
        
            int i = SplitprosLenghth + SplitConsLength + SplitBestUsesLength + splitGenderlength + splitRecommendlength + 5;

            object[] customs_array = new object[i];
            int j = 0;
           
            foreach (string str in Splitpros)
            {
                if (str == "") { continue; }
                string str2 = "hashtable"+ j.ToString();
                Hashtable customPros = new Hashtable();
                customPros["name"] = "pros";
                customPros["content"] = str ;
                customs_array.SetValue(customPros, j);
                j++;
             }
                   
            foreach (string str in SplitCons)
            {
                if (str == "") { continue; }
                Hashtable customCons = new Hashtable();
                customCons["name"] = "cons";
                customCons["content"] = str;
                customs_array.SetValue(customCons, j);
                j++;

            }
           
            foreach (string str in SplitBestUses)
            {
                if (str == "") { continue; }
                Hashtable customBestUses = new Hashtable();
                customBestUses["name"] = "bestuses";
                customBestUses["content"] = str;
                customs_array.SetValue(customBestUses, j);
                j++;

            }
          
            customs_array.SetValue(customProductName, j);
            customs_array.SetValue(customReview, j+1);
            customs_array.SetValue(customComment, j+2);
            customs_array.SetValue(customCountry, j+3);
            customs_array.SetValue(customgeturl, j+4);

            if (!string.IsNullOrWhiteSpace(splitGender))
            {
                customs_array.SetValue(customGender, j+5);
            }

            if (!string.IsNullOrWhiteSpace(splitRecommend))
            {
                if (string.IsNullOrWhiteSpace(splitGender))
                {
                    customs_array.SetValue(customRecommend, j+5);
                }
                else
                {
                    customs_array.SetValue(customRecommend, j+6);
                }                
            }

            contact_params["customs"] = customs_array;

            //// set params request object
            object[] add_contact_params_array = { api_key, contact_params };

            _request["params"] = add_contact_params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;
            // initialize client
            request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";
            request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));
            response_string = null;
            return campaign_id;                      
        }
        /// <summary>
        /// Overridden to reset Pagesize in case it is coming as 16
        /// </summary>
        /// <param name="localeId"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public override CustomerReviewListModel GetCustomerReviewList(string localeId, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            if (page["size"] == "16")
                page["size"] = "40";
            CustomerReviewListModel listModel = base.GetCustomerReviewList(localeId, filters, sorts, page);
            return listModel;
        }       

    }
}
