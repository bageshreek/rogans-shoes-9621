﻿
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.WebStore.Core.Helpers;

namespace Znode.WebStore.Custom.Controllers
{
    public class RSCategoryController : CategoryController
    {
        private readonly ICategoryAgent _categoryAgent;
        private readonly IWidgetDataAgent _widgetDataAgent;
        public RSCategoryController(ICategoryAgent categoryAgent, IWidgetDataAgent widgetDataAgent) : base(categoryAgent, widgetDataAgent)
        {
            _categoryAgent = categoryAgent;
            _widgetDataAgent = widgetDataAgent;
        }
        //[HttpPost]
        //public override JsonResult SiteMapList(int? pageSize, int? pageLength)
        //{
        //    CategoryHeaderListViewModel cv = _categoryAgent.GetCategories(pageSize, pageLength);
        //    var jsonResult= Json(new { Result = _categoryAgent.GetCategories(pageSize, pageLength) }, JsonRequestBehavior.AllowGet);
        //    jsonResult.MaxJsonLength = int.MaxValue;
        //    return jsonResult;

        //}

        /// <summary>
        /// Added CustomOutputCache for Blank Page and deviceType Parameter to Cache for everytime Page refresh for diff devices
        /// </summary>
        /// <param name="category"></param>
        /// <param name="categoryId"></param>
        /// <param name="viewAll"></param>
        /// <param name="fromSearch"></param>
        /// <param name="publishState"></param>
        /// <param name="bindBreadcrumb"></param>
        /// <param name="facetGroup"></param>
        /// <param name="sort"></param>
        /// <param name="localeId"></param>
        /// <param name="profileId"></param>
        /// <param name="accountId"></param>
        /// <param name="devicetype"></param>
        /// <returns></returns>
        [ChildActionOnly]
        [CustomOutputCache(Duration = 3600, VaryByParam = "categoryId;publishState;viewAll;facetGroup;pageSize;pageNumber;sort;profileId;localeId;accountId;devicetype")]
        public ActionResult RSCategoryContent(string category, int categoryId = 0, bool? viewAll = false, bool fromSearch = false, string publishState = "PRODUCTION", bool bindBreadcrumb = true, string facetGroup = "", string sort = "", int localeId = 0, int profileId = 0, int accountId = 0,string devicetype="")
        {
            if (viewAll.Value)
                SessionHelper.SaveDataInSession<bool>(WebStoreConstants.ViewAllMode, true);
            else
                SessionHelper.RemoveDataFromSession(WebStoreConstants.ViewAllMode);

            CategoryViewModel ViewModel = _categoryAgent.GetCategorySeoDetails(categoryId, bindBreadcrumb);

            if (bindBreadcrumb)
                _categoryAgent.GetBreadCrumb(ViewModel);

            if (!fromSearch)
                //Remove Facet from Session.
                _categoryAgent.RemoveFromSession(ZnodeConstant.FacetsSearchFields);

            if (HelperUtility.IsNotNull(ViewModel.SEODetails))
            {
                ViewBag.Title = ViewModel.SEODetails.SEOTitle;
                TempData["Title"] = ViewModel.SEODetails.SEOTitle;
                TempData["Keywords"] = ViewModel.SEODetails.SEOKeywords;
                TempData["Description"] = ViewModel.SEODetails.SEODescription;
                TempData["CanonicalURL"] = ViewModel.SEODetails.CanonicalURL;
                TempData["RobotTag"] = string.IsNullOrEmpty(ViewModel.SEODetails.RobotTag) || ViewModel.SEODetails.RobotTag.ToLower() == "none" ? string.Empty : ViewModel.SEODetails.RobotTag?.Replace("_", ",");
            }
            Dictionary<string, object> searchProperties = GetSearchProperties();
            ViewModel.ProductListViewModel = _widgetDataAgent.GetCategoryProducts(new WidgetParameter { CMSMappingId = categoryId, TypeOfMapping = ViewModel.CategoryName, LocaleId = PortalAgent.LocaleId, properties = searchProperties }
            );

            return PartialView("_ProductListContent", ViewModel);
        }
    }
}
