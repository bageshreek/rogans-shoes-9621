﻿using Znode.Api.Client.Custom.Clients.Clients;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Engine.WebStore;
using Znode.Libraries.Resources;
using Znode.Sample.Api.Model.RSCustomizedFormModel;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSCustomizedFormAgent : BaseAgent, IRSCustomizedFormAgent
    {
        private readonly IRSCustomizedFormClient _RSCustomizedFormClient;

        public RSCustomizedFormAgent()
        {
            _RSCustomizedFormClient = new RSCustomizedFormClient();
        }

        /// <summary>
        /// ScheduleATruck Implementation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool ScheduleATruck(ScheduleATruckViewModel model)
        {
            _RSCustomizedFormClient.ScheduleATruck(model.ToModel<ScheduleATruckModel>());
            if (!model.HasError)
                model.SuccessMessage = WebStore_Resources.SuccessAddressAdded;
            return true;
        }

        /// <summary>
        /// ShoeFinder Impleemntation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual bool ShoeFinder(ShoesFinderViewModel model)
        {
            _RSCustomizedFormClient.ShoeFinder(model.ToModel<ShoesFinderModel>());
            if (!model.HasError)
                model.SuccessMessage = WebStore_Resources.SuccessAddressAdded;
            return true;
        }
    }
}
