﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;

namespace Znode.WebStore.Custom.Agents.IAgents
{
  public interface IRSCartAgent: ICartAgent
    {
       void UpdateDeliveryPreferenceToPickUp(string storeId, string storeName, string storeAddress, int productId = 0);
        void UpdateDeliveryPreferenceToShip(int productId);
        CartViewModel UpdateCartItemPrice(string guid, decimal quantity, int productId, int shippingId, int? userId = 0);
    }
}
