﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Api.Custom.LibrariesAdmin
{
    public class RSPublishProductHelper : PublishProductHelper
    {
       

        public RSPublishProductHelper():base()
        {
            
           
        }
        
        /// <summary>
        /// Overridden to display Color,Size,Width values as per their displayOrder
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="localeId"></param>
        /// <param name="catalogVersionId"></param>
        /// <param name="configEntity"></param>
        /// <returns></returns>
        public override List<PublishProductModel> GetAssociatedProducts(int productId, int localeId, int? catalogVersionId, List<PublishedConfigurableProductEntityModel> configEntity)
        {
            //Check if entity is not null.
            if (HelperUtility.IsNotNull(configEntity) && configEntity?.Count > 0)
            {
                FilterCollection filters = GetConfigurableProductFilter(localeId, configEntity, catalogVersionId);
                //Get associated product list.
                List<PublishProductModel> associatedProducts = GetProductList(filters).GroupBy(g => g.SKU).Select(s => s.FirstOrDefault())?.ToModel<PublishProductModel>()?.ToList();

                List<PublishProductModel> newassociatedProducts = new List<PublishProductModel>();

                //Assign Display order to associated product list.
                associatedProducts?.ForEach(d =>
                {
                    PublishedConfigurableProductEntityModel configurableProductEntity = configEntity
                                .FirstOrDefault(s => s.AssociatedZnodeProductId == d.ProductId);
                    d.DisplayOrder = HelperUtility.IsNotNull(configurableProductEntity) ? configurableProductEntity.AssociatedProductDisplayOrder : 999;
                    foreach (PublishAttributeModel attribute in d.Attributes?.Where(x => x.IsConfigurable))
                    {
                        attribute.AttributeValues = attribute.SelectValues.FirstOrDefault()?.Value;
                        /*NIVI CODE*/
                        attribute.DisplayOrder = attribute.AttributeCode == "Color" ? attribute.DisplayOrder : Convert.ToInt32(attribute.SelectValues.FirstOrDefault()?.DisplayOrder);
                        /*NIVI CODE*/
                    }
                    newassociatedProducts.Add(d);
                });

                //Sort list according to display order.
                newassociatedProducts = newassociatedProducts.OrderBy(x => x.DisplayOrder)?.ToList();

                return newassociatedProducts;
            }
            return null;
        }
       

    }
}
