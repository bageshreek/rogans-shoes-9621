﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.WebStore.Controllers;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Controllers
{
    public class RSSalesDetailsController : BaseController
    {
        private readonly IRSSalesDetailsAgent _RSSalesDetailsAgent;
        public RSSalesDetailsController() {
            _RSSalesDetailsAgent = new RSSalesDetailsAgent();
        }
        /// <summary>
        /// Implementation for SalesDetails Page to have active products data everytime page is loaded
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult GetSalesDetails()
        {
            List<RSSalesDetailsViewModel> saleDetails = _RSSalesDetailsAgent.GetSalesDetails();
            return View("SaleDetails", saleDetails);
        }
    }
}
