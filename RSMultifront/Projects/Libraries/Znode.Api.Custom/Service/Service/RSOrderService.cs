﻿using klaviyo.net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.ShoppingCart;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Engine.Taxes.Helper;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Api.Custom.Service.Service
{
    public class RSOrderService : OrderService
    {
        private decimal thresholdShippingValue = Convert.ToDecimal(ConfigurationManager.AppSettings["ThresholdShippingValue"]);
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _orderLineItemRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderShipment> _orderOrderShipmentRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderOrderDetailRepository;
        private readonly IShoppingCartMap _shoppingCartMap;
        private readonly IZnodeRepository<ZnodeOmsOrderState> _omsOrderStateRepository;
        private readonly IZnodeRepository<ZnodePublishProductEntity> _publishProductEntity;
        #region Constructor

        public RSOrderService() : base()
        {
            _orderLineItemRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
            _orderOrderShipmentRepository = new ZnodeRepository<ZnodeOmsOrderShipment>();
            _orderOrderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            _shoppingCartMap = GetService<IShoppingCartMap>();
            _omsOrderStateRepository = new ZnodeRepository<ZnodeOmsOrderState>();
            _publishProductEntity = new ZnodeRepository<ZnodePublishProductEntity>(HelperMethods.Context);
        }

        #endregion Constructor

        /// <summary>
        /// Overridden to not save CC data in DB as per RS Requirement
        /// </summary>
        /// <param name="checkout"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public override IZnodeCheckout SetShoppingCartDataToCheckout(IZnodeCheckout checkout, ShoppingCartModel model)
        {
            IZnodeCheckout zCheckout = base.SetShoppingCartDataToCheckout(checkout, model);
            zCheckout.ShoppingCart.CustomShippingCost = model.ShippingCost;

            zCheckout.PoDocument = zCheckout.ShoppingCart.JobName;

            zCheckout.ShoppingCart.JobName = null;
            zCheckout.ShoppingCart.CreditCardNumber = null;
            zCheckout.ShoppingCart.CardType = null;
            zCheckout.ShoppingCart.CreditCardExpMonth = null;
            zCheckout.ShoppingCart.CreditCardExpYear = null;

            return zCheckout;
        }      
        #region Order Receipt Changes
            /// <summary>
            /// Overridden to use instance of RSZnodeOrderReceipt instead of IZnodeOrderReceipt Instance
            /// </summary>
            /// <param name="order"></param>
            /// <param name="checkout"></param>
            /// <param name="feedbackUrl"></param>
            /// <param name="localeId"></param>
            /// <param name="isUpdate"></param>
            /// <param name="isEnableBcc"></param>
            /// <param name="accountId"></param>
            /// <returns></returns>
        public override string GetOrderReceipt(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, bool isUpdate, out bool isEnableBcc, int accountId)
        {
            foreach (OrderLineItemModel item in order.OrderLineItems)
            {
                if (item.PersonaliseValueList != null)
                {
                    item.PersonaliseValueList.Remove("AllocatedLineItems");
                }

                if (item.PersonaliseValuesDetail != null)
                {
                    item.PersonaliseValuesDetail.RemoveAll(pv => pv.PersonalizeCode == "AllocatedLineItems");
                }
            }

            RSZnodeOrderReceipt receipt = new RSZnodeOrderReceipt(order, checkout.ShoppingCart);

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("OrderReceipt", (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
            if (HelperUtility.IsNotNull(emailTemplateMapperModel))
            {
                string receiptContent = ShowOrderAdditionalDetails(emailTemplateMapperModel.Descriptions, order.Custom1);
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                string text = "";
                try
                {
                    text = EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderReceiptHtml(receiptContent));
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage("GetOrderReceipt ERROR!=" + ex, "OrderReceipt", TraceLevel.Error);
                }
                return text;
            }
            isEnableBcc = false;
            return string.Empty;
        }

        /// <summary>
        /// Overridden to have RS specific Subject line
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="userEmailId"></param>
        /// <param name="subject"></param>
        /// <param name="senderEmail"></param>
        /// <param name="bccEmailId"></param>
        /// <param name="receiptHtml"></param>
        /// <param name="isEnableBcc"></param>
        /// <returns></returns>
        public override bool SendOrderReceipt(int portalId, string userEmailId, string subject, string senderEmail, string bccEmailId, string receiptHtml, bool isEnableBcc = false)
        {
            bool isSuccess = false;
            if (subject.Contains(Admin_Resources.TitleOrderReceipt))
            {
                subject = Admin_Resources.SubjectOrderReceipt;
            }

            ZnodeEmail.SendEmail(portalId, userEmailId, senderEmail, ZnodeEmail.GetBccEmail(isEnableBcc, portalId, bccEmailId), subject, receiptHtml, true);
            isSuccess = true;
            return isSuccess;
        }

        /// <summary>
        /// Called from GetOrderReceipt Method
        /// </summary>
        /// <param name="receiptContent"></param>
        /// <param name="customData"></param>
        /// <returns></returns>
        private string ShowOrderAdditionalDetails(string receiptContent, string customData)
        {
            if (!string.IsNullOrEmpty(customData))
            {
                return receiptContent.Replace("#FeedBack#", GenerateOrderAdditionalInfoTemplate(customData));
            }
            else
            {
                return receiptContent;
            }
        }

        /// <summary>
        /// Called from ShowOrderAdditionalDetails to generate order additional information template
        /// </summary>
        /// <param name="customData"></param>
        /// <returns></returns>
        private string GenerateOrderAdditionalInfoTemplate(string customData)
        {
            string template = string.Empty;
            try
            {
                Dictionary<string, string> CustomDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(customData);
                if (HelperUtility.IsNotNull(CustomDict))
                {
                    template = ("<b>Additional Information</b>");

                    if (CustomDict.ContainsKey("ProductName"))
                    {
                        template += $" <br />Product will be used by  { CustomDict["ProductName"]}";
                    }

                    if (CustomDict.ContainsKey("RecipientName"))
                    {
                        template += $" <br />Recipient of the product {CustomDict["RecipientName"]}";
                    }

                    if (CustomDict.ContainsKey("ApproverManager"))
                    {
                        template += $" <br />Approving Manager {CustomDict["ApproverManager"]}";
                    }

                    if (CustomDict.ContainsKey("ProjectName"))
                    {
                        template += $" <br />Project Name {CustomDict["ProjectName"]}";
                    }

                    if (CustomDict.ContainsKey("EventDate"))
                    {
                        template += $" <br />Event Date {CustomDict["EventDate"]}";
                    }

                    if (CustomDict.ContainsKey("InHandsDate"))
                    {
                        template += $" <br />In Hands Date {CustomDict["InHandsDate"]}";
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return template;
        }

        #endregion

        #region Order EMail
        /// <summary>
        /// Override to remove extra space
        /// </summary>
        /// <param name="orderShipment"></param>
        /// <returns></returns>     
        public override string GetOrderShipmentAddress(OrderShipmentModel orderShipment)
        {
            if (IsNotNull(orderShipment))
            {
                string ShippingcompanyName = _addressRepository.Table.Where(x => x.AddressId == orderShipment.AddressId)?.FirstOrDefault()?.CompanyName;

                string street1 = string.IsNullOrEmpty(orderShipment.ShipToStreet2) ? string.Empty : "<br />" + orderShipment.ShipToStreet2;
                orderShipment.ShipToCompanyName = IsNotNull(orderShipment?.ShipToCompanyName) ? $"{orderShipment?.ShipToCompanyName}" : ShippingcompanyName;
                //return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{"<br />"}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{"<br />"}{orderShipment.ShipToStateCode}{"<br />"}{orderShipment.ShipToPostalCode}{"<br />"}{orderShipment.ShipToCountry}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}{"<br />"}{WebStore_Resources.TitleEmail}{" : "}{orderShipment.ShipToEmailId}";
                if (orderShipment.ShipToCompanyName != null)
                {
                    //orderShipment.ShipToCompanyName =   orderShipment.ShipToCompanyName;
                    orderShipment.ShipToCompanyName = orderShipment?.ShipToCompanyName;
                }
                //return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{"<br />"}{orderShipment.ShipToStateCode}{"<br />"}{orderShipment.ShipToPostalCode}{"<br />"}{orderShipment.ShipToCountry}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}{"<br />"}{WebStore_Resources.TitleEmail}{" : "}{orderShipment.ShipToEmailId}";
                return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{","}{orderShipment.ShipToStateCode}{" "}{orderShipment.ShipToPostalCode}{"<br />"}{orderShipment.ShipToCountry}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}{"<br />"}{WebStore_Resources.TitleEmail}{" : "}{orderShipment.ShipToEmailId}";
            }
            return string.Empty;
        }

        /// <summary>
        /// Overridden to change Shipping address format in Order History
        /// </summary>
        /// <param name="orderShipment"></param>
        /// <param name="addressList"></param>
        /// <returns></returns>
        public override string GetOrderShipmentAddress(OrderShipmentModel orderShipment, List<ZnodeAddress> addressList)
        {
            string shippingCompanyName = string.Empty;
            if (IsNotNull(orderShipment))
            {
                if (addressList?.Count > 0)
                {
                    ZnodeLogging.LogMessage("AddressId to get shipping company name", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { AddressId = orderShipment.AddressId });
                    shippingCompanyName = addressList.FirstOrDefault(x => x.AddressId == orderShipment.AddressId)?.CompanyName;
                }

                string street1 = string.IsNullOrEmpty(orderShipment.ShipToStreet2) ? string.Empty : "<br />" + orderShipment.ShipToStreet2;
                orderShipment.ShipToCompanyName = IsNotNull(orderShipment?.ShipToCompanyName) ? $"{orderShipment?.ShipToCompanyName}{"<br />"}" : shippingCompanyName;
                return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{"<br />"}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{", "}{orderShipment.ShipToStateCode}{"  "}{orderShipment.ShipToPostalCode}{"<br />"}{Admin_Resources.LabelPhoneNumber}{": "}{orderShipment.ShipToPhoneNumber}{"<br />"}{"Email: "}{orderShipment.ShipToEmailId}";
            }
            return string.Empty;
        }
        
        /// <summary>
        /// Override to remove extra space-REmove company name as it not get in list
        /// </summary>
        /// <param name="orderBilling"></param>
        /// <returns></returns>
        public override string GetOrderBillingAddress(OrderModel orderBilling)
        {
            if (IsNotNull(orderBilling))
            {
                string street1 = string.IsNullOrEmpty(orderBilling.BillingAddress.Address2) ? string.Empty : "<br />" + orderBilling.BillingAddress.Address2;
                string CompanyName = "";
                if (orderBilling?.BillingAddress.CompanyName != null)
                {
                    CompanyName = string.IsNullOrEmpty(orderBilling.BillingAddress.CompanyName) ? string.Empty : "<br />" + orderBilling.BillingAddress.CompanyName;
                }
                //return $"{orderBilling?.BillingAddress.FirstName}{" "}{orderBilling?.BillingAddress.LastName}{"<br />"}{orderBilling?.BillingAddress.CompanyName}{"<br />"}{orderBilling.BillingAddress.Address1}{street1}{"<br />"}{ orderBilling.BillingAddress.CityName}{"<br />"}{(string.IsNullOrEmpty(orderBilling.BillingAddress.StateCode) ? orderBilling.BillingAddress.StateName : orderBilling.BillingAddress.StateCode)}{"<br />"}{orderBilling.BillingAddress.PostalCode}{"<br />"}{orderBilling.BillingAddress.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderBilling.BillingAddress.PhoneNumber}";
                return $"{orderBilling?.BillingAddress.FirstName}{" "}{orderBilling?.BillingAddress.LastName}{orderBilling.BillingAddress.CompanyName}{CompanyName}{"<br />"}{orderBilling.BillingAddress.Address1}{street1}{"<br />"}{ orderBilling.BillingAddress.CityName}{", "}{(string.IsNullOrEmpty(orderBilling.BillingAddress.StateCode) ? orderBilling.BillingAddress.StateName : orderBilling.BillingAddress.StateCode)}{"  "}{orderBilling.BillingAddress.PostalCode}{"<br />"}{Admin_Resources.LabelPhoneNumber}{": "}{orderBilling.BillingAddress.PhoneNumber}";
            }
            return string.Empty;
        }

        /// <summary>
        /// Sends mail
        /// </summary>
        /// <param name="orderModel"></param>
        public override void SendOrderStatusEmail(OrderModel orderModel)
        {
            ImageHelper imageHelper = new ImageHelper();
            foreach (OrderLineItemModel item in orderModel.OrderLineItems)
            {
                item.OrderLineItemCollection.AddRange(orderModel.OrderLineItems.Where(x => x.ParentOmsOrderLineItemsId == item.OmsOrderLineItemsId && x.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.AddOns)?.ToList());
                item.ProductImagePath = "";
                if (item.DownloadLink != "")
                {
                    item.ProductImagePath = item.DownloadLink;
                    string images = imageHelper.GetImageHttpPathSmallThumbnail(Convert.ToString(item.ProductImagePath));
                    item.ProductImagePath = "<img src=\"" + images + "\" data-src=\"" + images + "\"/>";
                }

            }
            orderModel.OrderLineItems.RemoveAll(x => x.ParentOmsOrderLineItemsId == null);

            string subject = string.Empty;
            bool isEnableBcc = false;
            if (orderModel.OrderState.ToUpper() == ZnodeOrderStatusEnum.CANCELLED.ToString())
            {
                //subject = $"{Admin_Resources.CancelledOrderStatusSubject} - {orderModel.OrderNumber}";
                subject = Admin_Resources.SubjectCancelReceipt;
                orderModel.ReceiptHtml = GetCancelledOrderReceiptForEmail(orderModel, out isEnableBcc);
            }
            else if (orderModel.OrderState.ToUpper() == ZnodeOrderStatusEnum.SUBMITTED.ToString())
            {
                //subject = $"{Admin_Resources.OrderStatusUpdated} - {orderModel.OrderNumber}";
                subject = Admin_Resources.SubjectSubmittedReceipt;
                orderModel.ReceiptHtml = GetSubmittedReceiptForEmail(orderModel, out isEnableBcc);
            }
            else if (orderModel.OrderState.ToUpper() == ZnodeOrderStatusEnum.SHIPPED.ToString())
            {
                // And finally attach the receipt HTML to the order and return
                // subject = $"{Admin_Resources.ShippedOrderStatusSubject} - {orderModel.OrderNumber}";
                subject = Admin_Resources.SubjectShippingReceipt;
                orderModel.ReceiptHtml = GetShippingReceiptForEmail(orderModel, out isEnableBcc);
            }

            SendOrderReceipt(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, subject, ZnodeConfigManager.SiteConfig.AdminEmail, string.Empty, orderModel.ReceiptHtml, isEnableBcc);
        }

        /// <summary>
        /// Gets email template with values for Cancelled Order
        /// </summary>
        /// <param name="orderModel"></param>
        /// <param name="isEnableBcc"></param>
        /// <returns></returns>
        public override string GetCancelledOrderReceiptForEmail(OrderModel orderModel, out bool isEnableBcc)
        {
            RSZnodeOrderReceipt receipt = new RSZnodeOrderReceipt(orderModel);
            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("CancelledOrderReceipt", (orderModel.PortalId > 0) ? orderModel.PortalId : PortalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderResendReceiptHtml(emailTemplateMapperModel.Descriptions));
            }
            isEnableBcc = false;
            return string.Empty;
        }

        //to generate shipping status receipt
        //Get Html Resend Receipt For Email.
        public override string GetShippingReceiptForEmail(OrderModel orderModel, out bool isEnableBcc)
        {
            RSZnodeOrderReceipt receipt = new RSZnodeOrderReceipt(orderModel);
            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ShippingReceipt", (orderModel.PortalId > 0) ? orderModel.PortalId : PortalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderResendReceiptHtml(emailTemplateMapperModel.Descriptions));
            }
            isEnableBcc = false;
            return string.Empty;
        }

        /// <summary>
        /// Gets email template with values for Submitted Receipt
        /// </summary>
        /// <param name="orderModel"></param>
        /// <param name="isEnableBcc"></param>
        /// <returns></returns>
        public string GetSubmittedReceiptForEmail(OrderModel orderModel, out bool isEnableBcc)
        {
            RSZnodeOrderReceipt receipt = new RSZnodeOrderReceipt(orderModel);
            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("OrderSubmitted", (orderModel.PortalId > 0) ? orderModel.PortalId : PortalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderResendReceiptHtml(emailTemplateMapperModel.Descriptions));
            }
            isEnableBcc = false;
            return string.Empty;
        }

        #endregion

        /// <summary>
        /// Get order list.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public override OrdersListModel GetOrderList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            //Bind the Filter conditions for the authorized portal access.
            BindUserPortalFilter(ref filters);

            //Replace sort key name.
            if (HelperUtility.IsNotNull(sorts))
            {
                ReplaceSortKeys(ref sorts);
            }

            int userId = 0;
            GetUserIdFromFilters(filters, ref userId);

            int fromAdmin = Convert.ToInt32(filters?.Find(x => string.Equals(x.FilterName, Znode.Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase))?.Item3);
            filters?.RemoveAll(x => string.Equals(x.FilterName, Znode.Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase));
            ReplaceFilterKeys(ref filters);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            IList<OrderModel> list = GetOrderList(pageListModel, userId, fromAdmin);
            foreach (OrderModel order in list)
            {
                List<ZnodeOmsOrderLineItem> orderLineItemDetails = GetOrderLineItemListByOrderId(order.OmsOrderDetailsId);
                List<OrderLineItemModel> orderLineItemModel = orderLineItemDetails?.ToModel<OrderLineItemModel>().ToList();
                order.OrderLineItems = orderLineItemModel;
                int OrderShipmentId = Convert.ToInt32(orderLineItemDetails.FirstOrDefault()?.OmsOrderShipmentId);
                order.ShippingAddress = SetShippingAddress(OrderShipmentId);
                ZnodeOmsOrderDetail omsOrderDetail = _orderOrderDetailRepository.Table.Where(w => w.OmsOrderDetailsId == order.OmsOrderDetailsId).FirstOrDefault();
                order.BillingAddress = SetBillingAddress(omsOrderDetail);

            }
            OrdersListModel orderListModel = new OrdersListModel() { Orders = list?.ToList() };

            GetCustomerName(userId, orderListModel);

            orderListModel.BindPageListModel(pageListModel);
            return orderListModel;
        }

        /// <summary>
        /// Called from GetOrderList
        /// </summary>
        /// <param name="omsOrderDetail"></param>
        /// <returns></returns>
        private AddressModel SetBillingAddress(ZnodeOmsOrderDetail omsOrderDetail)
        {
            AddressModel addressModel = new AddressModel();
            addressModel.FirstName = omsOrderDetail.BillingFirstName;
            addressModel.LastName = omsOrderDetail.BillingLastName;
            addressModel.EmailAddress = omsOrderDetail.BillingEmailId;

            addressModel.CompanyName = omsOrderDetail.BillingCompanyName;

            addressModel.CityName = omsOrderDetail.BillingCity;

            addressModel.StateCode = omsOrderDetail.BillingStateCode;

            addressModel.CountryCode = omsOrderDetail.BillingCountry;

            addressModel.PostalCode = omsOrderDetail.BillingPostalCode;

            addressModel.Address1 = omsOrderDetail.BillingStreet1;

            addressModel.Address2 = omsOrderDetail.BillingStreet2;
            addressModel.PhoneNumber = omsOrderDetail.BillingPhoneNumber;
            return addressModel;
        }

        /// <summary>
        /// Called from GetOrderList
        /// </summary>
        /// <param name="OrderShipmentId"></param>
        /// <returns></returns>
        private AddressModel SetShippingAddress(int OrderShipmentId)
        {
            ZnodeOmsOrderShipment OrderShipment = _orderOrderShipmentRepository.Table.Where(x => x.OmsOrderShipmentId == OrderShipmentId).FirstOrDefault();

            AddressModel addressModel = new AddressModel();
            addressModel.FirstName = OrderShipment.ShipToFirstName;
            addressModel.LastName = OrderShipment.ShipToLastName;
            addressModel.EmailAddress = OrderShipment.ShipToEmailId;

            addressModel.CompanyName = OrderShipment.ShipToCompanyName;

            addressModel.CityName = OrderShipment.ShipToCity;

            addressModel.StateCode = OrderShipment.ShipToStateCode;

            addressModel.CountryCode = OrderShipment.ShipToCountry;

            addressModel.PostalCode = OrderShipment.ShipToPostalCode;

            addressModel.Address1 = OrderShipment.ShipToStreet1;

            addressModel.Address2 = OrderShipment.ShipToStreet2;
            addressModel.PhoneNumber = OrderShipment.ShipToPhoneNumber;

            return addressModel;
        }

        /// <summary>
        /// Overridden to pass call to custom Shoppingcart instead of checkout.ShoppingCart
        /// </summary>
        /// <param name="shoppingCartModel"></param>
        /// <returns></returns>
        public override OrderModel CheckInventoryAndMinMaxQuantity(ShoppingCartModel shoppingCartModel)
        {
            //ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(shoppingCartModel))
            {
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorShoppingCartModelNull);
            }

            if (shoppingCartModel.UserDetails == null)
            {
                shoppingCartModel.UserDetails = new UserModel();
            }

            UserAddressModel userDetails = SetUserDetails(shoppingCartModel);

            ZnodeShoppingCart znodeShoppingCart = _shoppingCartMap.ToZnodeShoppingCart(shoppingCartModel, userDetails);

            // Create the checkout object
            IZnodeCheckout checkout = CheckoutMap.ToZnodeCheckout(userDetails, znodeShoppingCart);
            string isInventoryInStockMessage = string.Empty;
            //Nivi chnages to pass call to custom RsShoppingcrt class
            //checkout.ShoppingCart.CheckInventoryAndMinMaxQuantity(out isInventoryInStockMessage, out minMaxSelectableQuantity);
            znodeShoppingCart.CheckInventoryAndMinMaxQuantity(out isInventoryInStockMessage, out Dictionary<int, string> minMaxSelectableQuantity);

            if (!string.IsNullOrEmpty(isInventoryInStockMessage))
            {
                throw new ZnodeException(ErrorCodes.OutOfStockException, Admin_Resources.OutOfStockException);
            }

            if (IsNotNull(minMaxSelectableQuantity) && minMaxSelectableQuantity.Count > 0 && minMaxSelectableQuantity.ContainsKey(ErrorCodes.MinAndMaxSelectedQuantityError))
            {
                throw new ZnodeException(ErrorCodes.MinAndMaxSelectedQuantityError, minMaxSelectableQuantity[ErrorCodes.MinAndMaxSelectedQuantityError]);
            }

            //ZnodeLogging.LogMessage("Is available inventory message and MinMaxQuantity flag:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { isInventoryInStockMessage = isInventoryInStockMessage, minMaxSelectableQuantity = minMaxSelectableQuantity });

            //ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return new OrderModel() { IsAvailabelInventoryAndMinMaxQuantity = true };
        }

        /// <summary>
        /// Called from GetOrderList and calls SP to get  Order Line Item List
        /// </summary>
        /// <param name="OmsOrderDetailsId"></param>
        /// <returns></returns>
        public virtual List<ZnodeOmsOrderLineItem> GetOrderLineItemListByOrderId(int OmsOrderDetailsId)
        {
            IZnodeViewRepository<ZnodeOmsOrderLineItem> objStoredProc = new ZnodeViewRepository<ZnodeOmsOrderLineItem>();
            objStoredProc.SetParameter("@OmsOrderDetailsId", OmsOrderDetailsId, ParameterDirection.Input, DbType.Int32);
            return objStoredProc.ExecuteStoredProcedureList("RS_GetOmsOrderLineDetails @OmsOrderDetailsId").ToList();
        }

        #region Order Status Update

        /// <summary>
        /// Called from UpdateOrderStatus
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ZnodeOmsOrderDetail IsValidOrder(OrderStateParameterModel model)
        {
            if (IsNull(model))
            {
                throw new Exception("Order model cannot be null.");
            }

            if (model.OmsOrderId < 1)
            {
                throw new Exception("Order ID cannot be less than 1.");
            }

            if (IsNull(model.OmsOrderStateId) || model.OmsOrderStateId < 1)
            {
                throw new Exception("Invalid order status.");
            }

            ZnodeOmsOrderDetail order = _orderDetailsRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(CreatFiltersForOrder(model).ToFilterDataCollection())?.WhereClause, GetExpandsForOrderLineItem(GetOrderLineItemExpands()));
            return order;
        }


        /// <summary>
        /// Called from UpdateOrderStatus to Cancel tax transaction.
        /// </summary>
        /// <param name="updated"></param>
        /// <param name="order"></param>
        /// <param name="orderStateList"></param>
        private static void CancelTaxTransaction(bool updated, ZnodeOmsOrderDetail order, List<ZnodeOmsOrderState> orderStateList)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            bool isCancelledOrder = order.OmsOrderStateId.Equals(orderStateList?.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.CANCELLED.ToString())?.OmsOrderStateId);
            if (updated && isCancelledOrder)
            {
                ZnodeTaxHelper taxHelper = new ZnodeTaxHelper();
                int? omsLineItemId = order?.ZnodeOmsOrderLineItems?.FirstOrDefault()?.OmsOrderLineItemsId;
                if (IsNotNull(omsLineItemId))
                {
                    taxHelper.CancelTaxTransaction(omsLineItemId.Value, order?.PortalId);
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        /// <summary>
        /// Overridden to set Custom1 and Custom2 values
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override bool UpdateOrderStatus(OrderStateParameterModel model)
        {
            // ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            // ZnodeLogging.LogMessage("OrderStateParameterModel with Id: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsOrderId = model?.OmsOrderId });
            bool updated = false;

            ZnodeOmsOrderDetail order = IsValidOrder(model);

            if (IsNotNull(order))
            {
                int previousOrderStateId = order.OmsOrderStateId;
                order.OmsOrderStateId = model.OmsOrderStateId;
                order.TrackingNumber = model.TrackingNumber;
                order.ModifiedDate = GetDateTime();
                /*NIVI CODE*/
                order.Custom1 = Convert.ToString(model.Custom1);
                order.Custom2 = Convert.ToString(model.Custom2);
                /*NIVI CODE*/
                List<ZnodeOmsOrderState> orderStateList = _omsOrderStateRepository.GetEntityList(string.Empty)?.ToList();
                // ZnodeLogging.LogMessage("orderStateList Count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderStateListCount = orderStateList?.Count });

                if (orderStateList?.Count > 0)
                {
                    if (order.OmsOrderStateId.Equals(orderStateList?.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.SHIPPED.ToString())?.OmsOrderStateId))
                    {
                        order.ShipDate = DateTime.Now;
                    }
                    else if (order.OmsOrderStateId.Equals(orderStateList?.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.RETURNED.ToString())?.OmsOrderStateId))
                    {
                        order.ReturnDate = GetDateTime().Date + GetDateTime().TimeOfDay;
                    }
                }

                updated = _orderDetailsRepository.Update(order);
                if (updated)
                {
                    UpdateLineItemState(model.OmsOrderId, previousOrderStateId, model.OmsOrderStateId, order.ShipDate);
                }
                ZnodeLogging.LogMessage("Klaviyo OrderID:-" + order.OmsOrderId, "NIVI", TraceLevel.Error);
                //Cancel tax transaction.
                CancelTaxTransaction(updated, order, orderStateList);
                OrderModel orderModel = GetOrderById(order.OmsOrderId, null, GetOrderExpands());
                try
                {
                    if (order.OmsOrderStateId.Equals(orderStateList?.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.SHIPPED.ToString())?.OmsOrderStateId))
                    {
                        KlaviyoFulfillOrder(orderModel);
                    }
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage("Klaviyo Error:-" + ex.Message + ex.StackTrace, "NIVI", TraceLevel.Error);
                }

                if (updated && orderHelper.IsSendEmail(order.OmsOrderStateId))
                {
                    SendOrderStatusEmail(orderModel);
                    return true;
                }
            }
            return updated;
        }
        #endregion

        #region Klaviyo Integration
        //Create new order.
        public override OrderModel CreateOrder(ShoppingCartModel model)
        {
            OrderModel orderModel = base.CreateOrder(model);
            ZnodeLogging.LogMessage("ordernumber" + model.OrderNumber, "Nivi", TraceLevel.Error);
            KlaviyoPlaceOrder(orderModel, model);
            return orderModel;
        }
        private void KlaviyoPlaceOrder(OrderModel orderModel, ShoppingCartModel model)
        {
            KlaviyoGateway gateway = new KlaviyoGateway("SuefjY");
            KlaviyoEvent ev = new KlaviyoEvent()
            {
                Token = gateway.Token,
                Event = "Placed Order"
            };
            List<NotRequiredProperty> listBillingAddress = new List<NotRequiredProperty>();
            List<NotRequiredProperty> listShippingAddress = new List<NotRequiredProperty>();
            List<KlaviyoItems> items = new List<KlaviyoItems>();
            KlaviyoItems item = new KlaviyoItems();
            foreach (OrderLineItemModel lineitems in orderModel.OrderLineItems)
            {
                item = new KlaviyoItems();
                ShoppingCartItemModel shoppingCartItem = model.ShoppingCartItems.Where(s => s.SKU == lineitems.Sku).FirstOrDefault();

                item.ProductName = lineitems.ProductName;
                item.SKU = lineitems.Sku;
                item.Qunatity = lineitems.Quantity;
                item.ItemPrice = lineitems.Price;
                item.RowTotal = lineitems.Quantity * lineitems.Price;
                item.ProductURL = Convert.ToString(ConfigurationManager.AppSettings["ZnodeWebStoreUri"]) + "/" + shoppingCartItem.SeoPageName;
                item.ImageURL = shoppingCartItem.ImagePath;
                item.Description = lineitems.Description;
                items.Add(item);
            }

            KlaviyoAddress Billingaddress = new KlaviyoAddress();
            Billingaddress.FirstName = orderModel.BillingAddress.FirstName;
            Billingaddress.LastName = orderModel.BillingAddress.LastName;
            Billingaddress.Company = orderModel.BillingAddress.CompanyName;
            Billingaddress.Address1 = orderModel.BillingAddress.Address1;
            Billingaddress.Address2 = orderModel.BillingAddress.Address2;
            Billingaddress.City = orderModel.BillingAddress.CityName;
            Billingaddress.Region = orderModel.BillingAddress.StateName;
            Billingaddress.RegionCode = orderModel.BillingAddress.StateCode;
            Billingaddress.Country = orderModel.BillingAddress.CountryName;
            Billingaddress.CountryCode = orderModel.BillingAddress.CountryCode;
            Billingaddress.Zip = orderModel.BillingAddress.PostalCode;
            Billingaddress.Phone = orderModel.BillingAddress.PhoneNumber;

            KlaviyoAddress ShippingAddress = new KlaviyoAddress();
            ShippingAddress.FirstName = orderModel.ShippingAddress.FirstName;
            ShippingAddress.LastName = orderModel.ShippingAddress.LastName;
            ShippingAddress.Company = orderModel.ShippingAddress.CompanyName;
            ShippingAddress.Address1 = orderModel.ShippingAddress.Address1;
            ShippingAddress.Address2 = orderModel.ShippingAddress.Address2;
            ShippingAddress.City = orderModel.ShippingAddress.CityName;
            ShippingAddress.Region = orderModel.ShippingAddress.StateName;
            ShippingAddress.RegionCode = orderModel.ShippingAddress.StateCode;
            ShippingAddress.Country = orderModel.ShippingAddress.CountryName;
            ShippingAddress.CountryCode = orderModel.ShippingAddress.CountryCode;
            ShippingAddress.Zip = orderModel.ShippingAddress.PostalCode;
            ShippingAddress.Phone = orderModel.ShippingAddress.PhoneNumber;

            if (orderModel.CouponCode != null)
            {
                ev.Properties.NotRequiredProperties.Add(new NotRequiredProperty("DiscountCode", orderModel.CouponCode));
            }
            if (orderModel.DiscountAmount > 0)
            {
                ev.Properties.NotRequiredProperties.Add(new NotRequiredProperty("DiscountValue", orderModel.DiscountAmount));
            }
            if (orderModel.TaxCost > 0)
            {
                ev.Properties.NotRequiredProperties.Add(new NotRequiredProperty("Tax", orderModel.TaxCost));
            }
            if (orderModel.ShippingCost > 0)
            {
                ev.Properties.NotRequiredProperties.Add(new NotRequiredProperty("Shipping", orderModel.ShippingCost));
            }

            ev.Properties.Billaddress = Billingaddress;
            ev.Properties.ShipingAddress = ShippingAddress;
            ev.Properties.KlaviyoItems = items;
            ev.Properties.EventId = orderModel.OrderNumber;

            ev.CustomerProperties.Email = orderModel.BillingAddress.EmailAddress;
            //ev.CustomerProperties.FirstName = "Bhagyashri";
            //ev.CustomerProperties.LastName = "Joshi";
            ev.Properties.Value = orderModel.Total;

            gateway.Track(ev);
        }
        private void KlaviyoFulfillOrder(OrderModel orderModel)
        {
            KlaviyoGateway gateway = new KlaviyoGateway("SuefjY");
            KlaviyoEvent ev = new KlaviyoEvent()
            {
                Token = gateway.Token,
                Event = "Fulfilled Order"
            };
            List<NotRequiredProperty> listBillingAddress = new List<NotRequiredProperty>();
            List<NotRequiredProperty> listShippingAddress = new List<NotRequiredProperty>();
            List<KlaviyoItems> items = new List<KlaviyoItems>();
            KlaviyoItems item = new KlaviyoItems();
            int? CatalogVersionId = GetCatalogVersionId(0, 0, 0);
            foreach (OrderLineItemModel lineitems in orderModel.OrderLineItems.Where(x => x.ParentOmsOrderLineItemsId != null))
            {
                string ParentSku = Convert.ToString(orderModel.OrderLineItems.Where(x => x.ParentOmsOrderLineItemsId == lineitems.ParentOmsOrderLineItemsId).Select(y => y.Sku).FirstOrDefault());
                //ZnodeLogging.LogMessage("Klaviyo ParentSKU:-" + ParentSku, "NIVI", TraceLevel.Error);
                //ZnodeLogging.LogMessage("Klaviyo SEO:-" + Convert.ToString(_publishProductEntity.Table.Where(x => x.SKU == ParentSku & x.VersionId==CatalogVersionId).FirstOrDefault().SeoUrl), "NIVI", TraceLevel.Error);
                string ZnodeProductId = Convert.ToString(_publishProductEntity.Table.Where(x => x.SKU == ParentSku & x.VersionId == CatalogVersionId).FirstOrDefault().ZnodeProductId);
                string ProductReviewURL = Convert.ToString(ConfigurationManager.AppSettings["ZnodeWebStoreUri"]) + "Product/writereview?id=" + ZnodeProductId + "&name=" + lineitems.ProductName;
                ZnodePublishProductEntity ProductEntity = _publishProductEntity.Table.Where(x => x.SKU == lineitems.Sku & x.VersionId == CatalogVersionId).FirstOrDefault();
                List<PublishAttributeModel> attributeModels = JsonConvert.DeserializeObject<List<PublishAttributeModel>>(ProductEntity.Attributes);
                string BaseImagePath = ConfigurationManager.AppSettings["BaseImagePath"].ToString();
                string BaseImage = BaseImagePath + @"t_460/" + Convert.ToString(attributeModels.Value("BaseImage"));
                item = new KlaviyoItems();

                item.ProductName = lineitems.ProductName;
                item.SKU = lineitems.Sku;
                item.Qunatity = lineitems.Quantity;
                item.ItemPrice = lineitems.Price;
                item.RowTotal = lineitems.Quantity * lineitems.Price;
                item.ProductURL = ProductReviewURL;
                item.ImageURL = BaseImage;
                item.Description = lineitems.Description;
                items.Add(item);
            }

            KlaviyoAddress Billingaddress = new KlaviyoAddress();
            Billingaddress.FirstName = orderModel.BillingAddress.FirstName;
            Billingaddress.LastName = orderModel.BillingAddress.LastName;
            Billingaddress.Company = orderModel.BillingAddress.CompanyName;
            Billingaddress.Address1 = orderModel.BillingAddress.Address1;
            Billingaddress.Address2 = orderModel.BillingAddress.Address2;
            Billingaddress.City = orderModel.BillingAddress.CityName;
            Billingaddress.Region = orderModel.BillingAddress.StateName;
            Billingaddress.RegionCode = orderModel.BillingAddress.StateCode;
            Billingaddress.Country = orderModel.BillingAddress.CountryName;
            Billingaddress.CountryCode = orderModel.BillingAddress.CountryCode;
            Billingaddress.Zip = orderModel.BillingAddress.PostalCode;
            Billingaddress.Phone = orderModel.BillingAddress.PhoneNumber;

            KlaviyoAddress ShippingAddress = new KlaviyoAddress();
            ShippingAddress.FirstName = orderModel.ShippingAddress.FirstName;
            ShippingAddress.LastName = orderModel.ShippingAddress.LastName;
            ShippingAddress.Company = orderModel.ShippingAddress.CompanyName;
            ShippingAddress.Address1 = orderModel.ShippingAddress.Address1;
            ShippingAddress.Address2 = orderModel.ShippingAddress.Address2;
            ShippingAddress.City = orderModel.ShippingAddress.CityName;
            ShippingAddress.Region = orderModel.ShippingAddress.StateName;
            ShippingAddress.RegionCode = orderModel.ShippingAddress.StateCode;
            ShippingAddress.Country = orderModel.ShippingAddress.CountryName;
            ShippingAddress.CountryCode = orderModel.ShippingAddress.CountryCode;
            ShippingAddress.Zip = orderModel.ShippingAddress.PostalCode;
            ShippingAddress.Phone = orderModel.ShippingAddress.PhoneNumber;

            if (orderModel.CouponCode != null)
            {
                ev.Properties.NotRequiredProperties.Add(new NotRequiredProperty("DiscountCode", orderModel.CouponCode));
            }
            if (orderModel.DiscountAmount > 0)
            {
                ev.Properties.NotRequiredProperties.Add(new NotRequiredProperty("DiscountValue", orderModel.DiscountAmount));
            }
            if (orderModel.TaxCost > 0)
            {
                ev.Properties.NotRequiredProperties.Add(new NotRequiredProperty("Tax", orderModel.TaxCost));
            }
            if (orderModel.ShippingCost > 0)
            {
                ev.Properties.NotRequiredProperties.Add(new NotRequiredProperty("Shipping", orderModel.ShippingCost));
            }

            ev.Properties.Billaddress = Billingaddress;
            ev.Properties.ShipingAddress = ShippingAddress;
            ev.Properties.KlaviyoItems = items;
            ev.Properties.EventId = orderModel.OrderNumber;

            ev.CustomerProperties.Email = orderModel.BillingAddress.EmailAddress;
            //ev.CustomerProperties.FirstName = "Bhagyashri";
            //ev.CustomerProperties.LastName = "Joshi";
            ev.Properties.Value = orderModel.Total;

            gateway.Track(ev);
        }
        #endregion
    }
}
