﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.WebStore.Core.Agents;
using Znode.WebStore.Core.Extensions;
using Znode.WebStore.Custom.Agents.IAgents;
//using Znode.WebStore.Custom.Extensions;

namespace Znode.WebStore.Custom.Controllers
{
    [NoCacheAttribute]
    public class RSCartController:CartController
    {
        private readonly IRSCartAgent _cartAgent;
        private readonly IWSPromotionAgent _promotionAgent;
        #region Constructor
        public RSCartController(IRSCartAgent cartAgent, IPortalAgent portalAgent, IWSPromotionAgent promotionAgent):base(cartAgent,portalAgent,promotionAgent)
        {
            _cartAgent = cartAgent;
            _promotionAgent = promotionAgent;

        }
        #endregion

        /// <summary>
        /// Called when User selected Ship option in PDP and Changes it to PickUp in cart
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="storeName"></param>
        /// <param name="storeAddress"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpPost]
        public string UpdateDeliveryPreferenceToPickUp(string storeId, string storeName, string storeAddress,int productId = 0)
        {
            _cartAgent.UpdateDeliveryPreferenceToPickUp(storeId,storeName,storeAddress,productId);
            //return RedirectToAction<CartController>(x => x.GetShoppingCart());
            return "{\"msg\":\"success\"}";
        }

        /// <summary>
        /// Called when User selected PickUp option in PDP and Changes it to Ship in cart
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpPost]
        public string UpdateDeliveryPreferenceToShip(int productId)
        {
            _cartAgent.UpdateDeliveryPreferenceToShip(productId);
            //return RedirectToAction<CartController>(x => x.GetShoppingCart());
            return "{\"msg\":\"success\"}";
        }
        /// <summary>
        /// Called on Updates price in cart in Impersonation case.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="unitPrice"></param>
        /// <param name="productId"></param>
        /// <param name="shippingId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual string UpdateCartItemPrice(string guid, decimal unitPrice, int productId = 0, int shippingId = 0, int? userId = 0)
        {
            CartViewModel cartViewModel = _cartAgent.UpdateCartItemPrice(guid, unitPrice, productId, shippingId, userId);
            // return  RedirectToAction<CartController>(x => x.CalculateCart());
            return "{\"msg\":\"success\"}";
        }

    }

   

}
