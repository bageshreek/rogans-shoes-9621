﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Cache;
using Znode.Engine.Services;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Custom.Cache.Cache
{
  public  class RSPublishProductCache : PublishProductCache,IRSPublishProductCache
    {
        #region Private Variable
        private readonly IRSPublishProductService _service;
        #endregion

        #region Constructor
        public RSPublishProductCache(IRSPublishProductService publishProductService):base(publishProductService)
        {
            _service = publishProductService;
        }
        #endregion

        /// <summary>
        /// Gets all details related to Store Locations for PDP and Cart
        /// </summary>
        /// <param name="state"></param>
        /// <param name="sku"></param>
        /// <returns></returns>
        public RSPublishProductResponse GetStoreLocationDetails(string state, string sku)
        { 
            DataTable storelocations = _service.GetStoreLocationDetails(state, sku);

            RSPublishProductResponse response = new RSPublishProductResponse { StoreLocations = storelocations };

            return response;
        }
    }
}
