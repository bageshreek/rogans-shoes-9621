﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.WebStore.Agents;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSStoreLocatorAgent : StoreLocatorAgent, IRSStoreLocatorAgent
    {
        double cookiesexpiretime = Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]);

        #region Public Constructor

        public RSStoreLocatorAgent(IWebStoreLocatorClient webStoreLocatorClient) : base(webStoreLocatorClient)
        {

        }
        #endregion

        /// <summary>
        /// Saves Default Selected Store detailsin cookie everytime it is changed
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="storeName"></param>
        /// <param name="storeAddress"></param>
        public void SaveInCookie(string storeId, string storeName, string storeAddress)
        {
            RemoveCookie("DefaultSelectedStore");
            string defaultSelectedStoreDetails = storeId + "*" + storeName + "*" + storeAddress;
            SaveInCookie("DefaultSelectedStore", defaultSelectedStoreDetails, cookiesexpiretime);
        }

        /// <summary>
        /// Gets Default Store Details From Cookie whenever required
        /// </summary>
        /// <returns></returns>
        public string GetDefaultStoreDetailsFromCookie()
        {
            string defaultStore = GetFromCookie("DefaultSelectedStore");
            //ZnodeLogging.LogMessage("RSWIDGETDATAAGENT GetDefaultStoreDetailsFromCookie() GetFromCookie-" + defaultStore, "WIDGET", TraceLevel.Error);
            string[] storeValues = defaultStore.Split('*');
            string storeName = "";
            if (storeValues.Length > 1)
            {
                storeName = defaultStore;
            }
            if (storeName == "")
                storeName = "Locate A Store";
            return storeName;
        }
    }
}
