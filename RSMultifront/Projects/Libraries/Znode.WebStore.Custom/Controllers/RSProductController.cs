﻿using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;


namespace Znode.WebStore.Custom.Controllers
{
    public class RSProductController : ProductController
    {
        #region Public Constructor

      
        private readonly ICartAgent _cartAgent;
        public RSProductController(IProductAgent productAgent, IUserAgent userAgent, ICartAgent cartAgent, IWidgetDataAgent widgetDataAgent, IAttributeAgent attributeAgent, IRecommendationAgent recommendationAgent) :
            base(productAgent, userAgent, cartAgent, widgetDataAgent, attributeAgent, recommendationAgent)
        {           
            _cartAgent = cartAgent;
        }


        /// <summary>
        /// Overriden Just to show StickyBar Image on AddToCart
        /// </summary>
        /// <param name="cartItem"></param>
        /// <param name="IsRedirectToCart"></param>
        /// <returns></returns>
        [HttpPost]
        public override ActionResult AddToCartProduct(AddToCartViewModel cartItem, bool IsRedirectToCart = true)
        {
            string _stickyBarImage = ConfigurationManager.AppSettings["BaseImagePath"].ToString() + "t_55/";
            AddToCartViewModel ShoppingCart = _cartAgent.AddToCartProduct(cartItem);

            if (IsRedirectToCart)
            {
                return RedirectToAction<CartController>(x => x.Index());
            }
           
            return Json(new
            {
                status = ShoppingCart.HasError,
                cartCount = ShoppingCart.CartCount,
                Product = ShoppingCart?.ShoppingCartItems?.FirstOrDefault(x => x.SKU == cartItem.SKU),
                ImagePath = $"{_stickyBarImage}/{ShoppingCart?.ShoppingCartItems?.FirstOrDefault(x => x.SKU == cartItem.SKU)?.Product.Attributes.FirstOrDefault(x => x.AttributeCode == "BaseImage")?.AttributeValues}"
            },
            JsonRequestBehavior.AllowGet);

        }      
        #endregion
    }
}
