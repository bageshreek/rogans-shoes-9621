﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.WebStore.Agents;
using Znode.Libraries.Framework.Business;

namespace Znode.WebStore.Core.Helpers
{
    public class CustomOutputCacheAttribute : OutputCacheAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (IsCacheAllowed(filterContext.HttpContext))
                {
                    base.OnActionExecuting(filterContext);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, "CustomPageCacheIssue", TraceLevel.Error);
            }
        }
       
        protected bool IsCacheAllowed(HttpContextBase httpContext)
        {
            try
            {
                bool SkipCacheQueryString = IsSkipCacheQueryStringExists(httpContext);
                if (PortalAgent.CurrentPortal.IsFullPageCacheActive && !SkipCacheQueryString)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, "CustomPageCacheIssue", TraceLevel.Error);
                return false;
            }
        }

        private bool IsSkipCacheQueryStringExists(HttpContextBase httpContext)
        {
            List<string> queryStringKeys = new List<string> { "pagenumber", "sort", "viewAll", "cmsmode", "pageSize" };

            bool isSkipCache = false;
            bool isFromSearch;

            NameValueCollection _queryString = httpContext.Request.QueryString;

            foreach (string item in queryStringKeys)
            {
                if (_queryString[item] != null)
                {
                    isSkipCache = true;
                    break;
                }
                else
                {
                    isSkipCache = false;
                }
            }

            //Parse for from search value
            bool.TryParse(_queryString["fromSearch"], out isFromSearch);

            return isSkipCache || isFromSearch;
        }
    }
}

