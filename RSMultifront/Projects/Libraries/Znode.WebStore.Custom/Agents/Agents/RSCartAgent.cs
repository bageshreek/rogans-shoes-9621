﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSCartAgent : CartAgent, ICartAgent, IRSCartAgent
    {
        #region Private member      
        
        double cookiesexpiretime = Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]);
        #endregion

        #region Constructor
        public RSCartAgent(IShoppingCartClient shoppingCartsClient, IPublishProductClient publishProductClient, IAccountQuoteClient accountQuoteClient, IUserClient userClient) : base(shoppingCartsClient, publishProductClient, accountQuoteClient, userClient)
        {
           
        }
        #endregion

        #region PickUp/Ship

        /// <summary>
        /// Assign StoreValues To CartItem and then calls base method
        /// </summary>
        /// <param name="cartItem"></param>
        /// <returns></returns>
        public override AddToCartViewModel AddToCartProduct(AddToCartViewModel cartItem)
        {            
            if (IsNotNull(cartItem))
            {
                cartItem = AssignStoreValuesToCartItem(cartItem);
                cartItem = base.AddToCartProduct(cartItem);
                return cartItem;
            }
            return null;
        }

        /// <summary>
        /// Calls Base Method and then Assign Store Values To Cart returned by base method to be refleted on Webstore
        /// </summary>
        /// <param name="isCalCulateTaxAndShipping"></param>
        /// <param name="isCalculateCart"></param>
        /// <param name="isCalculatePromotionAndCoupon"></param>
        /// <returns></returns>
        public override CartViewModel GetCart(bool isCalCulateTaxAndShipping = true, bool isCalculateCart = true, bool isCalculatePromotionAndCoupon = true)
        {
            
            /*To Be USED in case you are getting No items in cart even after having successful Add to cart
             Capture cookie from AddToCartProduc tMethod ofRSCartAgent and save here.
            string cookie = GetFromCookie(WebStoreConstants.CartCookieKey);
            if (cookie == "")
            {
                SaveInCookie(WebStoreConstants.CartCookieKey, cookie, cookiesexpiretime);
            }*/

            CartViewModel cart = base.GetCart(isCalCulateTaxAndShipping, isCalculateCart, isCalculatePromotionAndCoupon);
            cart = AssignStoreValuesToBaseCart(cart);
            return cart;
        }

        /// <summary>
        /// If User selected Ship option in PDP and Changes it to PickUp in cart, 
        /// updates SHoppingCartItems Ship/PickUp details,updates default store details if changed in cart and 
        /// Adds Productid's in cookie for which change is done to be used in GetCart()
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="storeName"></param>
        /// <param name="storeAddress"></param>
        /// <param name="productId"></param>
        public void UpdateDeliveryPreferenceToPickUp(string storeId, string storeName, string storeAddress, int productId = 0)
        {
            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);

            string deliverytype = shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == productId)?.FirstOrDefault()?.Custom4;
            if (storeId == "" && storeName != "")
            {
                /*Case Ship to PickUp by clicking on Pick Up*/
                string defaultStore = GetFromCookie("DefaultSelectedStore");
                
                string[] storeValues = defaultStore.Split('*');
                if (storeValues.Length > 1)
                {
                    storeId = storeValues[0];
                    storeName = storeValues[1];
                    storeAddress = storeValues[2];
                    shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == productId).ToList().ForEach(cc => cc.Custom4 = "PICKUP");
                }               
            }
            else
            {
                /*Case Pick up already clicked then click on Change Store Or Ship to Pick up by clicking Change Store*/
                shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == productId).ToList().ForEach(cc => cc.Custom4 = "PICKUP");
                string defaultSelectedStoreDetails = storeId + "*" + storeName + "*" + storeAddress;
                RemoveCookie("DefaultSelectedStore");
                SaveInCookie("DefaultSelectedStore", defaultSelectedStoreDetails, cookiesexpiretime);
            }

            if (shoppingCartModel != null)
            {
                shoppingCartModel?.ShoppingCartItems.Where(x => x.Custom4 == "PICKUP").ToList().ForEach(cc => cc.Custom1 = storeId);
                shoppingCartModel?.ShoppingCartItems.Where(x => x.Custom4 == "PICKUP").ToList().ForEach(cc => cc.Custom2 = storeName);
                shoppingCartModel?.ShoppingCartItems.Where(x => x.Custom4 == "PICKUP").ToList().ForEach(cc => cc.Custom3 = storeAddress);
                shoppingCartModel?.ShoppingCartItems.Where(x => x.Custom4 == "PICKUP").ToList().ForEach(cc => cc.Custom4 = "PICKUP");

                shoppingCartModel?.ShoppingCartItems.Where(x => x.Custom4 == "SHIP").ToList().ForEach(cc => cc.Custom2 = storeName);
            }
            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            string changedCartDetails = GetFromCookie("ChangedCart");
            if (changedCartDetails.Contains("|Ship-" + productId.ToString()))
            {
                changedCartDetails = changedCartDetails.Replace("|Ship-" + productId.ToString(), "");
            }
            changedCartDetails = changedCartDetails + "|PickUp-" + productId; 
            RemoveCookie("ChangedCart");
            SaveInCookie("ChangedCart", changedCartDetails, cookiesexpiretime);
            
        }

        /// <summary>
        /// If User selected PickUp option in PDP and Changes it to Ship in cart, 
        /// updates ShoppingCartItems Ship/PickUp details,updates default store details if changed in cart and 
        /// Adds Productid's in cookie for which change is done to be used in GetCart()
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="storeName"></param>
        /// <param name="storeAddress"></param>
        /// <param name="productId"></param>
        public void UpdateDeliveryPreferenceToShip(int productId)
        {
            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);
            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == productId).ToList().ForEach(cc => cc.Custom1 = "");
            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == productId).ToList().ForEach(cc => cc.Custom3 = "");
            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == productId).ToList().ForEach(cc => cc.Custom4 = "SHIP");
            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            string changedCartDetails = GetFromCookie("ChangedCart");
            if (changedCartDetails.Contains("|PickUp-" + productId.ToString()))
            {
                changedCartDetails=changedCartDetails.Replace("|PickUp-"+productId.ToString(), "");
            }
            changedCartDetails= changedCartDetails+ "|Ship-" + productId;
            RemoveCookie("ChangedCart");
            SaveInCookie("ChangedCart", changedCartDetails, cookiesexpiretime);
        }
        #endregion
        
        /// <summary>
        /// Merge Cart after login
        /// </summary>
        /// <returns></returns>
        public override bool MergeGuestUserCart()
        {
            bool status = false;
            ShoppingCartModel cart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                                     GetCartFromCookie();
            List<ShoppingCartItemModel> OrgItems = new List<ShoppingCartItemModel>();
            //Merge cart
            if (cart?.ShoppingCartItems?.Count() > 0)
            {
                List<string> deliverytype = cart?.ShoppingCartItems.Select(x => x.Custom4).Distinct().ToList();

                if (deliverytype.Contains("PICKUP"))
                {
                    OrgItems = cart?.ShoppingCartItems;
                }

                RemoveInSession(WebStoreConstants.CartModelSessionKey);
                status = _shoppingCartsClient.MergeGuestUsersCart(GetFiltersForMergeCart(cart.CookieMappingId, Convert.ToInt32(cart.ShoppingCartItems.FirstOrDefault()?.OmsSavedcartLineItemId)));
            }

            //Get shopping cart by userId.
            _shoppingCartsClient.SetProfileIdExplicitly(Znode.Engine.WebStore.Helper.GetProfileId().GetValueOrDefault());
            ShoppingCartModel cartModel = _shoppingCartsClient.GetShoppingCart(new CartParameterModel
            {
                UserId = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey).UserId,
                PortalId = PortalAgent.CurrentPortal.PortalId,
                LocaleId = PortalAgent.LocaleId,
                PublishedCatalogId = GetCatalogId().GetValueOrDefault()
            });

            cartModel.ShippingId = (cart?.ShippingId).GetValueOrDefault();
            cartModel.Coupons = cart?.Coupons;

            if (OrgItems.Count > 0)
            {
                foreach (ShoppingCartItemModel s in cartModel.ShoppingCartItems)
                {
                    ShoppingCartItemModel modifieditem = OrgItems?.Where(x => x.ProductId == s.ProductId).FirstOrDefault();
                    if (modifieditem != null)
                    {
                        s.Custom1 = modifieditem.Custom1;
                        s.Custom2 = modifieditem.Custom2;
                        s.Custom3 = modifieditem.Custom3;
                        s.Custom4 = modifieditem.Custom4;
                    }
                    /*Case when cart has two items with diffrent stores*/
                    ShoppingCartItemModel latestSelectedStore = OrgItems?.Where(x => x.Custom4 == "PICKUP")?.FirstOrDefault();
                    if (s.Custom4 == "PICKUP" && latestSelectedStore?.Custom1 != s.Custom1)
                    {
                        s.Custom1 = latestSelectedStore?.Custom1;
                        s.Custom2 = latestSelectedStore?.Custom2;
                        s.Custom3 = latestSelectedStore?.Custom3;
                        s.Custom4 = latestSelectedStore?.Custom4;
                    }
                }
            }
            else
            {
                /*Case when cart has two items with diffrent stores before login and after login*/
                string defaultStore = GetFromCookie("DefaultSelectedStore");
                string[] storeValues = defaultStore.Split('*');
                string storeId = "", storeName = "", storeAddress = "";
                if (storeValues.Length > 1)
                {
                    storeId = storeValues[0];
                    storeName = storeValues[1];
                    storeAddress = storeValues[2];

                }
                foreach (ShoppingCartItemModel item in cartModel.ShoppingCartItems)
                {

                    if (item.Custom4 == "PICKUP" && storeId != item.Custom1)
                    {
                        item.Custom1 = storeId;
                        item.Custom2 = storeName;
                        item.Custom3 = storeAddress;
                    }
                }

            }
            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);
            RemoveCookie(WebStoreConstants.CartCookieKey);
            ClearCartCountFromSession();
            return status;
        }

        /// <summary>
        /// For RS if FedEx Ground shipping cost is zero then customised Shipping code is dispalyed on webstore
        /// Hence changes made to get original Shipping code to be sent to base method.
        /// </summary>
        /// <param name="shippingOptionId"></param>
        /// <param name="shippingAddressId"></param>
        /// <param name="shippingCode"></param>
        /// <param name="additionalInstruction"></param>
        /// <param name="isQuoteRequest"></param>
        /// <param name="isCalculateCart"></param>
        /// <returns></returns>
        public override CartViewModel CalculateShipping(int shippingOptionId, int shippingAddressId, string shippingCode, string additionalInstruction = "", bool isQuoteRequest = false, bool isCalculateCart = true)
        {
            /*On Load Case*/
            if (shippingCode == null || shippingCode.Contains("undefined"))
            {
                CartViewModel cartViewModel = base.CalculateShipping(shippingOptionId, shippingAddressId, shippingCode, additionalInstruction, isQuoteRequest, isCalculateCart);
                if (cartViewModel.ShippingCost == 0 && cartViewModel.Shipping.ShippingDiscount != 0)
                {
                    cartViewModel.Shipping.ShippingDiscount = 0;
                }

                return cartViewModel;
            }
            /*On Click Case*/
            else
            {
                string[] shippingCodeval = shippingCode.Split('*');
                if (shippingCodeval.Length > 1)
                {
                    shippingCode = shippingCodeval[1];
                }
                else
                {
                    shippingCode = shippingCodeval[0];
                }

                CartViewModel cartViewModel = base.CalculateShipping(shippingOptionId, shippingAddressId, shippingCode, additionalInstruction);
                if (cartViewModel.ShippingCost == 0 && cartViewModel.Shipping.ShippingDiscount != 0)
                {
                    cartViewModel.Shipping.ShippingDiscount = 0;
                }

                string shipcost = shippingCodeval[0].Replace("$", string.Empty).Trim();
                if (shippingCodeval.Length == 1)
                {
                    shipcost = "0";
                    cartViewModel.FreeShipping = true;
                }
                if (shipcost.Contains("Free"))
                {
                    shipcost = shipcost.Replace("(Free)", "");
                    cartViewModel.FreeShipping = true;
                }
                decimal shippingcost = 0;
                if (shipcost != "undefined")
                {
                    shippingcost = Convert.ToDecimal(shipcost);
                }

                if (cartViewModel.ShippingCost != shippingcost)
                {
                    if (cartViewModel.ErrorMessage == "Unable to calculate shipping rates at this time, please try again later.")
                    {
                        cartViewModel.ErrorMessage = "";
                    }

                    cartViewModel.Total = cartViewModel.Total - cartViewModel.ShippingCost;
                    cartViewModel.Total = cartViewModel.Total + shippingcost;
                    cartViewModel.ShippingCost = shippingcost;
                    ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ?? GetCartFromCookie();
                    cartModel.ShippingCost = cartViewModel.ShippingCost;
                    cartModel.TaxCost = cartViewModel.TaxCost;
                    cartModel.Total = cartViewModel.Total;
                    SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);
                }

                return cartViewModel;
            }
        }

        /// <summary>
        /// Sent value of cartCountAfterLogin parameter of ReturnCartCount method as true for proper calculation of cartcount in Header on login
        /// </summary>
        /// <returns></returns>
        public override decimal GetCartCount()
        {
            decimal? cartCount = GetFromSession<decimal?>(WebStoreConstants.CartCount);

            if (IsNull(cartCount))
            {
                /*Nivi Code*/
                cartCount = ReturnCartCount(true);
                SaveInSession<decimal?>(WebStoreConstants.CartCount, cartCount);
            }

            return cartCount.GetValueOrDefault();
        }

        #region Private Methods
        /// <summary>
        /// Assigns Store values selected in PDP depending on Ship/PickUp option selected 
        /// to cart Item and Cookie.
        /// </summary>
        /// <param name="cartItem"></param>
        /// <returns></returns>
        private AddToCartViewModel AssignStoreValuesToCartItem(AddToCartViewModel cartItem)
        {
            /*PICK UP IN STORE Cookie section*/
            string defaultStore, defaultSelectedStoreDetails = string.Empty;
            string selectedStoreID = cartItem.Custom1;
            string selectedStoreName = cartItem.Custom2;
            string selectedStoreAddress = cartItem.Custom3;

            if (string.IsNullOrEmpty(selectedStoreID))/*SHIP Case*/
            {
                defaultStore = GetFromCookie("DefaultSelectedStore");
                string[] storeValues = defaultStore.Split('*');
                defaultSelectedStoreDetails = defaultStore;
                if (storeValues.Length > 1)
                {
                    cartItem.Custom2 = storeValues[1];
                }

                cartItem.Custom4 = "SHIP";
            }
            else/*PICK UP Case*/
            {
                defaultSelectedStoreDetails = selectedStoreID + "*" + selectedStoreName + "*" + selectedStoreAddress;
                cartItem.Custom4 = "PICKUP";
            }

            RemoveCookie("DefaultSelectedStore");
            SaveInCookie("DefaultSelectedStore", defaultSelectedStoreDetails, cookiesexpiretime);
            return cartItem;
        }

        /// <summary>
        /// Assigns Store values depending on Ship/PickUp option selected in PDP and changes done in cart to previously added product   
        /// to base cart and Cookie.
        /// </summary>
        /// <param name="cartItem"></param>
        /// <returns></returns>
        private CartViewModel AssignStoreValuesToBaseCart(CartViewModel cart)
        {
            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);


            if (shoppingCartModel != null)
            {
                string defaultStore = GetFromCookie("DefaultSelectedStore");
                string[] storeValues = defaultStore.Split('*');
                /*IF  -if Default Store is selected then all cart items should show this stores details
                  ELSE-copy details from previously saved cart */
                if (storeValues.Length > 1)
                {
                    /*Start Code for Previous Products Ship/PickUp Option changed in Cart to be retained when new product is added*/
                    string changedCartDetails = GetFromCookie("ChangedCart");
                    if (changedCartDetails != "")
                    {
                        shoppingCartModel = UpdateChangedCartDetails(shoppingCartModel, changedCartDetails, storeValues);
                    }
                    /*End Code for Previous Products Ship/PickUp Option changed in Cart to be retained when new product is added*/

                    foreach (ShoppingCartItemModel s in shoppingCartModel.ShoppingCartItems)
                    {
                        /*if case - if any items in cart have Pick up in store option selected then store details will be updated to latest default selected store.
                         else case - only storename will be updated to latest default selected store name for Ship Case*/
                        int prdid = s.ProductId;
                        if (s.Custom1 != "" && s.Custom1 != null)
                        {
                            cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom1 = storeValues[0]);
                            cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom2 = storeValues[1]);
                            cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom3 = storeValues[2]);
                            s.Custom1 = storeValues[0];
                            s.Custom2 = storeValues[1];
                            s.Custom3 = storeValues[2];
                        }
                        else
                        {
                            cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom1 = s.Custom1);
                            cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom2 = storeValues[1]);
                            cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom3 = s.Custom3);
                            s.Custom2 = storeValues[1];
                        }
                    }
                }
                else
                {
                    foreach (ShoppingCartItemModel s in shoppingCartModel.ShoppingCartItems)
                    {
                        cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom1 = s.Custom1);
                        cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom2 = s.Custom2);
                        cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom3 = s.Custom3);
                    }
                }
            }
            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            return cart;
        }

        /// <summary>
        /// Previous Products Ship/PickUp Option changed in Cart to be retained when new product is added
        /// </summary>
        /// <param name="shoppingCartModel"></param>
        /// <param name="changedCartDetails"></param>
        /// <returns></returns>
        private ShoppingCartModel UpdateChangedCartDetails(ShoppingCartModel shoppingCartModel, string changedCartDetails, string[] storeValues)
        {
            string[] cartValues = changedCartDetails.Split('|');
            if (cartValues.Length > 1)
            {
                foreach (string productid in cartValues)
                {
                    if (productid != "")
                    {
                        if (productid.Contains("PickUp"))
                        {
                            string prodid = productid.Replace("PickUp-", "");
                            /*Product was set to PickUp from ship in cart Case*/
                            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == Convert.ToInt32(prodid)).ToList().ForEach(cc => cc.Custom1 = storeValues[0]);
                            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == Convert.ToInt32(prodid)).ToList().ForEach(cc => cc.Custom2 = storeValues[1]);
                            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == Convert.ToInt32(prodid)).ToList().ForEach(cc => cc.Custom3 = storeValues[2]);
                            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == Convert.ToInt32(prodid)).ToList().ForEach(cc => cc.Custom4 = "PICKUP");
                        } /*Product was set to ship from PickUp in cart Case*/
                        else if (productid.Contains("Ship"))
                        {
                            string prodid = productid.Replace("Ship-", "");
                            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == Convert.ToInt32(prodid)).ToList().ForEach(cc => cc.Custom1 = "");
                            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == Convert.ToInt32(prodid)).ToList().ForEach(cc => cc.Custom2 = storeValues[1]);
                            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == Convert.ToInt32(prodid)).ToList().ForEach(cc => cc.Custom3 = "");
                            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == Convert.ToInt32(prodid)).ToList().ForEach(cc => cc.Custom4 = "SHIP");
                        }
                    }
                }
            }
            return shoppingCartModel;
        }

        /// <summary>
        /// Updates price in cart in Impersonation case.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="unitPrice"></param>
        /// <param name="productId"></param>
        /// <param name="shippingId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public virtual CartViewModel UpdateCartItemPrice(string guid, decimal unitPrice, int productId, int shippingId, int? userId = 0)
        {
            // Get shopping cart from session.
            ShoppingCartModel shoppingCart  = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);

            if (IsNotNull(shoppingCart))
            {
                string sku = GetProductSku(shoppingCart?.ShoppingCartItems?.FirstOrDefault(w => w.ExternalId == guid), productId);
                IPublishProductClient _publishProductClient = GetClient<PublishProductClient>();
                ProductInventoryPriceListModel productInventory = _publishProductClient.GetProductPriceAndInventory(new ParameterInventoryPriceModel { Parameter = sku, PortalId = shoppingCart.PortalId });

                decimal cartItemPrice = (productInventory?.ProductList?.FirstOrDefault(w => w.SKU == sku)?.SalesPrice).GetValueOrDefault();

                //Update quantity and update the cart.
                //if (productId > 0)
                //{
                //    shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(x => x.GroupProducts?.Where(y => y.ProductId == productId)?.Select(z => { z.UnitPrice = unitPrice; return z; })?.FirstOrDefault())?.ToList();
                //    shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(z => { z.ExtendedPrice = Convert.ToDecimal((z.UnitPrice * z.Quantity).ToInventoryRoundOff()); return z; })?.ToList();

                //    if (cartItemPrice == unitPrice)
                //    {
                //        shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(z => { z.CustomUnitPrice = null; return z; })?.ToList();
                //    }
                //    else
                //        shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(z => { z.CustomUnitPrice = unitPrice; return z; })?.ToList();
                //}
                //else
                //{
                    shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(y => { y.UnitPrice = Convert.ToDecimal(unitPrice.ToInventoryRoundOff()); return y; })?.ToList();
                    shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(y => { y.ExtendedPrice = Convert.ToDecimal((y.UnitPrice * y.Quantity).ToInventoryRoundOff()); return y; })?.ToList();

                    if (cartItemPrice == unitPrice)
                    {
                        shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(y => { y.CustomUnitPrice = null; return y; })?.ToList();
                    }
                    else
                        shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(y => { y.CustomUnitPrice = Convert.ToDecimal(unitPrice.ToInventoryRoundOff()); return y; })?.ToList();
               // }
                shoppingCart.SubTotal = Convert.ToDecimal(shoppingCart.ShoppingCartItems?.Sum(x => x.ExtendedPrice));

                // SaveCartModelInSession(userId, shoppingCart, true);
                SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCart);
            }
            return shoppingCart?.ToViewModel<CartViewModel>();
        }
        protected virtual string GetProductSku(ShoppingCartItemModel cartItem, int productId)
        {
            string sku = string.Empty;
            if (IsNotNull(cartItem))
            {
               if (!string.IsNullOrEmpty(cartItem.ConfigurableProductSKUs))
                {
                    sku = cartItem.ConfigurableProductSKUs;
                }
                else
                    sku = cartItem.SKU;
            }
            return sku;
        }
        protected override CartViewModel ApplyCoupon(string couponCode, ShoppingCartModel cartModel)
        {
            ShoppingCartModel oldShoppingCartModel = cartModel;

            //Get coupons that are applied to cart.
            List<CouponModel> promocode = GetCoupons(cartModel);

            List<CouponViewModel> invalidCoupons = new List<CouponViewModel>();

            //Bind new coupon to shopping cart model to apply.
            if (!string.IsNullOrEmpty(couponCode))
            {
                GetNewCouponToApply(cartModel, couponCode);
            }
            else
            {
                invalidCoupons.Add(new CouponViewModel { Code = couponCode, PromotionMessage = WebStore_Resources.ErrorEmptyCouponCode, CouponApplied = false, CouponValid = false });
            }

            //Get invalid coupon list form cart.
            List<CouponModel> invalidCouponslist = GetInvalidCouponList(cartModel);

            RemoveInvalidCoupon(cartModel);
            //Calculate coupon for the cart.
            ShoppingCartModel calculatedCart = _shoppingCartsClient.Calculate(cartModel);

            if (IsNotNull(calculatedCart) && IsNotNull(cartModel))
            {
                cartModel.Coupons = calculatedCart.Coupons;
                cartModel.Total = calculatedCart.Total;
                cartModel.Discount = calculatedCart.Discount;
                cartModel.ShippingCost = calculatedCart.ShippingCost;
                cartModel.GiftCardAmount = calculatedCart.GiftCardAmount;
                cartModel.TaxCost = calculatedCart.TaxCost;
                cartModel.OrderLevelDiscountDetails = calculatedCart.OrderLevelDiscountDetails;

                if (IsNotNull(cartModel.ShoppingCartItems) && IsNotNull(calculatedCart.ShoppingCartItems))
                    SetCalculatedDetailsInLineItem(cartModel.ShoppingCartItems, calculatedCart.ShoppingCartItems);
            }

            if (IsNotNull(invalidCouponslist))
                cartModel.Coupons?.AddRange(invalidCouponslist);

            MapQuantityOnHandAndSeoName(oldShoppingCartModel, cartModel);
            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);

            CartViewModel cartViewModel = calculatedCart?.ToViewModel<CartViewModel>();
            //Bind User details to ShoppingCartModel.
            cartViewModel.CustomerPaymentGUID = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.CustomerPaymentGUID;
            return cartViewModel;
        }
        protected override void GetNewCouponToApply(ShoppingCartModel cartModel, string couponCode)
        {
            //Check if any coupon is already appled to cart and add new coupon.
            bool isCouponNotAvailableInCart = Equals(cartModel.Coupons?.Where(p => Equals(p.Code, couponCode)).ToList().Count, 0);

            //Add coupon code if not present in cart.
            if (isCouponNotAvailableInCart)
            {
                cartModel.Coupons.Add(new CouponModel { Code = couponCode });
            }
        }
        #endregion


    }
}
