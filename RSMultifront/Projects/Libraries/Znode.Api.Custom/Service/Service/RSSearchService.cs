﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Search;

namespace Znode.Api.Custom.Service.Service
{
    public class RSSearchService : SearchService
    {
        public RSSearchService()
        {

        }
        //public override KeywordSearchModel FullTextSearch(SearchRequestModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        //{            
        //    ZnodeLogging.LogMessage("RSEARCHSERVICE -FULLTEXTSEARCH STARTED AT-" + DateTime.Now + "FOR CATEGORY="+ model.Category, "PLP", TraceLevel.Error);
        //    KeywordSearchModel searchResult = base.FullTextSearch(model, expands, filters, sorts, page);            
        //    ZnodeLogging.LogMessage("RSEARCHSERVICE -FULLTEXTSEARCH ENDED AT-" + DateTime.Now + "FOR CATEGORY=" + model.Category, "PLP", TraceLevel.Error);
        //    return searchResult;
        //}

        /// <summary>
        /// For testing purpose just to log the search suggestions
        /// </summary>
        /// <param name="model"></param>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        //public override KeywordSearchModel GetKeywordSearchSuggestion(SearchRequestModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        //{
        //    KeywordSearchModel searchResult = base.GetKeywordSearchSuggestion(model,expands,filters,sorts,page);
        //    searchResult.Products?.ForEach(x =>
        //    {
        //        ZnodeLogging.LogMessage("Name -" + x.Name +" and Category="+ x.CategoryName, "searchsuggestion", TraceLevel.Info);
        //    });
        //        return searchResult;
        //}
        //public override KeywordSearchModel GetKeywordSearchSuggestion(SearchRequestModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        //{
        //    ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        //    var searchProvider = znodeSearchProvider;

        //    ZnodeLogging.LogMessage("CatalogId and LocaleId to get version Id: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { model?.CatalogId, model?.LocaleId });
        //    int? versionId = GetCatalogVersionId(model.CatalogId, model.LocaleId);
        //    ZnodeLogging.LogMessage("versionId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, versionId);
        //    filters.Add(WebStoreEnum.VersionId.ToString().ToLower(), FilterOperators.Equals, Convert.ToString(versionId));

        //    var searchRequest = GetZnodeSearchRequest(model, filters, sorts, true);
        //    searchRequest.GetFacets = false;

        //    IZnodeSearchResponse suggestions = searchProvider.SuggestTermsFor(searchRequest);
        //    string log = "";
        //    int i = 1;
        //    foreach (var resp in suggestions.ProductDetails)
        //    {
        //        //string sku = resp.GetType().GetProperty("sku").GetValue();
        //        string sku = resp["sku"];
        //        string productname = resp["productname"];
        //        string category = resp["categoryname"];
        //        // string znodecatalogid = resp["znodecatalogid"];
        //        log = log + "  ," + i.ToString() + ". SKU =" + sku + " PRODUCTNAME = " + productname + " CATEGORY = " + category;

        //        i = i + 1;
        //    }

        //    ZnodeLogging.LogMessage("GETKEYWORDSEARCHSUGGESTIONS IZnodeSearchResponse: " + log, "searchsuggestions", TraceLevel.Verbose);
        //    KeywordSearchModel searchResult = HelperUtility.IsNotNull(suggestions) ? GetKeywordSearchModel(suggestions) : new KeywordSearchModel();
        //    if (!model.IsAutocomplete && HelperUtility.IsNotNull(searchResult))
        //    {
        //        if (searchResult.Products?.Count > 0)
        //            //get expands associated to Product
        //            publishProductHelper.GetDataFromExpands(model.PortalId, GetExpands(expands), searchResult.Products, model.LocaleId, GetLoginUserId(), versionId ?? 0, GetProfileId());

        //        ZnodeLogging.LogMessage("portalId and productList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { model?.PortalId, searchResult?.Products?.Count });
        //        GetProductImagePathForSuggestions(model.PortalId, searchResult.Products);
        //        //set stored based In Stock, Out Of Stock, Back Order Message.
        //        SetPortalBasedDetails(model.PortalId, searchResult.Products);
        //    }
        //    ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        //    return searchResult;
        //}

        /// <summary>
        /// Overridden to add publishstate filter while fetching data from ZnodePublishProductEntity
        /// As some products had diff product id's for PREVIEW and PRODUCTION ,
        /// it was returning wrong ProductId for same SKU and PDP was going blank        
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="model"></param>
        protected override void ProductSeoType(FilterCollection filters, SEOUrlModel model)
        {
           
            filters.Add(new FilterTuple(FilterKeys.SKU, FilterOperators.Is, model.SeoCode));
            
            ReplaceFilterKeys(ref filters);
            ZnodePublishStatesEnum publishState = GetPortalPublishState();
            filters.Add(WebStoreEnum.RevisionType.ToString(), FilterOperators.Is, Convert.ToString(publishState));
            ZnodePublishProductEntity product = ZnodeDependencyResolver.GetService<IPublishedProductDataService>().GetPublishProductByFilters(filters);

            if (product!= null)
            {
                model.ProductId = product.ZnodeProductId;
                model.ProductName = product.Name;
                model.IsActive = product.IsActive;
                model.SEOId = product.ZnodeProductId;
                model.SeoCode = product.SKU;
            }
            
        }

        protected override void CategorySeoType(FilterCollection filters, SEOUrlModel model)
        {
            //ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            string localeId = filters.Find(x => x.FilterName.ToLower() == ZnodeLocaleEnum.LocaleId.ToString().ToLower())?.FilterValue;
            string catalogId = filters.Find(x => x.FilterName.ToLower() == ZnodePimCatalogEnum.PimCatalogId.ToString().ToLower())?.FilterValue;

            //ZnodeLogging.LogMessage("localeId and catalogId to create query: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { localeId, catalogId });

            filters.Add(new FilterTuple(FilterKeys.ZnodeCategoryId, FilterOperators.Equals, model.SEOId.ToString()));
            ReplaceFilterKeys(ref filters);

            if (HelperUtility.IsNotNull(localeId) && HelperUtility.IsNotNull(catalogId))
            {
                ZnodeLogging.LogMessage("NIVI CategorySEOTYPE  SEOUrlModel.seocode="+ model.SeoCode + ",CatalogVersionId="+ GetCatalogVersionId(int.Parse(catalogId), int.Parse(localeId)), "RSSEARCHSERVICE", TraceLevel.Info);
                ZnodePublishCategoryEntity category = ZnodeDependencyResolver.GetService<IPublishedCategoryDataService>().GetCategoryListByCatalogId(int.Parse(catalogId), int.Parse(localeId))?.FirstOrDefault(x => x.CategoryCode.ToLower() == model.SeoCode.ToLower() && x.VersionId == GetCatalogVersionId(int.Parse(catalogId), int.Parse(localeId)));

                if (HelperUtility.IsNotNull(category))
                {
                    ZnodeLogging.LogMessage("NIVI Category " + category.Name + "is not null", "RSSEARCHSERVICE", TraceLevel.Info);
                    model.CategoryId = category.ZnodeCategoryId;
                    model.CategoryName = category.Name;

                    //Make category De-Active,if category is De-Active./ActivationDate is greater than current date./ExpirationDate is less than current date.
                    if (category.IsActive &&
                    (category.ActivationDate == null || category.ActivationDate.GetValueOrDefault().Date <= HelperUtility.GetDate())
                      && (category.ExpirationDate == null || category.ExpirationDate.GetValueOrDefault().Date >= HelperUtility.GetDate()))
                    {
                        model.IsActive = category.IsActive;
                    }
                    else
                        model.IsActive = false;

                    model.SEOId = category.ZnodeCategoryId;
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

    }
}
