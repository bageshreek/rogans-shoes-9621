﻿namespace klaviyo.net
{
    public class KlaviyoItems
    {
        public decimal Qunatity { get; set; }
        public string ProductName { get; set; }
        public string SKU { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal RowTotal { get; set; }
        public string ProductURL { get; set; }
        public string ImageURL { get; set; }
        public string Description { get; set; }
    }
}
