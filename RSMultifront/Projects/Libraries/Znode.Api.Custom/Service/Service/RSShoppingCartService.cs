﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

//using Znode.Libraries.ECommerce.ShoppingCart;

namespace Znode.Api.Custom.Service.Service
{
    public class RSShoppingCartService : ShoppingCartService
    {
        private const string FEDEXGROUND = "FEDEX_GROUND";
        private readonly ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
        private const string THRESHOLD_FREE_SHIPPING_PROMOTION = "Threshold Free Shipping Value";

        public RSShoppingCartService() : base()
        {
        }

        /// <summary>
        /// Assign StoreValues To ShoppingCartItems and then calls base method
        /// </summary>
        /// <param name="shoppingCart"></param>
        /// <returns></returns>
        public override AddToCartModel AddToCartProduct(AddToCartModel shoppingCart)
        {
            try
            {
                string selectedStoreID = shoppingCart.Custom1 == null ? "" : shoppingCart.Custom1;
                string selectedStoreName = shoppingCart.Custom2 == null ? "" : shoppingCart.Custom2;
                string selectedStoreAddress = shoppingCart.Custom3 == null ? "" : shoppingCart.Custom3;

                shoppingCart?.ShoppingCartItems.ToList().ForEach(cc => cc.Custom1 = selectedStoreID);
                shoppingCart?.ShoppingCartItems.ToList().ForEach(cc => cc.Custom2 = selectedStoreName);
                shoppingCart?.ShoppingCartItems.ToList().ForEach(cc => cc.Custom3 = selectedStoreAddress);
                shoppingCart?.ShoppingCartItems.ToList().ForEach(cc => cc.Custom5 = shoppingCart.Custom5);

                base.AddToCartProduct(shoppingCart);

                return shoppingCart;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, "RSShoppingCartService", TraceLevel.Error, ex);
                return base.AddToCartProduct(shoppingCart);
            }
        }

        /// <summary>
        /// Overridden to implement RS specific requirements
        /// </summary>
        /// <param name="zipCode"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public override ShippingListModel GetShippingEstimates(string zipCode, ShoppingCartModel model)
        {
            ShippingListModel list = base.GetShippingEstimates(zipCode, model);

            /*NIVI CODE BEGIN*/

            /*1. When  All Items in cart are Pick Up In store Items then only PICKUPFREE shipping Option is applicable */
            List<ShoppingCartItemModel> _pickupinstoreItems = model.ShoppingCartItems?.Where(x => x.Custom1 != string.Empty && x.Custom1 != null).ToList<ShoppingCartItemModel>();
            if (_pickupinstoreItems != null && _pickupinstoreItems.Count == model.ShoppingCartItems.Count)
            {
                list?.ShippingList?.RemoveAll(r => (r.ShippingCode != "PICKUPFREE")); //&& r.ShippingCode != "FLAT");
                return list;
            }
            else
                list?.ShippingList?.RemoveAll(r => (r.ShippingCode == "PICKUPFREE"));
           
            List<PromotionModel> AllPromotions = promotionHelper.GetAllPromotions();
            PromotionModel freeshipping = AllPromotions?.Where(x => x.Name == THRESHOLD_FREE_SHIPPING_PROMOTION).FirstOrDefault();
            if (model.SubTotal< freeshipping?.OrderMinimum)
            {
                list?.ShippingList?.RemoveAll(r => (r.ShippingCode != "FLAT"));
                return list;
            }
            else
            {
                /*2. Display only FedEx Ground option if Cart contains only IsAvailableOnlineOnly Products*/
                ShoppingCartMap SCM = new ShoppingCartMap();

                ZnodeShoppingCart znodeShoppingCart = SCM.ToZnodeShoppingCart(model);
                string stateCode = GetStateCode(model.ShippingAddress?.StateName);
                int totalShoppingCartItemsCount = znodeShoppingCart.ShoppingCartItems.Count();

                int availableOnlineOnlyProductCount = znodeShoppingCart.ShoppingCartItems.SelectMany(x => x.Product.Attributes.Where(y => y.AttributeCode == "IsAvailableOnlineOnly" && y.AttributeValue == "true")).Count();
                if (totalShoppingCartItemsCount == availableOnlineOnlyProductCount && stateCode != "AK" && stateCode != "HI")
                {
                    list?.ShippingList?.RemoveAll(r => (r.ShippingCode != FEDEXGROUND));
                }

                /*3. For Hawaii And Alaska State only USPS Option should be dispalyed*/
                if (stateCode == "AK" || stateCode == "HI")
                {            //               
                    list?.ShippingList?.RemoveAll(r => (r.ShippingTypeName?.ToLower() != ZnodeConstant.USPS.ToLower() && r.ShippingCode != "FLAT"));
                }

                /*4. Set ApproximateArrival and EstimateDate*/
                foreach (ShippingModel item in list?.ShippingList)
                {
                    item.ApproximateArrival = znodeShoppingCart.ApproximateArrival == null ? "" : Convert.ToDateTime(znodeShoppingCart.ApproximateArrival).ToShortDateString();
                    if (item.ShippingCode == FEDEXGROUND)
                        item.EstimateDate = znodeShoppingCart.ApproximateArrival == null ? "" : Convert.ToDateTime(znodeShoppingCart.ApproximateArrival).AddDays(2).ToShortDateString();
                }
                if (!Equals(znodeShoppingCart?.Shipping?.ResponseCode, "0"))
                    ZnodeLogging.LogMessage("Shipping ERROR :item=" + znodeShoppingCart?.Shipping?.ShippingCode + " Error Code=" + znodeShoppingCart?.Shipping?.ResponseCode + " Error Description=" + znodeShoppingCart?.Shipping?.ResponseMessage, "Custom1", TraceLevel.Error);
                if (list.ShippingList?.Count > 1)
                {
                    list.ShippingList?.RemoveAll(x => x.ShippingCode == "FLAT");
                }
            }
            /*NIVI CODE ENDS*/
            return list;

        }
    }
}
