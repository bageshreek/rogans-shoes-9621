﻿using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints
{
    public class RSCustomizedFormEndpoints : BaseEndpoint
    {
        public static string ScheduleATruck() => $"{ApiRoot}/rscustomizedform/scheduleatruck";

        public static string ShoeFinder() => $"{ApiRoot}/rscustomizedform/shoefinder";
    }
}
