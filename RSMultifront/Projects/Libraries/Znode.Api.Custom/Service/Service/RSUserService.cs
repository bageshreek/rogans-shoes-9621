﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using Znode.Engine.Exceptions;
using System.Collections;
using System.Net;
using System.IO;
using System.Web;
using System.Configuration;
using Znode.Libraries.Framework.Business;
using System.Diagnostics;
using Microsoft.AspNet.Identity;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Resources;
using ApplicationUser = Znode.Engine.Services.ApplicationUser;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Data.Helpers;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Script.Serialization;

namespace Znode.Api.Custom.Service.Service
{
    public class RSUserService: UserService
    {
        private readonly IZnodeRepository<ZnodeUser> _userRepository;
        private readonly IZnodeRepository<ZnodeUserProfile> _accountProfileRepository;
        private readonly IZnodeRepository<ZnodeUserPortal> _userPortalRepository;
        private readonly IZnodeRepository<ZnodePortalProfile> _portalProfileRepository;
        private readonly IZnodeRepository<ZnodeProfile> _profileRepository;
        private readonly IZnodeRepository<ZnodeDepartmentUser> _departmentUserRepository;
        private readonly IZnodeRepository<ZnodeAccountProfile> _accountAssociatedProfileRepository;
        private readonly IZnodeRepository<ZnodeAccountPermissionAccess> _accountPermissionAccessRepository;
        private readonly IZnodeRepository<ZnodeAccountPermission> _accountPermissionRepository;

        string ResponseAPIKey= ConfigurationManager.AppSettings["GetResponseAPIKey"];
        string ResAPIUrl = ConfigurationManager.AppSettings["GetResponseAPIURL"];
        string CampaignName = ConfigurationManager.AppSettings["CampaignName"];
        UserLoginHelper LoginHelper = new UserLoginHelper();
        #region Constructor

        public RSUserService() : base()
        {
            _userRepository = new ZnodeRepository<ZnodeUser>();
            _accountProfileRepository = new ZnodeRepository<ZnodeUserProfile>();
            _userPortalRepository = new ZnodeRepository<ZnodeUserPortal>();
            _portalProfileRepository = new ZnodeRepository<ZnodePortalProfile>();
            _profileRepository = new ZnodeRepository<ZnodeProfile>();
            _departmentUserRepository = new ZnodeRepository<ZnodeDepartmentUser>();
            _accountAssociatedProfileRepository = new ZnodeRepository<ZnodeAccountProfile>();
            _accountPermissionAccessRepository = new ZnodeRepository<ZnodeAccountPermissionAccess>();
            _accountPermissionRepository = new ZnodeRepository<ZnodeAccountPermission>();
           
        }

        #endregion Constructor
        /// <summary>
        /// Calls AddContactEmailSubcriber
        /// </summary>
        /// <param name="Email"></param>
        public void GetNewsLetterCampaign(string Email)
        {
            if (ResponseAPIKey != null && ResAPIUrl != null)
            {              
                string newsLetterCampaign = CampaignName;
                if (!string.IsNullOrEmpty(newsLetterCampaign))
                {
                    string campaignID = AddContactEmailSubcriber(ResponseAPIKey, ResAPIUrl,
                        newsLetterCampaign, Email.Trim(), Email.Trim());
                }
            }
        }       

        /// <summary>
        /// Updates User data and Subscribe/unsubscribe user in GetResponse.
        /// </summary>
        /// <param name="userModel"></param>
        /// <param name="webStoreUser"></param>
        /// <returns></returns>
        public override bool UpdateUserData(UserModel userModel, bool webStoreUser)
        {
            bool returnParameter = base.UpdateUserData(userModel, webStoreUser);

            if (returnParameter)
            {
                if (userModel.EmailOptIn)   //Add to GetResponse
                {
                    try
                    {
                        string responseContactID = string.Empty;
                      
                        if (ResponseAPIKey != null && ResAPIUrl != null)
                        {

                            //string newsLetterCampaign = GetCompaignNameByPortal();
                            string newsLetterCampaign = CampaignName;
                            if (!string.IsNullOrEmpty(newsLetterCampaign))
                            {
                                string campaignID = AddContactEmailSubcriber(ResponseAPIKey, ResAPIUrl,
                                    newsLetterCampaign, userModel.Email.Trim(), userModel.Email.Trim());

                                //string contactid = emailSubscriber.GetContactEmailSubcriber(ConfigurationManager.AppSettings["GetResponseAPIKey"].ToString(), ConfigurationManager.AppSettings["GetResponseAPIURL"].ToString(), campaignID, Email.Text.Trim());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage(ex, "RSUserService:UpdateUserData ", TraceLevel.Error);
                    }
                }
                else  //Unsubscribe from GetResponse
                {
                    try
                    {
                        if (ResponseAPIKey != null && ResAPIUrl != null)
                        {
                            string contactID = userModel.Custom4;                          
                            if (!String.IsNullOrEmpty(contactID))
                                UnsubscribeContactEmail(ResponseAPIKey, ResAPIUrl, contactID);
                        }
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage(ex, "RSUserService:UpdateUserData ", TraceLevel.Error);
                    }

                }
            }
            return returnParameter;
        }

        /// <summary>
        /// Signs up user for Newsletter
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override bool SignUpForNewsLetter(NewsLetterSignUpModel model)
        {
            bool Isvalid = base.SignUpForNewsLetter(model);
            if (Isvalid)
            {
                try
                {
                    GetNewsLetterCampaign(model.Email);
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, "RSUserService:SignUpForNewsLetter ", TraceLevel.Error);
                }
            }
            return Isvalid;
        }
       
        // your API key
        // available at http://www.getresponse.com/my_api_key.html
        //String api_key = "8fbc4194dafbba2ea141e4c45e440d98";

        // API 2.x URL
        //String api_url = "http://api2.getresponse.com";

        /// <summary>
        /// Add Contact in GetResponse Email marketing
        /// </summary>
        /// <param name="api_key"></param>
        /// <param name="api_url"></param>
        /// <param name="campaignName"></param>
        /// <param name="contactName"></param>
        /// <param name="subscriberEmail"></param>
        /// <returns></returns>
        public string AddContactEmailSubcriber(string api_key, string api_url, string campaignName, string contactName, string subscriberEmail)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            // get CAMPAIGN_ID of 'sample_marketing' campaign
            // new request object
            Hashtable _request = new Hashtable();
            _request["jsonrpc"] = "2.0";
            _request["id"] = 1;
            // set method name
            _request["method"] = "get_campaigns";
            // set conditions
            Hashtable operator_obj = new Hashtable();
            operator_obj["EQUALS"] = campaignName;
            Hashtable name_obj = new Hashtable();
            name_obj["name"] = operator_obj;
            // set params request object
            object[] params_array = { api_key, name_obj };
            _request["params"] = params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            String response_string = null;

            // decode response to Json object
            Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;

            // get result
            Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;

            string campaign_id = null;

            // get campaign id
            foreach (object key in result.Keys)
            {
                campaign_id = key.ToString();
            }

            // add contact to 'sample_marketing' campaign
            // new request object
            _request = new Hashtable();

            _request["jsonrpc"] = "2.0";
            _request["id"] = 2;

            // set method name
            _request["method"] = "add_contact";

            Hashtable contact_params = new Hashtable();
            contact_params["campaign"] = campaign_id;
            contact_params["name"] = contactName;
            contact_params["email"] = subscriberEmail;

      

            // set params request object
            object[] add_contact_params_array = { api_key, contact_params };

            _request["params"] = add_contact_params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            response_string = null;

            return campaign_id;
           
        }

        /// <summary>
        /// Get Contact detail from GetResponse
        /// </summary>
        /// <param name="api_key"></param>
        /// <param name="api_url"></param>
        /// <param name="campaign_id"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public string GetContactEmailSubcriber(string api_key, string api_url, string campaign_id, string email)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            // get CAMPAIGN_ID of 'sample_marketing' campaign
            // new request object
            Hashtable _request = new Hashtable();
            _request["jsonrpc"] = "2.0";
            _request["id"] = 3;

            // set method name
            _request["method"] = "get_contacts";

            Hashtable contact_params = new Hashtable();
            contact_params["campaigns"] = campaign_id;

            Hashtable contact_params1 = new Hashtable();
            contact_params1["EQUALS"] = email;

            Hashtable name_obj1 = new Hashtable();
            name_obj1["email"] = contact_params1;

            // set params request object
            object[] add_contact_params_array = { api_key, name_obj1, contact_params };

            _request["params"] = add_contact_params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            string response_string = null;

            // decode response to Json object
            Dictionary<string, object> jsonContent = jss.DeserializeObject(response_string) as Dictionary<string, object>;

            // get result
            Dictionary<string, object> result = jsonContent["result"] as Dictionary<string, object>;
            string contactID = null;

            // get campaign id
            foreach (object key in result.Keys)
            {
                contactID = key.ToString();
            }
            return contactID;
        }

        /// <summary>
        /// Delete (Unsubscribe) contact from GetResponse
        /// </summary>
        /// <param name="contactID"></param>
        public void UnsubscribeContactEmail(string api_key, string api_url, string contactID)
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            String response_string = null;
            // new request object
            Hashtable _request = new Hashtable();

            _request["jsonrpc"] = "2.0";
            _request["id"] = 4;

            // set method name
            _request["method"] = "delete_contact";

            Hashtable contact_params = new Hashtable();
            contact_params["contact"] = contactID;

            // set params request object
            object[] add_contact_params_array = { api_key, contact_params };

            _request["params"] = add_contact_params_array;

            // send headers and content in one request
            // (disable 100 Continue behavior)
            System.Net.ServicePointManager.Expect100Continue = false;

            // initialize client
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(api_url);
            request.Method = "POST";

            byte[] request_bytes = Encoding.UTF8.GetBytes(jss.Serialize(_request));

            response_string = null;
        }




        
    }
}
