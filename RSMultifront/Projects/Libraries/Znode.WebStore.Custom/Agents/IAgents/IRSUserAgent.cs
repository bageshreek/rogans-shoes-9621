﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore.Agents;

namespace Znode.WebStore.Custom.Agents.IAgents
{
    public interface IRSUserAgent: IUserAgent
    {
        string GetDefaultStoreDetailsFromCookie();

        void SetDefaultStoreDetailsInCookie();
    }
}
