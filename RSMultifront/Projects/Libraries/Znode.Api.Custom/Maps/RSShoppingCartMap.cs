﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Maps
{
    public class RSShoppingCartMap : ShoppingCartMap
    {
        private readonly IShoppingCartItemMap shoppingCartItemMap;
        public RSShoppingCartMap() : base()
        {
            shoppingCartItemMap = ZnodeDependencyResolver.GetService<IShoppingCartItemMap>();
        }

        public override ShoppingCartModel ToModel(Znode.Libraries.ECommerce.ShoppingCart.ZnodeShoppingCart znodeCart, IImageHelper objImage = null)
        {
            if (HelperUtility.IsNull(znodeCart))
                return new ShoppingCartModel();

            ShoppingCartModel model = new ShoppingCartModel
            {
                Discount = znodeCart.Discount,
                GiftCardAmount = znodeCart.GiftCardAmount,
                GiftCardApplied = znodeCart.IsGiftCardApplied,
                GiftCardMessage = znodeCart.GiftCardMessage,
                GiftCardNumber = znodeCart.GiftCardNumber,
                GiftCardValid = znodeCart.IsGiftCardValid,
                GiftCardBalance = znodeCart.GiftCardBalance,
                MultipleShipToEnabled = znodeCart.IsMultipleShipToAddress,
                OrderLevelDiscount = znodeCart.OrderLevelDiscount,
                OrderLevelShipping = znodeCart.OrderLevelShipping,
                OrderLevelTaxes = znodeCart.OrderLevelTaxes,
                ProfileId = znodeCart.ProfileId,
                PortalId = znodeCart.PortalId.GetValueOrDefault(),
                UserId = znodeCart.UserId,
                SalesTax = znodeCart.SalesTax,
                ShippingCost = HelperUtility.IsNotNull(znodeCart.Shipping) ? znodeCart.ShippingCost : 0,
                ShippingDifference = HelperUtility.IsNotNull(znodeCart.Shipping) ? znodeCart.ShippingDifference : 0,
                SubTotal = znodeCart.SubTotal,
                TaxCost = znodeCart.TaxCost,
                Shipping = ShippingMap.ToModel(znodeCart.Shipping),
                TaxRate = znodeCart.TaxRate,
                Total = znodeCart.Total,
                LocaleId = znodeCart.LocalId,
                PublishedCatalogId = znodeCart.PublishedCatalogId,
                OmsOrderId = znodeCart.OrderId,
                ShippingId = znodeCart.Shipping.ShippingID,
                CSRDiscountAmount = znodeCart.CSRDiscountAmount,
                CSRDiscountDescription = znodeCart.CSRDiscountDescription,
                CSRDiscountApplied = znodeCart.CSRDiscountApplied,
                CSRDiscountMessage = znodeCart.CSRDiscountMessage,
                CurrencyCode = znodeCart.CurrencyCode,
                CultureCode = znodeCart.CultureCode,
                CurrencySuffix = znodeCart.CurrencySuffix,
                CookieMappingId = new ZnodeEncryption().EncryptData(znodeCart.CookieMappingId.ToString()),
                CustomShippingCost = znodeCart.CustomShippingCost,
                CustomTaxCost = znodeCart.CustomTaxCost,
                IsLineItemReturned = znodeCart.IsLineItemReturned,
                EstimateShippingCost = znodeCart.EstimateShippingCost,
                Custom1 = znodeCart.Custom1,
                Custom2 = znodeCart.Custom2,
                Custom3 = znodeCart.Custom3,
                Custom4 = znodeCart.Custom4,
                Custom5 = znodeCart.Custom5,
                TotalAdditionalCost = znodeCart.TotalAdditionalCost,
                IsAllowWithOtherPromotionsAndCoupons = znodeCart.IsAllowWithOtherPromotionsAndCoupons,
                IsCalculateVoucher = znodeCart.IsCalculateVoucher,
                IsAllVoucherRemoved = znodeCart.IsAllVoucherRemoved,
                JobName = znodeCart.JobName
            };

            model.OrderLevelDiscountDetails = znodeCart.OrderLevelDiscountDetails?.DistinctBy(x => x.DiscountCode)?.ToList();

            foreach (Libraries.ECommerce.Entities.ZnodeCoupon coupon in znodeCart?.Coupons)
                model.Coupons.Add(CouponMap.ToModel(coupon));

            foreach (Libraries.ECommerce.Entities.ZnodeVoucher voucher in znodeCart?.Vouchers)
                model.Vouchers.Add(VoucherMap.ToVoucherModel(voucher));

            if ((model.ShippingCost.Equals(0) && (model.Shipping.ShippingDiscount > 0) && (model.Shipping.ShippingId > 0)) || model.Shipping.ShippingDiscountApplied)
                model.FreeShipping = true;

            int FreeShippingItemsCount = 0;
            int NonFreeShippingItemsCount = 0;
            foreach (Znode.Libraries.ECommerce.ShoppingCart.ZnodeShoppingCartItem cartItem in znodeCart.ShoppingCartItems)
            {
                model.ShoppingCartItems.Add(shoppingCartItemMap.ToModel(cartItem, znodeCart, objImage));
                OrderAttributeModel orderAttributeModel = cartItem.Product.Attributes.Find(x => string.Equals(x.AttributeCode, "IsDownloadable", StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.AttributeValue, "true", StringComparison.InvariantCultureIgnoreCase));
                if (cartItem.Product.FreeShippingInd || orderAttributeModel != null)
                    FreeShippingItemsCount++;
                else NonFreeShippingItemsCount++;
            }
            if (NonFreeShippingItemsCount <= 0 && FreeShippingItemsCount > 0) model.FreeShipping = true;
            return model;
        }
    }
}
