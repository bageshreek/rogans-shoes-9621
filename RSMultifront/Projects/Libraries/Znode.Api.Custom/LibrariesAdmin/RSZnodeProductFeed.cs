﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Xml;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;


namespace Znode.Api.Custom.LibrariesAdmin
{
    public class RSZnodeProductFeedHelper : ZnodeProductFeedHelper
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        // private readonly IZnodeRepository<ZnodePublishProductEntity> _publishProductEntity;
        private readonly IZnodeRepository<ZnodePublishCategoryEntity> _publishCategoryEntity;

        // private readonly ZnodeRssWriter rssWriter;
        private const string SEOURLProductType = "Product";
        private const string SEOURLContentPageType = "ContentPage";
        private const string SEOURLCategoryType = "Category";
        private const string LastModifier = "Use the database update date";
        private int fileCount = 0;
        private readonly int recCount = Convert.ToInt32(ZnodeApiSettings.ProductFeedRecordCount);
        #endregion

        public RSZnodeProductFeedHelper() : base()
        {
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _publishCategoryEntity = new ZnodeRepository<ZnodePublishCategoryEntity>(HelperMethods.Context);
        }
        /// <summary>
        /// ZnodeCategoryIds filter commented in base code here it is uncommented
        /// </summary>
        /// <param name="commaSepCatalogIds"></param>
        /// <param name="localeId"></param>
        /// <returns></returns>
        protected override List<PublishedProductEntityModel> GetProductData(string commaSepCatalogIds, int localeId)
        {
            //Get version id 
            string versionIds = GetVersionIds(commaSepCatalogIds, localeId);

            FilterCollection productFilter = new FilterCollection();
            if (!string.IsNullOrEmpty(commaSepCatalogIds))
            {
                productFilter.Add(FilterKeys.ZnodeCatalogId, FilterOperators.In, commaSepCatalogIds);
            }

            productFilter.Add(FilterKeys.PublishedLocaleId, FilterOperators.Equals, Convert.ToString(localeId));
            productFilter.Add(FilterKeys.VersionId, FilterOperators.In, versionIds);
            productFilter.Add(FilterKeys.ProductIndex, FilterOperators.Equals, ZnodeConstant.DefaultPublishProductIndex.ToString());
            productFilter.Add(FilterKeys.PublishedIsActive, FilterOperators.Equals, ZnodeConstant.True);
            productFilter.Add(FilterKeys.ZnodeCategoryIds, FilterOperators.NotEquals, "0");
            NameValueCollection sortCollection = new NameValueCollection();

            return GetProductList(productFilter, sortCollection);
        }

        /// <summary>
        /// Overridden to add logic to copy the default sitemap files to webstore root folder
        /// </summary>
        /// <param name="fileNameCount"></param>
        /// <param name="txtXMLFileName"></param>
        /// <param name="priority"></param>
        /// <returns></returns>
        public override string GenerateGoogleSiteMapIndexFiles(int fileNameCount, string txtXMLFileName, string priority)
        {
            /*Start: It is use to check sitemap is category, product, contentpage or all*/
            string fileNameDestination = "";
            if (txtXMLFileName.ToLower().Contains("klaviyo"))
            {
                txtXMLFileName = txtXMLFileName.Replace("Klaviyo##_", "");
                fileNameDestination = "KlaviyoFeed.xml";
            }
            else if (txtXMLFileName.ToLower().Contains("category"))
            {
                txtXMLFileName = txtXMLFileName.Replace("Category##_", "");
                fileNameDestination = "sitemapcategory.xml";
            }
            else if (txtXMLFileName.ToLower().Contains("product"))
            {
                txtXMLFileName = txtXMLFileName.Replace("Product##_", "");
                fileNameDestination = "sitemapproduct.xml";
            }
            else if (txtXMLFileName.ToLower().Contains("content"))
            {
                txtXMLFileName = txtXMLFileName.Replace("Content##_", "");
                fileNameDestination = "sitemapcontent.xml";
            }
            else if (txtXMLFileName.ToLower().Contains("all"))
            {
                txtXMLFileName = txtXMLFileName.Replace("ALL##_", "");
                //fileNameDestination = "sitemap.xml";
                fileNameDestination = "sitemapall.xml";
            }

            /*End: It is use to check sitemap is category, product, contentpage or all*/
            string rootTag = "sitemapindex";
            string roottagxmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            // Construct the XML for the Site Map creation
            XmlDocument requestXMLDoc = new XmlDocument();
            requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", "UTF-8", null));
            XmlElement urlsetElement = null;
            urlsetElement = requestXMLDoc.CreateElement(rootTag);
            urlsetElement.SetAttribute("xmlns", roottagxmlns);
            requestXMLDoc.AppendChild(urlsetElement);

            for (int i = 0; i < fileNameCount; i++)
            {
                XmlElement urlElement = requestXMLDoc.CreateElement("sitemap");
                string fileName = ZnodeStorageManager.HttpPath($"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{txtXMLFileName.Trim()}_{i}{".xml"}");
                string filepath = fileName.Replace("~/data/default/content/", "");
                if (fileName.StartsWith("~/"))
                {
                    fileName = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Replace(System.Web.HttpContext.Current.Request.Url.AbsolutePath, string.Empty) + fileName.Substring(1);
                }

                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "loc", fileName));
                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "lastmod", DateTime.Now.ToString("yyyy-MM-ddhh-mm-ss")));

                if (!string.IsNullOrEmpty(priority))
                {
                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, "priority", priority));
                }

                urlsetElement.AppendChild(urlElement);
                /*Nivi Code start: Below Lines are written to copy the default sitemap files to webstore root folder */
                try
                {
                    string sitemapsource = ConfigurationManager.AppSettings["SitemapSource"].ToString() + filepath;
                    ZnodeLogging.LogMessage("sitemapsource= " + sitemapsource, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    string sitemapdesti = ConfigurationManager.AppSettings["SitemapDestination"].ToString() + fileNameDestination;
                    ZnodeLogging.LogMessage("sitemapdesti= " + sitemapdesti, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    System.IO.File.Copy(sitemapsource, sitemapdesti, true);
                }
                catch (System.IO.IOException e)
                {
                    ZnodeLogging.LogMessage("Sitemap Error" + e.Message, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                }
                /*Nivi Code end: Below Lines are written to copy the default sitemap files to webstore root folder */
            }
            string strSiteMapIndexFile = $"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{txtXMLFileName.Trim()}_{DateTime.Now.ToString("yyyy-MM-ddhh-mm-ss")}{".xml"}";
            ZnodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, strSiteMapIndexFile);
            return strSiteMapIndexFile;
        }

        /// <summary>
        /// Overridden to set model.ChangeFreq value from web.config key
        /// </summary>
        /// <param name="portalIds"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        protected override DataSet GetContentPageFeedData(string portalIds, ProductFeedModel model)
        {
            model.ChangeFreq = Convert.ToString(ConfigurationManager.AppSettings["changefrequency"]);
            DataSet datasetContentPagesList = base.GetContentPageFeedData(portalIds, model);
            return datasetContentPagesList;
        }

        /// <summary>
        /// Private method
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="tagName"></param>
        /// <param name="tagValue"></param>
        /// <returns></returns>
        private XmlElement MakeElement(XmlDocument doc, string tagName, string tagValue)
        {
            XmlElement elem = tagName.Contains("g:") ? doc.CreateElement("g", tagName.Split(':')[1], "http://base.google.com/ns/1.0") : doc.CreateElement(tagName);
            elem.InnerText = tagValue;
            return elem;
        }

        /// <summary>
        /// Overridden to add lastmod and modify changefreq columns
        /// </summary>
        /// <param name="dsCategory"></param>
        /// <param name="categoryDataFromSQL"></param>
        /// <param name="publishedCategoryData"></param>
        /// <param name="changeFrequency"></param>
        /// <param name="priority"></param>
        /// <param name="seoEntities"></param>
        protected override void AddDataToCategoryDataSet(ref DataSet dsCategory, DataSet categoryDataFromSQL, List<PublishedCategoryEntityModel> publishedCategoryData, string changeFrequency, string priority, List<ZnodePublishSeoEntity> seoEntities)
        {
            //Merge the Product Data from SQLin one DataSet
            for (int iCount = 0; iCount < publishedCategoryData.Count; iCount++)
            {
                int catID = publishedCategoryData[iCount].ZnodeCategoryId;
                string seoCode = publishedCategoryData[iCount].Attributes.FirstOrDefault(x => x.AttributeCode == "CategoryCode")?.AttributeValues;
                string seoUrl = seoEntities.FirstOrDefault(x => x.SEOCode == seoCode)?.SEOUrl;

                EnumerableRowCollection<DataRow> result = categoryDataFromSQL.Tables[0].AsEnumerable().Where(dr => dr.Field<int>("id") == catID);

                foreach (DataRow row in result)
                {
                    string domainName = $"{HttpContext.Current.Request.Url.Scheme}://{Convert.ToString(row["DomainName"])}";
                    DataRow dr = dsCategory.Tables[0].NewRow();
                    dr["loc"] = string.IsNullOrEmpty(seoUrl) ? $"{domainName}{"/category/"}{Convert.ToString(row["id"])}" : $"{domainName}/{seoUrl}";
                    /*Nivi Code*/
                    dr["lastmod"] = DateTime.Now.ToString("yyyy/MM/dd");
                    dr["changefreq"] = ConfigurationManager.AppSettings["changefrequency"].ToString();
                    /*Nivi Code*/
                    dr["priority"] = priority;
                    dsCategory.Tables[0].Rows.Add(dr);
                }
            }
        }


        /// <summary>
        /// Overridden to add lastmod and changefreq columns
        /// </summary>
        /// <param name="dsDataSet"></param>
        /// <param name="type"></param>
        protected override void CreateDataSetColumns(ref DataSet dsDataSet, string type)
        {
            dsDataSet.Tables.Add(new DataTable());
            switch (type)
            {
                case SEOURLProductType:
                    dsDataSet.Tables[0].Columns.Add("loc", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("lastmod", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("changefreq", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("title", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:condition", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("description", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:id", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:image_link", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("link", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:price", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:identifier_exists", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("g:availability", typeof(string));
                    break;
                case SEOURLCategoryType:
                    dsDataSet.Tables[0].Columns.Add("loc", typeof(string));
                    /*Nivi Code*/
                    dsDataSet.Tables[0].Columns.Add("lastmod", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("changefreq", typeof(string));
                    /*Nivi Code*/
                    dsDataSet.Tables[0].Columns.Add("priority", typeof(string));
                    break;
                case SEOURLContentPageType:
                    dsDataSet.Tables[0].Columns.Add("loc", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("changefreq", typeof(string));
                    dsDataSet.Tables[0].Columns.Add("priority", typeof(string));
                    break;
            }
        }


        /// <summary>
        /// Overridden to call our  CreateProductSiteMap method instead of base rsswriter.CreateProductSiteMap method
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="rootTagValue"></param>
        /// <param name="feedType"></param>
        /// <param name="rootTag"></param>
        /// <param name="productFeedModel"></param>
        /// <returns></returns>
        public override int GetProductXMLList(string portalId, string rootTagValue, string feedType, string rootTag, ProductFeedModel productFeedModel)
        {
            return CreateProductSiteMap(GetProductFeedDataFromSQL(portalId, productFeedModel.LocaleId, feedType, productFeedModel.ProductFeedTimeStampName, productFeedModel.Date), rootTag, rootTagValue, productFeedModel.FileName, Convert.ToString(productFeedModel.ProductFeedPriority));
        }
        public override int GetProductList(string portalId, string rootTagValue, string xmlFileName, string googleFeedTitle, string googleFeedLink, string googleFeedDesc, string feedType, int localeId, string productFeedTimeStampName, string lastModifiedDate)
        {
            if (feedType == "Klaviyo")
            {
                return CreateKlaviyofeedMap(GetProductFeedDataFromSQL(portalId, localeId, feedType, productFeedTimeStampName, lastModifiedDate), rootTagValue, xmlFileName, googleFeedTitle, googleFeedLink, googleFeedDesc);
            }
            else
            {
                return base.GetProductList(portalId, rootTagValue, xmlFileName, googleFeedTitle, googleFeedLink, googleFeedDesc, feedType, localeId, productFeedTimeStampName, lastModifiedDate);
            }
        }

        /// <summary>
        /// Overridden to add lastmod and changefreq nodes
        /// </summary>
        /// <param name="datasetValues"></param>
        /// <param name="rootTag"></param>
        /// <param name="rootTagValue"></param>
        /// <param name="xmlFileName"></param>
        /// <param name="priority"></param>
        /// <returns></returns>
        public int CreateProductSiteMap(DataSet datasetValues, string rootTag, string rootTagValue, string xmlFileName, string priority)
        {
            XmlDocument requestXMLDoc = null;
            try
            {
                if (datasetValues != null)
                {
                    int recordsCount = datasetValues.Tables[0].Rows.Count;
                    int loopCnt = 0;

                    for (; loopCnt < recordsCount;)
                    {
                        string filePath = $"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{xmlFileName.Trim()}_{this.fileCount.ToString()}{".xml"}";
                        string[] rootTagValues = rootTagValue.Split(',');
                        // Construct the XML for the Site Map creation
                        requestXMLDoc = new XmlDocument();
                        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", "UTF-8", null));
                        XmlElement urlsetElement = null;
                        urlsetElement = requestXMLDoc.CreateElement(rootTag);
                        if (rootTagValues?.Count() > 0)
                        {
                            string[] values = rootTagValues[0].Split('=');
                            urlsetElement.SetAttribute(values[0], values[1]);
                        }
                        requestXMLDoc.AppendChild(urlsetElement);

                        // Loop thro the dataset values.
                        do
                        {
                            DataRow dr = datasetValues.Tables[0].Rows[loopCnt];
                            XmlElement urlElement = requestXMLDoc.CreateElement("url");
                            urlsetElement.AppendChild(urlElement);
                            foreach (DataColumn dc in datasetValues.Tables[0].Columns)
                            {
                                if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()) && Equals(dc.ColumnName, "loc"))
                                {
                                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()?.Trim()));
                                }
                            }
                            /*Nivi Code*/
                            if (!string.IsNullOrEmpty(priority))
                            {
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "lastmod", DateTime.Now.ToString("yyyy/MM/dd")));
                            }

                            if (!string.IsNullOrEmpty(priority))
                            {
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "changefreq", ConfigurationManager.AppSettings["changefrequency"].ToString()));
                            }
                            /*Nivi Code*/
                            if (!string.IsNullOrEmpty(priority))
                            {
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "priority", priority));
                            }

                            loopCnt++;
                        }
                        while (loopCnt < recordsCount && (loopCnt + 1) % recCount != 0);
                        ZnodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, filePath);
                        // Increment the file count if the file has to be splitted.
                        this.fileCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return 0;
            }
            requestXMLDoc = null;
            return this.fileCount;
        }
        public int CreateKlaviyofeedMap(DataSet datasetValues, string rootTagValue, string xmlFileName, string googleFeedTitle, string googleFeedLink, string googleFeedDesc)
        {
            try
            {
                int recordsCount = datasetValues.Tables[0].Rows.Count;
                int loopCnt = 0;

                for (; loopCnt < recordsCount;)
                {
                    string filePath = $"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{xmlFileName.Trim()}_{this.fileCount.ToString()}{".xml"}";

                    string[] rootTagValues = rootTagValue.Split(',');

                    // Construct the XML for the Site Map creation
                    XmlDocument requestXMLDoc = new XmlDocument();

                    XmlNode rss = requestXMLDoc.CreateElement("rss");
                    XmlAttribute version = requestXMLDoc.CreateAttribute("version");
                    version.Value = "2.0";
                    rss.Attributes.Append(version);

                    XmlAttribute nameSpace = requestXMLDoc.CreateAttribute("xmlns:g");
                    nameSpace.Value = rootTagValues[0];
                    rss.Attributes.Append(nameSpace);

                    XmlNode channel = requestXMLDoc.CreateElement("Products");
                    // Loop thro the dataset values.
                    do
                    {
                        XmlElement itemtElement = requestXMLDoc.CreateElement("Product");
                        DataRow dr = datasetValues.Tables[0].Rows[loopCnt];

                        foreach (DataColumn dc in datasetValues.Tables[0].Columns)
                        {
                            if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()))
                            {
                                itemtElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()?.Trim()));
                            }
                        }

                        channel.AppendChild(itemtElement);

                        loopCnt++;
                    }
                    while (loopCnt < recordsCount && (loopCnt + 1) % recCount != 0);

                    rss.AppendChild(channel);
                    requestXMLDoc.AppendChild(rss);

                    ZnodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, filePath);

                    // Increment the file count if the file has to be splitted.
                    this.fileCount++;
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return 0;
            }

            return this.fileCount;
        }
        /// <summary>
        /// This method will get the data from both Monogo and SQL.  Also it will convert the data in one DataSet. 
        /// </summary>
        /// <param name="portalId">Portal Id</param>
        /// <param name="localeId">Locale Id</param>
        /// <param name="feedType">Feed Type For Ex Google, Bing etc.</param>
        /// <returns>DataSet of combination of SQL and Published Data</returns>
        protected override DataSet GetProductFeedDataFromSQL(string portalId, int localeId, string feedType, string productFeedTimeStampName, string lastModifiedDate)
        {
            DataSet productDataFromSQL = null;
            DataSet finalMergedDataSet = null;
            List<ZnodePublishSeoEntity> SEOSettings = null;
            try
            {
                List<PublishedProductEntityModel> productData = null;
                string commaSeparatedProductIds = string.Empty;

                List<PublishedCategoryEntityModel> categoryData = null;
                string commaSeparatedCategoryIds = string.Empty;

                //Get the Published Catalog Ids from SQL on the basis of Portal and Locale
                string commaSeparatedCatalogIds = GetPublishedCatgalogIDsFromSQL(portalId, localeId);

                //Get the category details 
                if (!string.IsNullOrEmpty(commaSeparatedCatalogIds))
                {
                    categoryData = GetCategoryData(commaSeparatedCatalogIds, localeId);
                }

                //Get the Category Ids 
                if (HelperUtility.IsNotNull(categoryData) && categoryData?.Count > 0)
                {
                    commaSeparatedCategoryIds = string.Join(",", categoryData.Select(x => x.ZnodeCategoryId).ToList());
                }

                string versionIds = GetVersionIds(commaSeparatedCatalogIds, localeId);
                //Get the product details 
                if (!string.IsNullOrEmpty(commaSeparatedCatalogIds))
                {
                    productData = GetPublishedProductData(commaSeparatedCatalogIds, localeId, commaSeparatedCategoryIds);
                }

                List<string> productCodes = productData.Select(h => h.SKU).Distinct().ToList();

                //Get the Product Ids 
                if (HelperUtility.IsNotNull(productData) && productData.Count > 0)
                {
                    commaSeparatedProductIds = string.Join(",", productCodes);
                }

                //Get the other details for ex. Price, Inventory etc from SQL for the selected product ids
                if (!string.IsNullOrEmpty(commaSeparatedProductIds))
                {
                    productDataFromSQL = GetProductDataFromSQL(commaSeparatedProductIds, localeId, portalId, feedType);
                }

                if (!string.IsNullOrEmpty(portalId))
                {
                    SEOSettings = GetSeoSettingList(localeId, portalId, ZnodeConstant.Product, productCodes, versionIds);
                }

                //Merge the published data and return the merged data set for further processing
                if (HelperUtility.IsNotNull(productData) && productData.Count > 0 && HelperUtility.IsNotNull(productDataFromSQL) && HelperUtility.IsNotNull(productDataFromSQL.Tables)
                    && productDataFromSQL.Tables[0].Rows.Count > 0)
                {
                    if (feedType == "Klaviyo")
                    {
                        finalMergedDataSet = ConvertDataInKlaviyoDataSet(productDataFromSQL, productData, SEOSettings);
                    }
                    else
                    {
                        finalMergedDataSet = ConvertDataInDataSet(productDataFromSQL, productData, SEOSettings);
                    }
                }

                return finalMergedDataSet;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return null;
            }
            finally
            {
                productDataFromSQL = null;
                finalMergedDataSet = null;
            }
        }
        private List<ZnodePublishSeoEntity> GetSeoSettingList(int localeId, string portalId, string seoType, List<string> commaSeparatedProductIds = null, string versionIds = null)
        {
            IZnodeRepository<ZnodePublishSeoEntity> _publishSEOEntity = new ZnodeRepository<ZnodePublishSeoEntity>(HelperMethods.Context);
            List<int?> portalIds = portalId.Split(',').Select(n => (int?)Convert.ToInt32(n)).ToList();

            FilterCollection filters = new FilterCollection();
            if (!portalIds.Any(x => x.Value == 0))
            {
                filters.Add("PortalId", FilterOperators.In, portalId);
            }

            if (!string.IsNullOrEmpty(versionIds))
            {
                filters.Add("VersionId", FilterOperators.In, versionIds);
            }

            filters.Add("SEOTypeName", FilterOperators.Is, seoType);
            filters.Add("LocaleId", FilterOperators.Equals, localeId.ToString());

            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollections());
            List<ZnodePublishSeoEntity> publishSEOList = _publishSEOEntity.GetEntityListWithoutOrderBy(whereClauseModel.WhereClause, whereClauseModel.FilterValues)?.ToList();

            if (commaSeparatedProductIds?.Count > 0)
            {
                publishSEOList = publishSEOList.Where(x => commaSeparatedProductIds.Contains(x.SEOCode))?.ToList();
            }

            if (publishSEOList?.Count > 0)
            {
                return publishSEOList;
            }

            return new List<ZnodePublishSeoEntity>();
        }

        /// <summary>
        /// Overridden to call our  CreateXMLSiteMapForAll method instead of base rsswriter.CreateXMLSiteMapForAll method
        /// </summary>
        /// <param name="model"></param>
        /// <param name="portalId"></param>
        /// <param name="rootTag"></param>
        /// <param name="rootTagValue"></param>
        /// <param name="feedType"></param>
        /// <returns></returns>
        public override int GetAllList(ProductFeedModel model, string portalId, string rootTag, string rootTagValue, string feedType)
        {
            int allsitemap = CreateXMLSiteMapForAll(GetAllFeedDataFromSQL(portalId, feedType, model), rootTag, rootTagValue, model.FileName, Convert.ToString(model.ProductFeedPriority));
            return allsitemap;
        }

        /// <summary>
        /// Overridden to add lastmod and changefreq nodes
        /// </summary>
        /// <param name="datasetValues"></param>
        /// <param name="rootTag"></param>
        /// <param name="rootTagValue"></param>
        /// <param name="xmlFileName"></param>
        /// <param name="priority"></param>
        /// <returns></returns>
        public int CreateXMLSiteMapForAll(DataSet datasetValues, string rootTag, string rootTagValue, string xmlFileName, string priority)
        {
            XmlDocument requestXMLDoc = null;
            try
            {
                int tableCount = datasetValues.Tables.Count;
                string filePath = $"{ZnodeConfigManager.EnvironmentConfig.ContentPath}{xmlFileName.Trim()}_{this.fileCount.ToString()}{".xml"}";
                string[] rootTagValues = rootTagValue.Split(',');
                // Construct the XML for the Site Map creation
                requestXMLDoc = new XmlDocument();
                requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", "UTF-8", null));
                XmlElement urlsetElement = null;
                urlsetElement = requestXMLDoc.CreateElement(rootTag);
                if (rootTagValues?.Count() > 0)
                {
                    string[] values = rootTagValues[0].Split('=');
                    urlsetElement.SetAttribute(values[0], values[1]);
                }
                requestXMLDoc.AppendChild(urlsetElement);
                for (int i = 0; i < tableCount; i++)
                {
                    int recordsCount = datasetValues.Tables[i].Rows.Count;
                    int loopCnt = 0;
                    if (datasetValues.Tables[i].TableName == "Content" || datasetValues.Tables[i].TableName == "Category")
                    {
                        // Loop thro the dataset values.
                        do
                        {
                            DataRow dr = datasetValues.Tables[i].Rows[loopCnt];
                            XmlElement urlElement = requestXMLDoc.CreateElement("url");
                            urlsetElement.AppendChild(urlElement);
                            foreach (DataColumn dc in datasetValues.Tables[i].Columns)
                            {
                                if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()))
                                {
                                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()));
                                }
                            }

                            loopCnt++;
                        }
                        while (loopCnt < recordsCount);
                    }
                    else if (datasetValues.Tables[i].TableName == "Product")
                    {
                        // Loop thro the dataset values.
                        do
                        {
                            DataRow dr = datasetValues.Tables[i].Rows[loopCnt];
                            XmlElement urlElement = requestXMLDoc.CreateElement("url");
                            urlsetElement.AppendChild(urlElement);
                            foreach (DataColumn dc in datasetValues.Tables[i].Columns)
                            {
                                if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()) && Equals(dc.ColumnName, "loc"))
                                {
                                    urlElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()));
                                }
                            }
                            if (!string.IsNullOrEmpty(priority))
                            {
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "lastmod", DateTime.Now.ToString("yyyy/MM/dd")));
                            }

                            if (!string.IsNullOrEmpty(priority))
                            {
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "changefreq", ConfigurationManager.AppSettings["changefrequency"].ToString()));
                            }

                            if (!string.IsNullOrEmpty(priority))
                            {
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "priority", priority));
                            }

                            loopCnt++;
                        }
                        while (loopCnt < recordsCount);
                    }
                    ZnodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, filePath);
                }
                // Increment the file count.
                this.fileCount++;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return 0;
            }
            requestXMLDoc = null;
            return this.fileCount;
        }

        /// <summary>
        /// Overridden to get different Versionid value as compared to base
        /// </summary>
        /// <param name="commaSepCatalogIds"></param>
        /// <param name="localeId"></param>
        /// <returns></returns>
        protected override List<PublishedCategoryEntityModel> GetCategoryData(string commaSepCatalogIds, int localeId)
        {
            //Get version id 
            // string versionIds = GetVersionIds(commaSepCatalogIds, localeId);
            /*Nivi code start: get current version id*/
            int versionIds = ZnodeDependencyResolver.GetService<IPublishProductHelper>()?.GetCatalogVersionId(Convert.ToInt32(commaSepCatalogIds), localeId) ?? 0;
            /*Nivi code start: get current version id*/
            FilterCollection categoryFilter = new FilterCollection();
            if (!string.IsNullOrEmpty(commaSepCatalogIds))
            {
                categoryFilter.Add(FilterKeys.ZnodeCatalogId, FilterOperators.In, commaSepCatalogIds);
            }

            categoryFilter.Add(FilterKeys.PublishedLocaleId, FilterOperators.Equals, Convert.ToString(localeId));
            categoryFilter.Add(FilterKeys.VersionId, FilterOperators.In, Convert.ToString(versionIds));
            NameValueCollection sortCollection = new NameValueCollection();
            sortCollection.Add(FilterKeys.ZnodeCategoryId, "asc");


            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(categoryFilter.ToFilterDataCollections());
            return _publishCategoryEntity.GetEntityList(whereClauseModel.WhereClause, DynamicClauseHelper.GenerateDynamicOrderByClause(sortCollection))?.ToModel<PublishedCategoryEntityModel>()?.ToList();
        }

        protected DataSet ConvertDataInKlaviyoDataSet(DataSet productDataFromSQL, List<PublishedProductEntityModel> publishedProductData, List<ZnodePublishSeoEntity> seoSettings)
        {
            DataSet dsProduct = new DataSet();
            CreateKlaviyoDataSetColumns(ref dsProduct, SEOURLProductType);
            AddKlaviyoDataToDataSet(ref dsProduct, productDataFromSQL, publishedProductData, seoSettings);
            return dsProduct;
        }
        protected void CreateKlaviyoDataSetColumns(ref DataSet dsDataSet, string type)
        {
            dsDataSet.Tables.Add(new DataTable());
            dsDataSet.Tables[0].Columns.Add("id", typeof(string));
            dsDataSet.Tables[0].Columns.Add("title", typeof(string));
            dsDataSet.Tables[0].Columns.Add("link", typeof(string));
            dsDataSet.Tables[0].Columns.Add("image_link", typeof(string));
            dsDataSet.Tables[0].Columns.Add("description", typeof(string));
            dsDataSet.Tables[0].Columns.Add("price", typeof(string));
            dsDataSet.Tables[0].Columns.Add("gender", typeof(string));
            dsDataSet.Tables[0].Columns.Add("brand", typeof(string));
            dsDataSet.Tables[0].Columns.Add("categoryname", typeof(string));
        }
        protected void AddKlaviyoDataToDataSet(ref DataSet dsProduct, DataSet productDataFromSQL, List<PublishedProductEntityModel> productData, List<ZnodePublishSeoEntity> seoSettigs)
        {
            try
            {
                int loopCount = productData.Count;
                if (productData.Count > productDataFromSQL.Tables[0].Rows.Count)
                {
                    loopCount = productDataFromSQL.Tables[0].Rows.Count;
                }
                //Merge the Product Data from SQL in one DataSet
                for (int iCount = 0; iCount < loopCount; iCount++)
                {
                    int pid = Convert.ToInt16(productDataFromSQL.Tables[0].Rows[iCount]["PortalId"].ToString());
                    if (pid > 0 && string.IsNullOrEmpty(productDataFromSQL.Tables[0].Rows[iCount]["DomainName"].ToString()))
                    {
                        break;
                    }
                    int id = Convert.ToInt32(Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["g:id"]));
                    PublishedProductEntityModel publishedProduct = productData?.FirstOrDefault(x => x.ZnodeProductId == id);
                    if (!Equals(publishedProduct, null))
                    {
                        string domainName = $"{HttpContext.Current.Request.Url.Scheme}://{Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["DomainName"])}";
                        string seoUrl = seoSettigs?.Where(x => x.SEOCode == publishedProduct.SKU && x.VersionId == publishedProduct.VersionId)?.Select(x => x.SEOUrl)?.FirstOrDefault();
                        //string Desc= seoSettigs?.Where(x => x.SEOCode == publishedProduct.SKU && x.VersionId == publishedProduct.VersionId)?.Select(x => x.SEOUrl)?.FirstOrDefault();
                        string ImageURL = Convert.ToString(ConfigurationManager.AppSettings["BaseImagePath"]) + "t_460/" + publishedProduct.Attributes.Where(x => x.AttributeCode == "BaseImage").FirstOrDefault().AttributeValues;
                        DataRow dr = dsProduct.Tables[0].NewRow();
                        dr["title"] = publishedProduct.Name;
                        dr["id"] = publishedProduct.SKU;
                        dr["link"] = domainName + "/" + seoUrl;// Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["loc"]);
                        dr["image_link"] = ImageURL;
                        dr["description"] = publishedProduct.Name;
                        dr["price"] = Convert.ToString(productDataFromSQL.Tables[0].Rows[iCount]["g:price"]);
                        dr["gender"] = Convert.ToString(publishedProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == "Gender")?.SelectValues[0].Value);
                        dr["brand"] = Convert.ToString(publishedProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == "Brand")?.SelectValues[0].Value);
                        dr["categoryname"] = publishedProduct.CategoryName;
                        dsProduct.Tables[0].Rows.Add(dr);
                    }
                }
            }

            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
        }

    }
}
