﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Engine.WebStore;
using Znode.Libraries.Resources;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.WebStore.Models;
/*TBD*/
using Znode.Engine.Api.Models;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSProductAgent : ProductAgent, IProductAgent
    {
        #region Private Variables
        
        private readonly IPublishCategoryClient _publishCategoryClient;        
        private readonly IPublishProductClient _productClient;
        #endregion

        #region Public Constructor

        public RSProductAgent(ICustomerReviewClient reviewClient, IPublishProductClient productClient, IWebStoreProductClient webstoreProductClient, ISearchClient searchClient, IHighlightClient highlightClient, IPublishCategoryClient publishCategoryClient):
            base(reviewClient,productClient,webstoreProductClient,searchClient,highlightClient,publishCategoryClient)
        {            
            _publishCategoryClient = GetClient<IPublishCategoryClient>(publishCategoryClient);            
            _productClient = GetClient<IPublishProductClient>(productClient);
        }

        #endregion

        /// <summary>
        /// Written our own logic to avoid GetConfigurableProduct Call on Color,Size,Width Selection on PDP Page
        /// </summary>
        /// <param name="productID"></param>
        /// <returns></returns>
        public override ProductViewModel GetProduct(int productID)
        {
            
            ProductViewModel viewModel = base.GetProduct(productID);
            //try
            //{
               
                if (viewModel != null)
                {
                    string size = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Size")?.AttributeValues;
                    string colorval = "";
                    try
                    {
                        colorval = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Color").SelectedAttributeValue[0];
                    }
                    catch (Exception ex)
                    {

                    }
                    if (viewModel?.ConfigurableData?.SwatchImages == null)
                        viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();
                    //GALLERY IMAGES=>OMSColorCode = AS_ZK2017_BWB1.JPG || ASICS Kayano Single Tab Socks -Womens Black White Blue,AS_ZK2017_BWB2.JPG || ASICS Kayano Single Tab Socks -Womens Black White Blue View 2,||,||,,
                    //COLOR => OMSColorValue = Black White Blue, 
                    //IMAGEPATH + BASEIMAGE=> OMSColorPath = https://images.rogansshoes.com/t_97/AS_ZK2017_BWB1.JPG,
                    //BASEIMAGE=> OMSColorSwatchText =AS_ZK2017_BWB1.JPG,
                    //Size=> Custom1 =M,
                    //ShoeWidth => Custom2 =M,
                    //Color-Size-Shoewidth => Custom3 |Black White Blue-M-M
                    List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();

                    string _colorSizeWidthList = string.Join(",", viewModel.AssociatedProducts.Select(o => o.Custom3).Distinct().ToList());
                    _colorSizeWidthList = _colorSizeWidthList + ",";
                    foreach (string color in _colors)
                    {
                        SwatchImageViewModel svm = new SwatchImageViewModel();
                        svm.AttributeCode = "Color";
                        svm.AttributeValues = color;
                        svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
                        /*Custom3 - GalleryImages*/
                        svm.Custom2 = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorSwatchText;
                        svm.Custom3 = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorCode;
                        viewModel.ConfigurableData.SwatchImages.Add(svm);

                        SwatchImageViewModel colorsize = new SwatchImageViewModel();
                        colorsize.AttributeCode = "ColorSize";
                        List<string> colorspecificsizes = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == color).Select(o => o.Custom1).Distinct().ToList();
                        colorsize.AttributeValues = color + "-";
                        colorsize.Custom1 = color;
                        colorspecificsizes.ForEach(cs => { colorsize.AttributeValues = colorsize.AttributeValues + "," + Convert.ToString(cs); });
                        colorsize.AttributeValues = colorsize.AttributeValues + ",";
                        List<string> selctedcolorsizespecificwidths = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == colorval && Convert.ToString(x.Custom1) == size)?.Select(o => o.Custom2).Distinct().ToList();
                        selctedcolorsizespecificwidths?.ForEach(cs => { colorsize.Custom2 = colorsize.Custom2 + "," + Convert.ToString(cs); });
                        colorsize.Custom2 = colorsize.Custom2 + ",";
                        viewModel.ConfigurableData.SwatchImages.Add(colorsize);
                    }
                    viewModel = SetDefaultStoreAddressDetails(viewModel);
                    viewModel.ShowAddToCart = true;
                    viewModel.InventoryMessage = "";
                    viewModel.Custom4 = _colorSizeWidthList;
                    viewModel.Custom5 = "FROMPLP";
                }
            //}
            //catch(Exception ex)
            //{
            //    ZnodeLogging.LogMessage(ex.Message, "RSProductAgent", TraceLevel.Error, ex);
            //}
            return viewModel;
        }

        /// <summary>
        /// Assigns Default selected store values to ViewModel to be shown in Pick Up on PDP
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        private ProductViewModel SetDefaultStoreAddressDetails(ProductViewModel viewModel)
        {
            try
            {
                string defaultStore = GetFromCookie("DefaultSelectedStore");
                string[] storeValues = defaultStore.Split('*');
                if (storeValues.Length == 3)
                {
                    viewModel.Custom1 = storeValues[0];
                    viewModel.Custom2 = storeValues[1];
                    viewModel.Custom3 = storeValues[2];
                }
            }
            catch(Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, "RSProductAgent", TraceLevel.Error, ex);
            }
            return viewModel;
        }       

        /// <summary>
        /// To show Call for pricing products Price when admin comes through Impersonation
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="addOns"></param>
        /// <param name="minQuantity"></param>
        /// <param name="addOnIds"></param>
        protected override void GetProductFinalPrice(ProductViewModel viewModel, List<AddOnViewModel> addOns, decimal minQuantity, string addOnIds)
        {
            if (!viewModel.IsCallForPricing || (viewModel.IsCallForPricing && SessionHelper.GetDataFromSession<ImpersonationModel>(WebStoreConstants.ImpersonationSessionKey) != null))
            {
                viewModel.UnitPrice = viewModel.SalesPrice > 0 ? viewModel.SalesPrice : viewModel.RetailPrice;
                //Apply tier price if any.
                if (viewModel.TierPriceList?.Count > 0 && viewModel.TierPriceList.Where(x => minQuantity >= x.MinQuantity)?.Count() > 0)
                    viewModel.ProductPrice = viewModel.TierPriceList.FirstOrDefault(x => minQuantity >= x.MinQuantity && minQuantity < x.MaxQuantity)?.Price;
                else
                    viewModel.ProductPrice = (minQuantity > 0 && HelperUtility.IsNotNull(viewModel.SalesPrice)) ? viewModel.SalesPrice * minQuantity : viewModel.PromotionalPrice > 0 ? viewModel.PromotionalPrice * minQuantity : viewModel.RetailPrice;

                if (addOns?.Count > 0)
                {
                    decimal? addonPrice = 0.00M;

                    if (!string.IsNullOrEmpty(addOnIds))
                    {
                        foreach (string addOn in addOnIds.Split(','))
                        {
                            AddOnValuesViewModel addOnValue = addOns.SelectMany(
                                       y => y.AddOnValues.Where(x => x.SKU == addOn))?.FirstOrDefault();
                            if (HelperUtility.IsNotNull(addOnValue))
                                addonPrice = addonPrice + (HelperUtility.IsNotNull(addOnValue.SalesPrice) ? addOnValue.SalesPrice : addOnValue.RetailPrice);

                        }
                    }
                    viewModel.ProductPrice = addonPrice > 0 ? viewModel.ProductPrice + addonPrice : viewModel.ProductPrice;

                    //Check add on price.
                    if (HelperUtility.IsNull(addonPrice))
                    {
                        viewModel.ShowAddToCart = false;
                        viewModel.InventoryMessage = Convert.ToBoolean(viewModel?.Attributes?.Value(ZnodeConstant.CallForPricing)) ? string.Empty : WebStore_Resources.ErrorAddOnPrice;
                    }
                }
                //Check product final price.
                if (HelperUtility.IsNull(viewModel.ProductPrice) && (!Equals(viewModel.ProductType, ZnodeConstant.GroupedProduct)))
                {
                    viewModel.ShowAddToCart = false;
                    viewModel.InventoryMessage = Convert.ToBoolean(viewModel?.Attributes?.Value(ZnodeConstant.CallForPricing)) ? string.Empty : WebStore_Resources.ErrorPriceNotAssociate;
                }
            }
            else
                viewModel.ShowAddToCart = false;
        }

        /// <summary>
        /// Instead of LinkHomeIcon we require LinkTextHome
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="productAssociatedCategoryIds"></param>
        /// <param name="checkFromSession"></param>
        /// <returns></returns>
        public override string GetBreadCrumb(int categoryId, string[] productAssociatedCategoryIds, bool checkFromSession)
        {
            if (checkFromSession)
            {
                categoryId = GetFromSession<int>(string.Format(WebStoreConstants.LastSelectedCategoryForPortal, PortalAgent.CurrentPortal.PortalId));
                if ((!productAssociatedCategoryIds?.Contains(categoryId.ToString())).GetValueOrDefault())
                    categoryId = Convert.ToInt32(productAssociatedCategoryIds[0]);
                else if (HelperUtility.IsNull(productAssociatedCategoryIds))
                    /*here*/
                    return $"<a href='/'>{WebStore_Resources.LinkTextHome}</a>";
            }
            FilterCollection filters = GetRequiredFilters();
            filters.Add(WebStoreEnum.IsGetParentCategory.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue);
            filters.Add(WebStoreEnum.IsBindImage.ToString(), FilterOperators.Equals, ZnodeConstant.FalseValue);
            CategoryViewModel category = _publishCategoryClient.GetPublishCategory(categoryId, filters, new ExpandCollection { ZnodeConstant.SEO })?.ToViewModel<CategoryViewModel>();
            /*here*/
            return $"<a href='/'>{WebStore_Resources.LinkTextHome}</a> / {GetBreadCrumbHtml(category)}";
        }
        /// <summary>
        /// Private Method
        /// </summary>
        /// <param name="category"></param>
        /// <param name="isParent"></param>
        /// <returns></returns>
        private string GetBreadCrumbHtml(CategoryViewModel category, bool isParent = false)
        {
            if (HelperUtility.IsNotNull(category))
            {
                string breadCrumb = $"<a href='/{(string.IsNullOrEmpty(category.SEODetails?.SEOUrl) ? "category/" + category.CategoryId : category.SEODetails.SEOUrl)}'>{category.CategoryName}</a>";
                if (category.ParentCategory?.Count > 0)
                    breadCrumb = GetBreadCrumbHtml(category.ParentCategory[0], true) + " / " + breadCrumb;
                return breadCrumb;
            }
            return string.Empty;
        }

        /// <summary>
        /// Overridden to set Product image value to be shown on Write A review Page
        /// </summary>
        /// <param name="productID"></param>
        /// <param name="productName"></param>
        /// <param name="rating"></param>
        /// <returns></returns>
        public override  ProductReviewViewModel GetProductForReview(int productID, string productName, decimal? rating)
        {
            _productClient.SetProfileIdExplicitly(Znode.Engine.WebStore.Helper.GetProfileId().GetValueOrDefault());
            PublishProductModel model = _productClient.GetPublishProduct(productID, GetRequiredFilters(), new ExpandCollection { ExpandKeys.SEO });
            if (HelperUtility.IsNotNull(model))
            {
                //Updated rating added againest that product.
                List<RecentViewModel> recentViewProductCookie = GetFromSession<List<RecentViewModel>>(ZnodeConstant.RecentViewProduct);
                if (recentViewProductCookie!= null)
                {
                    foreach (RecentViewModel recentViewModel in recentViewProductCookie.Where(x => x.PublishProductId == productID))
                    {
                        recentViewModel.Rating = Convert.ToDecimal(rating);
                    }
                    SaveInSession(ZnodeConstant.RecentViewProduct, recentViewProductCookie);
                }
                ProductReviewViewModel viewModel = model.ToViewModel<ProductReviewViewModel>();
                if (model.ConfigurableProductId > 0)
                {
                    viewModel.PublishProductId = model.ConfigurableProductId;
                    viewModel.ProductName = model.ParentConfiguarableProductName;
                    viewModel.Image = model.Attributes?.FirstOrDefault(x => x.AttributeCode == "BaseImage")?.AttributeValues;
                }
                return viewModel;
            }
            else
                return new ProductReviewViewModel { PublishProductId = productID, ProductName = productName };
        }
    }
}
